<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Income;


class IncomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [            
            ['id' => 1,
             'rate' => '0',
             'percent' => '0',
            ],
            ['id' => 2,
             'rate' => '1',
             'percent' => '0',
            ],
            ['id' => 3,
             'rate' => '2',
             'percent' => '10',
            ],
            ['id' => 4,
             'rate' => '3',
             'percent' => '15',
            ],
            ['id' => 5,
             'rate' => '4',
             'percent' => '20',
            ],
           
            
            
        ];
    
        foreach ($items as $item) {
            Income::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
