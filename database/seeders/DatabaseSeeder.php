<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Grant;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $items = [   
            ['id' => 1,
             'total_sales' => '0',
             'name' => 'Thành viên mới',
             'percent' => '0',
             'order' => '0',
            ],         
            ['id' => 2,
             'total_sales' => '500000',
             'name' => 'Cộng tác viên',
             'percent' => '10',
             'order' => '1',
            ],
            ['id' => 3,
             'total_sales' => '5000000',
             'name' => 'Đại lý xã',
             'percent' => '15',
             'order' => '2',
            ],
            ['id' => 4,
             'total_sales' => '50000000',
             'name' => 'Đại lý huyện',
             'percent' => '20',
             'order' => '3',
            ],
            ['id' => 5,
             'total_sales' => '200000000',
             'name' => 'Đại lý tỉnh',
             'percent' => '25',
             'order' => '4',
            ],
            
            
            
        ];
    
        foreach ($items as $item) {
            Grant::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
