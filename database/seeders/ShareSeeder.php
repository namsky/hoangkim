<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Share;

class ShareSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [  
            ['id' => 1,
             'rate' => '0',
             'percent' => '0',
            ],          
            ['id' => 2,
             'rate' => '1',
             'percent' => '0',
            ],
            ['id' => 3,
             'rate' => '2',
             'percent' => '2',
            ],
            ['id' => 4,
             'rate' => '3',
             'percent' => '3',
            ],
            ['id' => 5,
             'rate' => '4',
             'percent' => '5',
            ],
            
        ];
    
        foreach ($items as $item) {
            Share::updateOrCreate(['id' => $item['id']], $item);
        }
    }
}
