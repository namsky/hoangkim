<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_bonuses', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('rate');
            $table->float('percent', 10, 0);
            $table->timestamps();
        });
        
        Artisan::call('db:seed', [
            '--class' => GroupBonusSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_bonuses');
    }
}
