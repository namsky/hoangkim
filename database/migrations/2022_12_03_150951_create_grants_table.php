<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grants', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->float('total_sales',0,15);
            $table->integer('percent');
            $table->integer('order');
            $table->timestamps();
            //
        });
        Artisan::call('db:seed', [
            '--class' => DatabaseSeeder::class
        ]);
        

    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grants');
    }
}
