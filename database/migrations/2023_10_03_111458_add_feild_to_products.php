<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFeildToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('f1_price');
            $table->dropColumn('f2_price');
            $table->dropColumn('f3_price');
            $table->tinyInteger('status')->default(1)->after('hot')->comment('0: Không hiển thị | 1 : Hiển thị');
            $table->integer('price_sale')->default(0)->after('price');
            $table->integer('price_system')->default(0)->after('price_sale');
            $table->integer('price_member')->default(0)->after('price_system');
            $table->integer('stock')->default(0)->after('price_member');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
        });
    }
}
