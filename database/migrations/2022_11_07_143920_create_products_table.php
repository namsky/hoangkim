<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('price');
            $table->string('f1_price')->nullable();
            $table->string('f2_price')->nullable();
            $table->string('f3_price')->nullable();
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->text('content');
            $table->tinyInteger('type')->comment('1: Sản phẩm đơn| 2: Gói sản phẩm');
            $table->tinyInteger('hot')->default(0);
            $table->foreignId('category_id')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
