<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('name');
            $table->string('phone');
            $table->text('address');
            $table->unsignedTinyInteger('status')->default(0);
            $table->string('total_money');
            $table->tinyInteger('product_id');
            $table->tinyInteger('amount');
            $table->tinyInteger('payment_type')->default(0);// 0: trả tiền khi nhận hàng 1: trả bằng tiền
            $table->tinyInteger('pro_type')->default(2)->comment('1: Sản phẩm đơn | 2: Sản phẩm gói');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
