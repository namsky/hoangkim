<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('username');
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('phone')->nullable()->unique();
            $table->tinyInteger('type')->default(0)->comment('1: Thành viên | 2 : Nhà Cung Cấp | 3 : Nhà Sản Xuất');
            $table->unsignedTinyInteger('gender')->default(1);
            $table->text('address')->nullable();
            $table->float('wallet1',15,0)->default(0)->nullable();
            $table->float('shares',15,0)->default(0)->nullable();
            $table->integer('f1_id')->nullable()->comment('id bố');
            $table->integer('f2_id')->nullable()->comment('id ông');
            $table->integer('f3_id')->nullable()->comment('id cụ');
            $table->integer('total_child')->default(0)->nullable();
            $table->integer('level');
            $table->tinyInteger('group_status')->default(0);
            $table->string('avatar')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
