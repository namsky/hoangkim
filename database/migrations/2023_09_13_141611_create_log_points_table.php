<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_points', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('before');
            $table->integer('point');
            $table->integer('after');
            $table->integer('type');
            $table->integer('status')->comment('1: Công | 2: Trừ');
            $table->text('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_points');
    }
}
