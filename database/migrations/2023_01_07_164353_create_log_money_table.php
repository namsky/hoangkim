<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogMoneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_money', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->float('before',15,0);
            $table->float('after',15,0);
            $table->float('money',15,0);
            $table->integer('type')->comment('1: Chuyển ví, 2: Chuyển khoản, 3: Thưởng, 4: Mua hàng, 5: Thưởng chiết khấu theo tháng, 6: Hoa hồng');
            $table->text('reason');
            $table->tinyInteger('status')->comment('0: Trừ tiền, 1: Cộng tiền')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_money');
    }
}
