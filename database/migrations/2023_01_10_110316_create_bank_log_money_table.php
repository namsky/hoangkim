<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankLogMoneyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_log_money', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('send_user_id');
            $table->tinyInteger('receiver_id');
            $table->float('before',15,0);
            $table->float('money',15,0);
            $table->float('after',15,0);
            $table->tinyInteger('status')->default(0)->comment('0: Chuyển ví, 1: Chuyển khoản');
            $table->tinyInteger('type')->default(0)->comment('0: Cộng tiền, 1: Trừ tiền');
            $table->text('reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_log_money');
    }
}
