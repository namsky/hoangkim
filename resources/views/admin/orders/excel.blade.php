<table class="table table-striped table-bordered table-hover" id="admin-table">
    <thead>
        <tr>
            <th width="50px"> <b>Mã đơn hàng</b></th>
            <th width="250px"><b>Tên thành viên</b></th>
            <th width="100px"><b>Số điện thoại</b></th>
            <th width="300px"><b>Địa điểm giao hàng</b></th>
            <th width="100px"><b>Số điểm (VNĐ)</b></th>
            <th width="100px"><b>Ngày đặt</b></th>
            <th width="100px"><b>Trạng thái đơn</b></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $key => $order)
        <tr>
            <td >{{ $order->id }}</td>
            <td >{{ $order->user_id ? $order->user->name : 'Khách hàng đã bị xóa' }}</td>
            <td >{{ $order->phone }}</td>
            <td >{{ $order->address }}</td>
            <td >{{ ($order->pay_money)? number_format($order->total_money , 0, ',', '.'): '...'}}</td>
            <td >{{ \Carbon\Carbon::createFromDate($order->created_at)->format('d/m/Y') }}</td>
            <td >{{ ($order->status == 1) ? 'Đã nhận' : 'Đang xử lý' }}</td>
           
        </tr>
        @endforeach
    </tbody>
</table>