@extends('admin.layouts.master')

@section('page_title', 'Danh sách đơn hàng')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        {{-- <a href="" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Tạo mới</a> --}}
                        <a href="{{ route('ad.export') }}" class="btn btn-success mx-5">Xuất File Excel</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen ml-5" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <div class="portlet-title">
                    <form action="{{route('ad.order.searchOrder')}}" method="GET">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" name="code" class="form-control" value="{{ old('code') }}" id="exampleInputEmail1" placeholder="Nhập mã đơn hàng...">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                            <tr>
                                <th width="5%">Mã đơn hàng</th>
                                <th width="15%">Tên thành viên</th>
                                <th width="15%">Tên đăng nhập</th>
                                <th width="10%">Giá tiền đã chiết khấu</th>
                                <th width="10%">Giá niêm yết (chưa chiết khấu)</th>
                                <th width="10%">Ngày đặt</th>
                                <th width="7%">Trạng thái đơn</th>
                                <th width="5%">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $key => $order)
                            <tr>
                                <td >{{ $order->id }}</td>
                                <td >{{ $order->user_id ? $order->user->name : 'Khách vãng lai' }}

                                        @if($order->pro_type == 2)
                                        (<span style="font-weight: bold">Đơn hàng chuyển nhượng)
                                        @elseif($order->pro_type == 3)
                                                (<span style="font-weight: bold">Sản phẩm đầu vào)
                                        @endif
                                    </span>
                                </td>
                                <td >{{ $order->user_id ? $order->user->username : 'Khách vãng lai' }}</td>
                                <td >{{ ($order->pay_money)? number_format($order->pay_money , 0, ',', '.'): '0'}}đ</td>
                                <td >{{ ($order->total_money)? number_format($order->total_money , 0, ',', '.'): '0'}}đ</td>
                                <td >{{ \Carbon\Carbon::createFromDate($order->created_at)->format('H:i:s d/m/Y') }}</td>
                                <td >{{ ($order->status == 1) ? 'Đã nhận' : 'Đang xử lý' }}</td>
                                <td>
                                    <a class="btn btn-circle btn-sm btn-primary" href="{{ route('ad.order.edit',$order->id)}}">
                                        <i class="fa-solid fa-file"></i>
                                    </a>
                                    <form action="{{route('ad.order.destroy',$order->id)}}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-circle btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                         {{ $orders->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
