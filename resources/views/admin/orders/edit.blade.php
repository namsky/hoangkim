@extends('admin.layouts.master')

@section('page_title', 'Chi tiết đơn hàng')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-default" href="{{ route('ad.order.index') }}" title="">Quay lại</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-8">
                            <table class="table  table-bordered table-striped" id="admin-table">
                                <h5 class="text-center mb-1" style="font-weight:700">Thông tin sản phẩm</h5>
                                <thead>
                                    <tr>
                                        <th class="p-1" width="15%">Tên sản phẩm</th>
                                        <th class="p-1" width="15%">Số lượng</th>
                                        <th class="p-1" width="15%">Giá niêm yết (vnđ)</th>
                                        <th class="p-1" width="15%">Giá đã chiết khấu (vnđ)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php
                                    $total_money = 0;
                                    $total_pay_money = 0;
                                @endphp
                                    @foreach($orderDetails as $orderDetail)
                                        <tr>
                                            <td class="p-1">{{ $orderDetail->product->name }}</td>
                                            <td class="p-1">{{ $orderDetail->amount}}</td>
                                            <td class="p-1">{{ ($orderDetail->product) ? number_format($orderDetail->product->price , 0, ',', '.') : '...'}}đ</td>
                                            <td class="p-1">{{ number_format($orderDetail->pay_money ) }}đ</td>
                                        </tr>
                                        @php
                                            $total_money += $orderDetail->product->price;
                                            $total_pay_money += $orderDetail->pay_money;
                                        @endphp
{{--                                    <tr>--}}
{{--                                        <td colspan="2" class="text-center p-1">Tổng cộng</td>--}}
{{--                                        <td class="p-1">{{ ($orderDetail->pay_money) ? number_format($orderDetail->pay_money , 0, ',', '.') : '...'}}đ</td>--}}
{{--                                    </tr>--}}
                                    @endforeach
                                <tr>
                                    <td colspan="2" style="text-align: right; font-weight: bold">Tổng</td>
                                    <td style="font-weight: bold">{{number_format($total_money) }} đ</td>
                                    <td style="font-weight: bold">{{number_format($total_pay_money)}} đ</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <h5 class="text-center mb-1 font-weight-bold" style="font-weight:700">Thông tin khách hàng</h5>
                            @if ($order->user_id)
                            <ul class="list-group">
                                <li class="list-group-item p-1">
                                    <span style="font-weight:700">Tên:</span> {{ $order->name }}
                                </li>
                                <li class="list-group-item p-1">
                                    <span style="font-weight:700">Email:</span> {{ $order->user->email}}
                                </li>
                                <li class="list-group-item p-1">
                                    <span style="font-weight:700">Số điện thoại:</span> {{ $order->phone }}
                                </li>
                                <li class="list-group-item p-1">
                                    <span style="font-weight:700">Địa chỉ:</span> {{ $order->address}}
                                </li>
                            </ul>
                            @else
                            <ul class="list-group">
                                <li class="list-group-item p-1">
                                    <span style="font-weight:700">Tên:</span> {{ $order->name }}
                                </li>
                                <li class="list-group-item p-1">
                                    <span style="font-weight:700">Số điện thoại:</span> {{ $order->phone }}
                                </li>
                                <li class="list-group-item p-1">
                                    <span style="font-weight:700">Địa chỉ:</span> {{ $order->address}}
                                </li>
                            </ul>
                            @endif
                            <h5 for="status" style="font-weight:700" class="text-center mb-1">Trạng thái đơn hàng</h5>
                            <form action="{{route('ad.order.update',$order->id)}}" id="delete-course-item-form" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn cập nhật trạng thái không?');">
                                @csrf
                                @method('PUT')
                                <div class="form-group actions" >
                                    @if ($order->status == 1)
                                    <select class="custom-select btn  w-75 {{($order->status == 1)? 'btn-success' : 'btn-warning'}}" name="status" id="status">
                                        <option value="1" {{ ($order->status == 1)? 'selected':''}}>Đã nhận</option>
                                    </select>
                                    @else
                                    <select class="custom-select btn  w-75 {{($order->status == 1)? 'btn-success' : 'btn-warning'}}" name="status" id="status">
                                        <option value="0" {{ ($order->status == 0)? 'selected':''}}>Đang xử lý</option>
                                        <option value="1" {{ ($order->status == 1)? 'selected':''}}>Đã nhận</option>
                                    </select>
                                    @endif
                                        <button class="btn btn-primary" style="width:22%" >Cập nhật</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
@endpush

@prepend('scripts')
@endprepend
