@extends('admin.layouts.master')

@section('page_title', 'Danh sách banner')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a href="{{ route('ad.banner.create')}}" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Thêm mới</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <div class="portlet-body" style="background: #fff">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>tên</th>
                            <th width="400">Hình ảnh</th>
                            <th>Trạng thái</th>
                            <th width="160">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($banners as $banner)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{ $banner->name }}</td>
                                <td><img src="{{ $banner->image }}" alt="{{ $banner->name }}" style="width: 100%"></td>
                                <td>{{ $banner->type == 0 ? 'Ẩn' : 'Hiện' }}</td>
                                <td>
                                    <a class="btn btn-circle btn-sm btn-warning" href="{{route('ad.banner.edit', $banner->id)}}"><i class="fa fa-pencil"></i></a>
                                    <form action="{{ route('ad.banner.type', $banner) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn @php echo $banner->type == 0 ?  'ẩn' : 'hiện' @endphp banner?');">
                                        @csrf
                                        @method('PATCH')
                                        <button title="Cập nhật trạng thái banner" class="btn btn-circle btn-sm {{ $banner->type == 0 ? 'btn-primary' : 'btn-info' }}  ">
                                            @if ($banner->type == 1)
                                                <i class="fa fa-eye" aria-hidden="true"></i>                                                
                                            @else
                                                <i class="fa fa-eye-slash" aria-hidden="true"></i>                                                                                                
                                            @endif
                                        </button>
                                    </form>
                                    <form action="{{route('ad.banner.destroy', $banner->id)}}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-circle btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $banners->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
