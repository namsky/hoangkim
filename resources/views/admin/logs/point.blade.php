@extends('admin.layouts.master')

@section('page_title', $title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        {{-- <a href="" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Tạo mới</a> --}}
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <div class="portlet-title">
                </div>
                <div class="portlet-body">
                    <h3 class="mb-1">{{$title}}</h3>
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th width="25%">Tên thành viên</th>
                            <th width="20%">Nội dung</th>
                            <th width="10%">Số điểm</th>
                            <th width="10%">Thời gian</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($datas as $key => $item)
                            <tr>
                                <td >{{ $item->id }}</td>
                                <td >{{ $item->user->username }}</td>
                                <td >{{ $item->reason }}</td>
                                <td >{{$item->status == 0 ? '-' : '+'}}{{ number_format($item->point, 0, ',', '.') }} điểm</td>
                                <td >{{ \Carbon\Carbon::createFromDate($item->created_at)->format('H:i:s d/m/Y') }}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{$datas->links()}}
                        {{-- {{ $items->withQueryString()->links('web.layouts.paginate') }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
