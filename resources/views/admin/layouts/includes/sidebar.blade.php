<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item {{ request()->is('admin') ? 'active' : '' }}">
                <a href="{{ route('ad.index') }}"><i class="fa fa-home"></i> <span class="title">Dashboard</span></a>
            </li>

            <li class="nav-item {{ request()->is('admin/banner*') ? 'active' : '' }}">
                <a href="{{ route('ad.banner.index')}}"><i class="fa fa-object-group" aria-hidden="true"></i>
                    <span class="title">Banner</span>
                </a>
            </li>
            <li class="nav-item {{ request()->is('admin/san-voucher*') ? 'active' : '' }}">
                <a href="{{ route('ad.voucher.index')}}"><i class="fa fa-money-bill" aria-hidden="true"></i>
                    <span class="title">Săn Voucher</span>
                </a>
            </li>


            <li class="nav-item {{ request()->is(['admin/product*','admin/category*']) ? 'active' : '' }}">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="fa-brands fa-product-hunt"></i>
                    <span class="title">Sản phẩm</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{ request()->is(['admin/recharge*']) ? 'active' : '' }}">
                        <a href="{{ route('ad.product.index')}}" class="nav-link">
                            <span class="title">Quản lý sản phẩm</span>
                        </a>
                    </li>
                    <li class="nav-item {{ request()->is(['admin/category*']) ? 'active' : '' }}">
                        <a href="{{ route('ad.category.index') }}" class="nav-link">
                            <span class="title">Danh mục sản phẩm</span>
                        </a>
                    </li>
                </ul>
            </li>
{{--            <li class="nav-item {{ request()->is(['admin/rose-config*','admin/group-bonus*','admin/incomes*']) ? 'active' : '' }}">--}}
{{--                <a href="javascript:void(0);" class="nav-link nav-toggle">--}}
{{--                    <i class="fa-brands fa-product-hunt"></i>--}}
{{--                    <span class="title">Cấu hình</span>--}}
{{--                    <span class="arrow"></span>--}}
{{--                </a>--}}
{{--                <ul class="sub-menu">--}}
{{--                    <li class="nav-item {{ request()->is(['admin/rose-config*']) ? 'active' : '' }}">--}}
{{--                        <a href="{{ route('ad.rose.config') }}" class="nav-link">--}}
{{--                            <span class="title">Thưởng trực tiếp</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item {{ request()->is(['admin/group-bonus*']) ? 'active' : '' }}">--}}
{{--                        <a href="{{ route('ad.group_bonus.config') }}" class="nav-link">--}}
{{--                            <span class="title">Thưởng nhóm theo ngày</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item {{ request()->is(['admin/incomes*']) ? 'active' : '' }}">--}}
{{--                        <a href="{{ route('ad.incomes.config') }}" class="nav-link">--}}
{{--                            <span class="title">Thưởng thu nhập/thu nhập</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

            <li class="nav-item {{ request()->is('admin/order*') ? 'active' : '' }}">
                <a href="{{ route('ad.order.index')}}"><i class="fa fa-file"></i> <span class="title">Đơn hàng</span></a>
            </li>
            <li class="nav-item {{ request()->is(['admin/withdraw*']) ? 'active' : '' }}">
                <a href="{{ route('ad.payment.withdraw') }}">
                    <i class="fa fa-money-bill"></i>  <span class="title">Quản lý Rút tiền</span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('admin/blog/*') ? 'active' : '' }}">
                <a href="{{ route('ad.blog.index')}}"><i class="fa fa-object-group" aria-hidden="true"></i> <span class="title">Tin tức</span></a>
            </li>

            <li class="nav-item {{ request()->is('admin/blog_category*') ? 'active' : '' }}">
                <a href="{{ route('ad.blog_category.index')}}"><i class="fa fa-object-group" aria-hidden="true"></i> <span class="title">Danh mục bài viết</span></a>
            </li>

            <li class="nav-item {{ request()->is('admin/khach-hang-vang-lai*') ? 'active' : '' }}">
                <a href="{{ route('ad.customer.customer')}}"><i class="fa fa-user"></i> <span class="title">Khách hàng vãng lai</span></a>
            </li>

            <li class="nav-item {{ request()->is('admin/customer*') ? 'active' : '' }}">
                <a href="{{ route('ad.customer.index')}}"><i class="fa fa-user"></i> <span class="title">Thành viên</span></a>
            </li>
            <li class="nav-item {{ request()->is('admin/log*') ? 'active' : '' }}">
                <a href="javascript:void(0);" class=" nav-link nav-toggle">
                    <i class="fa-solid fa-clock"></i>
                    <span class="title">Lịch sử giao dịch</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{ request()->is(['admin/log-transaction*']) ? 'active' : '' }}">
                        <a href="{{ route('ad.log.transaction') }}" class="nav-link">
                            <span class="title">Lịch sử hoa hồng</span>
                        </a>
                    </li>
                    <li class="nav-item {{ request()->is(['admin/log-point*']) ? 'active' : '' }}">
                        <a href="{{ route('ad.log.point') }}" class="nav-link">
                            <span class="title">Lịch sử tích điểm</span>
                        </a>
                    </li>
                </ul>
            </li>

{{--            <li class="nav-item {{ request()->is('admin/config*') ? 'active' : '' }}">--}}
{{--                <a href="{{ route('ad.config.index')}}" >--}}
{{--                    <i class="fa fa-object-group" aria-hidden="true"></i>--}}
{{--                    <span class="title">Config</span>--}}
{{--                </a>--}}
{{--            </li>--}}

            {{-- <li class="nav-item {{ request()->is('admin/affiliate*') ? 'active' : '' }}">
                <a href="{{ route('ad.affiliate.index')}}"><i class="fa-solid fa-dollar-sign"></i> <span class="title">Affiliate</span></a>
            </li> --}}

            {{-- <li class="nav-item {{ request()->is('admin/customer*') ? 'active' : '' }}">
                <a href="{{ route('ad.customer.store')}}"><i class="fa-solid fa-user-plus"></i> <span class="title">Thành viên</span></a>
            </li>
            <li class="nav-item {{ request()->is(['admin/recharge*', 'admin/withdraw*']) ? 'active' : '' }}">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="fa-solid fa-money-check-dollar"></i>
                    <span class="title">Quản lý Nạp/Rút</span>
                    <span class="arrow {{ request()->is(['admin/recharge*', 'admin/withdraw*']) ? 'active' : '' }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{ request()->is(['admin/recharge*']) ? 'active' : '' }}">
                        <a href="{{ route('ad.payment.recharge') }}" class="nav-link">
                            <span class="title">Nạp tiền</span>
                        </a>
                    </li>
                    <li class="nav-item {{ request()->is(['admin/withdraw*']) ? 'active' : '' }}">
                        <a href="{{ route('ad.payment.withdraw') }}" class="nav-link">
                            <span class="title">Rút tiền</span>
                        </a>
                    </li>

                </ul>
            </li> --}}

            <li class="nav-item {{ request()->is('admin/admin*') ? 'active' : '' }}">
                <a href="{{ route('ad.admin.index')}}"><i class="fa fa-cogs"></i><span class="title">Admin</span></a>
            </li>
            <!-- END SIDEBAR TOGGLE BUTTON -->
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
