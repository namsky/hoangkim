<!DOCTYPE html>
<!--suppress ALL -->
<html lang="{{ app()->getLocale()}}">

<head>
    <title>@yield('page_title')</title>

    @yield('head')

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:image" content="{{asset('web/images/logo-ocean.jpg')}}">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('web/images/logo-ocean.jpg')}}">

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
        type="text/css" />
    <link href="{{ asset('global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet"
    type="text/css" />
    <link href="{{ asset('global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet"
    type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" href="{{ asset('global/plugins/fontawesome-free-6.2.1-web/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css_account/d-AppContainer.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css_account/d-Deposit.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css_account/d-MemberCenter.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css_account/d-MemberCenterProfile.css') }}">
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>


    @stack('css')
    <style>
        .tox-tinymce-aux {
            z-index: 10000000 !important;
        }

        .hide-not-important {
            display: none;
        }

        .portlet-body .pagination {
            margin: 0 !important;
            float: unset;
        }
    </style>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css" />
</head>

<body
    class="loaded page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white ">
    <div id="box-loading">
        <div class="bg-over-loading" style="left: 0; max-width: 100%;">
            <div class="lds-ellipsis">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
    <div id="app">
        <div class="page-wrapper">

            @include('admin.layouts.includes.header')
            @include('sweetalert::alert')

            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <div class="page-container">
                @include('admin.layouts.includes.sidebar')
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        @include('admin.layouts.includes.breadcrumb')
                       
                        @include('admin.components.errors')

                        @yield('content')

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>

            @include('admin.layouts.includes.footer')
        </div>
    </div>
    
    <!--[if lt IE 9]>
    <script src="{{ asset('global/plugins/respond.min.js') }}"></script>
    <script src="{{ asset('global/plugins/excanvas.min.js') }}"></script>
    <script src="{{ asset('global/plugins/ie8.fix.min.js') }}"></script>
    <![endif]-->

    <script type="text/javascript">
        let baseUrl = '{{ url('/') }}';
        window.flashMessages = [];

        @if ($success = session('success'))
            window.flashMessages = [{
                'type': 'success',
                'message': "{{ $success }}"
            }];
        @elseif ($warning = session('warning'))
            window.flashMessages = [{
                'type': 'warning',
                'message': "{{ $warning }}"
            }];
        @elseif ($error = session('error'))
            window.flashMessages = [{
                'type': 'error',
                'message': "{{ $error }}"
            }];
        @elseif ($info = session('info'))
            window.flashMessages = [{
                'type': 'info',
                'message': "{{ $info }}"
            }];
        @endif
    </script>
    
    <!-- BEGIN CORE PLUGINS -->

    <script src="{{ asset('global/plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>

    <!-- END CORE PLUGINS -->

    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    @stack('scripts')
</body>

</html>
