@extends('admin.layouts.master')

@section('page_title', 'Danh sách đơn Affiliate')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <div class="portlet-title">
                    <form action="{{ route('ad.affiliate.searchAffiliate')}}" method="GET">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" id="exampleInputEmail1" placeholder="Nhập tên cộng tác viên cần tìm..." >
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                            <tr>
                                <th width="7%">Mã đơn hàng</th>
                                <th width="10%">Ngày mua</th>
                                <th width="10%">Số tiền (vnđ)</th>
                                <th width="10%">Cộng tác viên hưởng</th>
                                <th width="10%">Hoa hồng (vnđ)</th>
                                <th width="4%">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($affiliates as $key => $affiliate)
                                @if ($affiliate->user != null)
                                    @if ($affiliate->order)
                                        <tr>
                                            <td >{{ $affiliate->order->id}}</td>
                                            <td >{{ \Carbon\Carbon::createFromDate($affiliate->order->created)->format('d/m/Y') }}</td>
                                            <td >{{ number_format($affiliate->order->pay_money , 0, ',', '.') }}</td>
                                            <td >{{ $affiliate->user->name }}</td>
                                            <td >{{ number_format($affiliate->money , 0, ',', '.')}}</td>
                                            <td>
                                                <form action="{{ route('ad.affiliate.destroy', $affiliate->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-circle btn-sm btn-danger" title="Xóa đơn Affiliate"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td >Đơn hàng đã bị xóa</td>
                                            <td >....</td>
                                            <td >....</td>
                                            <td >{{ $affiliate->user->name }}</td>
                                            <td >{{ number_format($affiliate->money , 0, ',', '.')}}</td>
                                            <td>
                                                <form action="{{ route('ad.affiliate.destroy', $affiliate->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-circle btn-sm btn-danger" title="Xóa đơn Affiliate"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endif
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{-- {{ $affiliates->withQueryString()->links('web.layouts.paginate') }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
