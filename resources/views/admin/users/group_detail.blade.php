@extends('admin.layouts.master')

@section('page_title', 'Chi tiết '.$group_name)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        {{-- <a href="" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Tạo mới</a> --}}
                        {{-- <a href="{{ route('ad.export') }}" class="btn btn-success mx-5">Xuất File Excel</a> --}}
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen ml-5" href="javascript:void(0);" title=""></a>
                    </div>
                </div>

                <div class="portlet-body">
                    <table id="thistory" class="table table-bordered table-striped bg-white table-hover" style="font-size:15px; min-width:1000px">
                        <thead>
                          <tr>
                            <th style="min-width:120px;">ID Thành viên</th>
                            <th style="min-width:120px;">Tên đại lý</th>
                            <th style="min-width:120px;">Số điện thoại</th>
                            <th style="min-width:120px;">Email</th>
                            <th style="min-width:120px;">Địa chỉ</th>
                            <th style="min-width:120px;">Doanh số</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($group_members as $member)
                            <tr>
                                <td>{{ $member->members[0]->id }}</td>
                                <td>{{ $member->members[0]->name }}</td>
                                <td>{{ $member->members[0]->phone ? $member->members[0]->phone : 'Chưa cập nhật' }}</td>
                                <td>{{ $member->members[0]->email }}</td>
                                <td>{{ $member->members[0]->address ? $member->members[0]->address : 'Chưa cập nhật' }}</td>
                                <td>{{ number_format($member->members[0]->order()->where('status', 1)->sum('total_money') , 0, ',', '.') }} ₫</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
