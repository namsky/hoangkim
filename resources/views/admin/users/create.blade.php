@extends('admin.layouts.master')

@section('page_title', 'Tạo mới Khách Hàng')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        {{-- <a class="btn btn-circle btn-default" href="{{ route('ad.user.index') }}" title="">Quay lại</a> --}}
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('ad.user.store') }}" method="POST" @submit.prevent="onSubmit" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" :class="[errors.has('name') ? 'has-error' : '']">
                                    <label for="name">Tên <span class="required">*</span></label>
                                    <input type="text" id="name" class="form-control" name="name" placeholder="Nhập tên..." v-validate="'required'" data-vv-as="&quot;Tên&quot;" value="">
                                    <span class="help-block" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('username') ? 'has-error' : '']">
                                    <label for="name">Tên đăng nhập<span class="required">*</span></label>
                                    <input type="text" id="username" class="form-control" name="username" placeholder="Nhập tên đăng nhập..." v-validate="'required'" data-vv-as="&quot;Tên đăng nhập&quot;" value="">
                                    <span class="help-block" v-if="errors.has('username')">@{{ errors.first('username') }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="gender">Giới tính</label>
                                    <select class="form-control" name="gender" id="gender">
                                        <option value="1" selected>Nam</option>
                                        <option value="0" >Nữ</option>
                                    </select>
                                </div>
                                <div class="form-group" :class="[errors.has('email') ? 'has-error' : '']">
                                    <label for="email">Email <span class="required">*</span></label>
                                    <input type="text" id="email" class="form-control" v-validate="'required'" name="email" value="" data-vv-as="&quot;Email&quot;" placeholder="Nhập email...">
                                    <span class="help-block" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('phone') ? 'has-error' : '']">
                                    <label for="phone">Số điện thoại <span class="required">*</span></label>
                                    <input type="text" id="phone" class="form-control" v-validate="'required'"  name="phone" value="" data-vv-as="&quot;Số điện thoại&quot;" placeholder="Nhập số điện thoại...">
                                    <span class="help-block" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('address') ? 'has-error' : '']">
                                    <label for="address">Địa chỉ</label>
                                    <input type="text" id="address" class="form-control" v-validate="'required'" name="address" data-vv-as="&quot;Địa chỉ&quot;" value="" placeholder="Nhập địa chỉ...">
                                    <span class="help-block" v-if="errors.has('address')">@{{ errors.first('address') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('password') ? 'has-error' : '']">
                                    <label for="password">Mật khẩu </label>
                                    <input type="text" id="password" class="form-control" v-validate="'required'" data-vv-as="&quot;Mật khẩu&quot;"  name="password"  value="" placeholder="Nhập mật khẩu..."> 
                                    <span class="help-block" v-if="errors.has('password')">@{{ errors.first('password') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                            <a href="{{ route('ad.user.index') }}" class="btn btn-default">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
@endpush

@prepend('scripts')
@endprepend

