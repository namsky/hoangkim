@extends('admin.layouts.master')

@section('page_title', 'Danh sách các Khách Hàng vãng lai')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        {{-- button add new user --}}
{{--                        <a href="{{route('ad.user.create')}}" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Thêm mới</a>--}}
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                {{-- Search info user --}}
{{--                <div class="portlet-title">--}}
{{--                    <form action="{{ route('ad.user.index')}}" method="GET">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-10">--}}
{{--                                <div class="form-group">--}}
{{--                                    <input type="text" name="name" class="form-control" placeholder="Nhập tên khách hàng cần tìm ..." value="{{ old('name') }}" id="exampleInputEmail1" >--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-2">--}}
{{--                                <div class="form-group">--}}
{{--                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
                {{-- table info users --}}
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                        <tr>
                            <th width="1%">ID</th>
                            <th width="10%">Họ tên</th>
                            <th width="7%">Email</th>
                            <th width="7%">SĐT</th>
                            <th width="25%">Địa chỉ</th>
                            <th width="3%">Ngày tạo</th>
{{--                            <th width="5%">Action</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($customers as $key => $user)
                            <tr>
                                <td >{{ $user->id}}</td>
                                <td >{{ $user->name }}</td>
                                <td >{{ $user->phone }}</td>
                                <td >{{ $user->email }}</td>
                                <td >{{ $user->address }}</td>
                                <td >{{ \Carbon\Carbon::createFromDate($user->created_at)->format('d/m/Y') }}</td>
{{--                                <td>--}}
{{--                                    <a title="Sửa thông tin khách hàng" class="btn btn-circle btn-sm btn-warning" href="{{ route('ad.user.edit', $user->id) }}"><i class="fa fa-pencil"></i></a>--}}
{{--                                    <form action="{{ route('ad.user.destroy', $user->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">--}}
{{--                                        @csrf--}}
{{--                                        @method('DELETE')--}}
{{--                                        <button class="btn btn-circle btn-sm btn-danger" title="Xóa khách hàng"><i class="fa fa-trash"></i></button>--}}
{{--                                    </form>--}}
{{--                                </td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $customers->withQueryString()->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
