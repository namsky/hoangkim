@extends('admin.layouts.master')

@section('page_title', 'Sửa thông tin Khách Hàng')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        {{-- <a class="btn btn-circle btn-default" href="{{ route('ad.user.index') }}" title="">Quay lại</a> --}}
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('ad.user.update', $user) }}" method="POST" @submit.prevent="onSubmit" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group" :class="[errors.has('name') ? 'has-error' : '']">
                                    <label for="name">Tên <span class="required">*</span></label>
                                    <input type="text" id="name" class="form-control" name="name" v-validate="'required'" data-vv-as="&quot;Tên&quot;" value="{{ old('name', $user->name) }}">
                                    <span class="help-block" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('username') ? 'has-error' : '']">
                                    <label for="username">Tên <span class="required">*</span></label>
                                    <input type="text" id="username" class="form-control" name="username" v-validate="'required'" data-vv-as="&quot;Tên&quot;" value="{{ old('username', $user->username) }}">
                                    <span class="help-block" v-if="errors.has('username')">@{{ errors.first('username') }}</span>
                                </div>
                                <div class="form-group">username
                                    <label for="gender">Giới tính</label>
                                    <select class="form-control" name="gender" id="gender">
                                        <option value="1" {{$user->gender == 1 ? 'selected' : ''}}>Nam</option>
                                        <option value="0" {{$user->gender == 0 ? 'selected' : ''}}>Nữ</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email <span class="required">*</span></label>
                                    <input type="text" id="email" class="form-control" disabled name="email" value="{{ old('email', $user->email) }}">
                                    <span class="help-block" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('phone') ? 'has-error' : '']">
                                    <label for="phone">Số điện thoại <span class="required">*</span></label>
                                    <input type="text" id="phone" class="form-control" v-validate="'required'"  name="phone" value="{{ old('phone', $user->phone) }}">
                                    <span class="help-block" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('address') ? 'has-error' : '']">
                                    <label for="address">Địa chỉ</label>
                                    <input type="text" id="address" class="form-control" v-validate="'required'" name="address" data-vv-as="&quot;SĐT&quot;" value="{{ old('address', $user->address) }}">
                                    <span class="help-block" v-if="errors.has('address')">@{{ errors.first('address') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('password') ? 'has-error' : '']">
                                    <label for="password">Mật khẩu </label>
                                    <input type="text" id="password" class="form-control" v-validate="'required'" name="password"  value="{{ old('password') }}">
                                    <span class="help-block" v-if="errors.has('password')">@{{ errors.first('password') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-primary">Cập nhật</button>
                            <a href="{{ route('ad.user.index') }}" class="btn btn-default">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
@endpush

@prepend('scripts')
@endprepend
