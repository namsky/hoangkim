@extends('admin.layouts.master')

@section('page_title', 'Danh sách đội nhóm')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        {{-- <a href="" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Tạo mới</a> --}}
                        {{-- <a href="{{ route('ad.export') }}" class="btn btn-success mx-5">Xuất File Excel</a> --}}
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen ml-5" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <div class="portlet-title">
                    <form action="{{route('ad.order.searchOrder')}}" method="GET">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" name="code" class="form-control" value="{{ old('code') }}" id="exampleInputEmail1" placeholder="Nhập mã đơn hàng...">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form> 
                </div>

                <div class="portlet-body">
                    <table id="thistory" class="table table-bordered table-striped bg-white table-hover" style="font-size:15px; min-width:1000px">
                        <thead>
                          <tr>
                            <th style="min-width:120px;">STT</th>
                            <th style="min-width:120px;">Tên đội nhóm</th>
                            <th style="min-width:120px;">Doanh số</th>
                            <th style="min-width:120px;">Số thành viên</th>
                            <th style="min-width:120px;">Xem chi tiết</th>
                          </tr>
                        </thead>
                        <tbody>
                          @php
                              $i = 1;
                          @endphp
                          @foreach ($groupData as $groupName => $data)
                          <tr>
                              <td>{{ $i }}</td>
                              <td>{{ $groupName }}</td>
                              <td>{{ number_format($data[0]->money , 0, ',', '.') }}đ</td>
                              <td>{{ $data->count() }}</td>
                              <td>
                                <a href="{{ route('ad.group.group_detail', $groupName) }}" class="btn btn-primary">Chi tiết</a>
                              </td>
                              </tr>
                              @php
                              $i++;
                          @endphp
                          @endforeach
                        </tbody>
                      </table>
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
