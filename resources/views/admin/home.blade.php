@extends('admin.layouts.master')

@section('page_title', 'Trang tổng quan')

@section('content')
    <div class="row">
    <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        {{-- <span class="caption-subject bold uppercase">@yield('page_title')</span> --}}
                        <span class="caption-subject bold uppercase"> Ngày: @php echo date('d/m/Y') @endphp</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row d-flex justify-content-center ">
                        <div class="col-md-6">
                            <div class="card border-0 mb-1" style="color:white !important; background: #364150!important;border-radius: 6px;">
                                <div  class="card-body p-2 text-center">
                                    <h3 class="" style="font-weight:700">Tổng số tiền đơn hàng đã duyệt</h3>
                                    <h4 style="font-weight:700; padding-top:20px">{{number_format($orders->where('status',1)->sum('pay_money'), 0, ',', '.')}} VNĐ</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card border-0 mb-1" style="color:white !important; background: #364150!important;border-radius: 6px;">
                                <div  class="card-body p-2 text-center">
                                    <h3 class="" style="font-weight:700">Tổng số tiền đơn hàng chưa duyệt</h3>
                                    <h4 style="font-weight:700; padding-top:20px">{{number_format($orders->where('status',0)->sum('pay_money'), 0, ',', '.')}} VNĐ</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card border-0 mb-1" style="color:white !important; background: #364150!important;border-radius: 6px;">
                                <div  class="card-body p-2 text-center">
                                    <h3 class="" style="font-weight:700">Số thành viên đăng ký</h3>
                                    <h4 style="font-weight:700; padding-top:20px">{{ $allUser->count()}} Đại lý</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card border-0 mb-1" style="color:white !important; background: #364150!important;border-radius: 6px;">
                                <div  class="card-body p-2 text-center">
                                    <h3 class="" style="font-weight:700">Số thành viên hoạt động</h3>
                                    <h4 style="font-weight:700; padding-top:20px">{{ $allUser->where('type',1)->count()}} Đại lý</h4>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <hr>
                    
                </div>
            </div>
        </div>
    </div>

@endsection
