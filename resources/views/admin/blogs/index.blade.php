@extends('admin.layouts.master')

@section('page_title', 'Danh sách bài viết')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a href="{{ route('ad.blog.create')}}" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Thêm mới</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <div class="portlet-title">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" placeholder="Nhập tên bài viết cần tìm..." name="name" class="form-control" value="{{ old('name') }}" id="name" >
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body" style="background: #fff">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                        <tr>
                            <th width="3%">ID</th>
                            <th width="9%">Ảnh</th>
                            <th>Tên bài viết</th>
                            <th>Mô tả</th>
                            <th width="10%">Danh mục</th>
                            <th width="9%">Slug</th>
                            <th width="110">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($blogs as $key => $blog)
                            <tr>
                                <td>{{ $blog->id }}</td>
                                <td><img src="{{$blog->image}}" style="max-width: 25px" alt=""></td>
                                <td>{{ $blog->name }}</td>
                                <td>{{ $blog->description }}</td>
                                <td>{{ $blog->category ? $blog->category->name : 'Chưa phân loại' }}</td>
                                <td>{{ $blog->slug }}</td>
                                <td>
                                    <a class="btn btn-circle btn-sm btn-warning" href="{{ route('ad.blog.edit', $blog) }}"><i class="fa fa-pencil"></i></a>
                                    <form action="{{ route('ad.blog.destroy', $blog) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-circle btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{-- <div class="text-center">
                        {{ $blogs->links('web.layouts.paginate') }}
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
