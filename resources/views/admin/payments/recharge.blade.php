@extends('admin.layouts.master')

@section('page_title', 'Quản lý Rút tiền')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <hr>
                <hr>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                        <tr>
                            <th width="3%">ID</th>
                            <th width="10%">User Id</th>
                            <th width="10%">Tên KH</th>
                            <th width="10%">Số tiền</th>
                            <th width="15%">Method</th>
                            <th width="7%">Order Id</th>
                            <th width="7%">Trạng thái</th>
                            <th width="7%">Action</th>
                            <th width="10%">Ngày tạo</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($payments as $payment)
                            <tr>
                                <td >{{ $payment->id }}</td>
                                <td >{{ $payment->user->id }}</td>
                                <td >{{ $payment->user->name }}</td>
                                <td >{{ number_format($payment->amount) }} VNĐ</td>
                                <td >{{ $payment->method }}</td>
                                <td >{{ $payment->oder_id }}</td>
                                <td >
                                    @if($payment->status == 0)
                                        <p class="font-weight-bold text-danger">Chưa thành công</p>
                                    @elseif($payment->status == 1)
                                        <p class="font-weight-bold text-success">Thành công</p>
                                    @elseif($payment->status == 2)
                                        <p class="font-weight-bold text-info">Chưa duyệt</p>
                                    @elseif($payment->status == 3)
                                        <p class="font-weight-bold text-primary">Đã hủy</p>
                                    @endif
                                </td>
                                <td >
                                @if($payment->status == 2)
                                    <form action="{{ route('ad.payment.post_recharge', $payment->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn duyệt giao dịch này?');">
                                        @method('PATCH')
                                        @csrf
                                        <input type="hidden" name="type" value="0">
                                        <button type="submit" class="btn btn-success">Duyệt</button>
                                    </form>
                                    <form action="{{ route('ad.payment.post_recharge', $payment->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn hủy giao dịch này?');">
                                         @method('PATCH')
                                        @csrf
                                        <input type="hidden" name="type" value="1">
                                        <button class="btn btn-danger">Hủy</button>
                                    </form>
                                @endif
                                </td>
                                <td >{{ \Carbon\Carbon::createFromDate($payment->created_at)->format('H:i:s d/m/Y') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $payments->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
