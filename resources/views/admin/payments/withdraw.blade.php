@extends('admin.layouts.master')

@section('page_title', 'Quản lý Rút tiền')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <hr>
                <hr>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                        <tr>
                            <th width="3%">ID</th>
                            <th width="10%">Tài khoản</th>
                            <th width="15%">Tên tài khoản</th>
                            <th width="10%">Ngân hàng</th>
                            <th width="15%">Số tài khoản</th>
                            <th width="10%">Số tiền yêu cầu</th>
                            <th width="7%">Trạng thái</th>
                            <th width="10%">Ngày tạo</th>
                            <th width="10%">Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($payments as $payment)
                            <tr>
                                <td >{{ $payment->id }}</td>
                                <td >{{ $payment->user->name }}</td>
                                <td >{{ $payment->customer_bank->name }}</td>
                                <td >{{ $payment->customer_bank->bank }}</td>
                                <td >{{ $payment->customer_bank->number }}</td>
                                <td >{{ number_format($payment->amount) }} VNĐ</td>
                                <td >
                                    @if($payment->status == 0) Chưa duyệt
                                    @elseif($payment->status == 1) Đã duyệt
                                    @else Đã hủy
                                    @endif
                                </td>
                                <td >{{ \Carbon\Carbon::createFromDate($payment->created_at)->format('d/m/Y') }}</td>
                                <td>
                                    @if($payment->status == 0)
                                        <form action="{{ route('ad.payment.post_withdraw', $payment->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn duyệt giao dịch này?');">
                                            @csrf
                                            @method('PATCH')
                                            <input type="hidden" name="type" value="0">
                                            <button class="btn btn-circle btn-sm btn-danger">Duyệt</button>
                                        </form>
                                        <form action="{{ route('ad.payment.post_withdraw', $payment->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn hủy và hoàn tiền giao dịch này?');">
                                            @csrf
                                            @method('PATCH')
                                            <input type="hidden" name="type" value="1">
                                            <button class="btn btn-circle btn-sm btn-danger">Hủy</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $payments->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
