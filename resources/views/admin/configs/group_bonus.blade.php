@extends('admin.layouts.master')

@section('page_title', 'Cấu hình thưởng nhóm theo ngày')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Chức vụ</th>
                                    <th>Phần trăm</th>
                                    <th width="10%">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($roses as $rose)
                                <tr>
                                    <td>{{ $rose->id }}</td>
                                    <td>{{ $rose->grant->name }}</td>
                                    <td>{{ $rose->percent }}</td>
                                    <td><a class="btn btn-primary edit-btn" 
                                        data-toggle="modal" 
                                        data-target="#editmodal"
                                        data-id="{{ $rose->id }}"
                                        data-name="{{ $rose->grant->name }}" 
                                        data-percent="{{ $rose->percent }}">Chỉnh sửa</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="editmodalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Chỉnh sửa thưởng trực tiếp</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('ad.group_bonus.update') }}" method="POST">
                    @csrf
                    {{-- @method('PUT') --}}
                    <input type="hidden" id="modal-id" name="rose_id">
                    <div class="form-group">
                        <label for="modal-user-name">Chức vụ</label>
                        <input type="text" class="form-control" id="modal-name" readonly>
                    </div>
                    <div class="form-group">
                        <label for="modal-user-email">Phần trăm thưởng doanh số</label>
                        <input type="number" class="form-control" id="modal-percent" name="percent">
                    </div>
                    <div class="float-left left">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                      <button type="submit" class="btn btn-primary">Cập nhật</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>

@endsection

@push('css')
@endpush

@push('scripts')
<script>
    $(document).ready(function() {
        $(".edit-btn").click(function() {
            var Id = $(this).data('id');
            var name = $(this).data('name');
            var percent = $(this).data('percent');

            // Đưa thông tin vào modal
            $("#modal-id").val(Id);
            $("#modal-name").val(name);
            $("#modal-percent").val(percent);

            // Hiển thị modal
            // $("#editmodal").modal('show');
        });
    });
</script>
@endpush
