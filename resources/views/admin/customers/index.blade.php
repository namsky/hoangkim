@extends('admin.layouts.master')

@section('page_title', 'Danh sách thành viên')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        {{-- <a href="{{route('ad.customer.create')}}" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Tạo mới</a> --}}
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <div class="portlet-title">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group col-md-3 pl-0">
                                    <select name="find_option" id="" class="form-select form-control">
                                        <option value="username">Tìm theo username</option>
                                        <option value="phone">Tìm theo SĐT</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" placeholder="Nhập username hoặc số điện thoại cần tìm..." name="data" class="form-control" value="{{ old('name') }}" id="exampleInputEmail1" >
                                </div>
                                <div class="form-group col-md-3">
                                    <select name="rate" id="" class="form-control">
                                        <option value="">Tất cả chức vụ</option>
                                        <option value="0">Thành viên mới</option>
                                        <option value="1">Cộng tác viên</option>
                                        <option value="2">Đại lý xã</option>
                                        <option value="3">Đại lý huyện</option>
                                        <option value="4">Đại lý tỉnh</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                            <tr>
                                <th width="1%">ID</th>
                                <th width="10%">Họ tên</th>
                                <th width="10%">Tên ĐN</th>
                                <th width="10%">Danh hiệu</th>
                                <th width="7%">Email</th>
                                <th width="3%">SĐT</th>
                                <th width="7%">Doanh số cá nhân</th>
                                <th width="7%">Doanh số Trực tiếp</th>

                                {{-- <th width="20%">Địa chỉ</th> --}}
                                <th width="7%">Số dư ví</th>
                                <th width="5%">Cấp trên</th>
                                <th width="5%">Số đại lý con</th>
                                <th width="5%">Số nhóm đã tạo</th>
                                {{-- <th width="5%">Ngày tạo</th> --}}
                                <th width="7%">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customers as $key => $customer)
                            <tr>
                                <td >{{ $customer->id}}</td>
                                <td >{{ $customer->name }}</td>
                                <td >{{ $customer->username }}</td>
                                <td >@switch($customer->rate)
                                        @case(1)
                                        Cộng tác viên
                                        @break
                                        @case(2)
                                        Đại lý xã
                                        @break
                                        @case(3)
                                        Đại lý huyện
                                        @break
                                        @case(4)
                                        Đại lý tỉnh
                                        @break
                                        @default
                                        Thành viên mới
                                    @endswitch</td>
                                <td >{{ $customer->email }}</td>
                                <td >{{ $customer->phone ? $customer->phone : 'Chưa cập nhật' }}</td>
                                <td >{{ number_format( $customer->order()->where('status',1)->sum('total_money'), 0, ',','.') }}đ</td>
                                <td >0 đ</td>

                                {{-- <td >{{ $customer->address }}</td> --}}
                                <td >{{ number_format($customer->wallet1) }} Vnđ</td>
                                <td >{{ $customer->father ? $customer->father->name : 'Không có' }} </td>
                                <td >{{ number_format($customer->total_child) }}</td>
                                <td >{{ number_format($customer->groups) }}</td>
                                {{-- <td >{{ \Carbon\Carbon::createFromDate($customer->created_at)->format('d/m/Y') }}</td> --}}
                                <td>
                                    <a title="Sửa thông tin cộng tác viên" class="btn btn-circle btn-sm btn-warning" href="{{ route('ad.customer.edit',$customer->id) }}"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" data-user-id="{{ $customer->id }}">Cộng/trừ tiền</button>
                                    {{-- <form action="{{ route('ad.customer.destroy', $customer->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-circle btn-sm btn-danger" title="Xóa cộng tác viên"><i class="fa fa-trash"></i></button>
                                    </form> --}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $customers->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <b><h3 class="modal-title" id="exampleModalLongTitle">Cộng/ trừ tiền</h3></b>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('ad.customer.money') }}" method="POST">
                    @csrf
                    <input type="hidden" name="user_id" id="user_id">
                    <div class="form-group">
                        <label for="">Hành động</label>
                        <select name="type" id="" class="form-control">
                            <option value="0">Cộng tiền</option>
                            <option value="2">Cộng tiền hoa hồng</option>
                            <option value="1">Trừ tiền</option>
                        </select>
                    </div>
                    <div class="form-group" class="money">
                        <label for="">Số tiền</label>
                       <input type="number" name="money" class="form-control">
                    </div>

                    <button type="submit" class="btn btn-primary">Thực hiện</button>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <script>
        $('#exampleModalCenter').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button
            var userId = button.data('user-id'); // Lấy id của user từ thuộc tính data-user-id của button
            var modal = $(this);
            modal.find('#user_id').val(userId); // Đưa id của user vào trường ẩn trong form
            var type = $('select').val();
            // if(type == 2){
            //     $('.money').hide();
            // }
            // alert(type);
        });
    </script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
