@extends('admin.layouts.master')

@section('page_title', 'Danh sách sản phẩm')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a href="{{ route('ad.product.create')}}" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Thêm mới</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <div class="portlet-title">
                    <form action="{{ route('ad.product.searchProduct')}}" method="GET">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="text" placeholder="Nhập tên sản phẩm cần tìm..." name="name" class="form-control" value="{{ old('name') }}" id="name" >
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="portlet-body" style="background: #fff">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                        <tr>
                            <th width="3%">Mã</th>
                            <th>Ảnh</th>
                            <th>Tên sản phẩm</th>
                            <th>Slug</th>
                            <th width="10%">Danh mục</th>
                            <th width="9%">Giá </th>
                            <th width="9%">Đã bán </th>
                            <th width="9%">Loại </th>
{{--                            <th width="250">Mô tả</th>--}}
                            {{-- <th width="150">Thể loại</th> --}}
                            <th width="160">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $key => $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td><img src="{{ $product->image }}" style="max-width: 25px" alt=""></td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->slug }}</td>
                                <td>{{ $product->category_id !=0 ? $product->category->category_name : "Chưa phân loại" }}</td>
                                <td>{{ ($product->price)? number_format($product->price , 0, ',', '.').' vnđ' : '...'}}</td>
                                <td>{{ $product->product_buy($product->id) }}</td>
                                 <td>
                                     @if($product->type == 1)
                                         Sản phẩm lẻ
                                     @elseif($product->type == 2)
                                         Đơn hàng chuyển nhượng
                                     @elseif($product->type == 3)
                                         Sản phẩm đầu vào
                                     @endif
                                 </td>
{{--                                <td>--}}
{{--                                    <div class="form-check form-switch">--}}
{{--                                        <input style="cursor: pointer;" class="form-check-input mx-auto status" type="checkbox" data-target="{{$product->id}}" id="status" name="status" {{$product->hot == 1 ? 'checked' : ''}}>--}}
{{--                                    </div>--}}
{{--                                    <div class="custom-control custom-switch">--}}
{{--                                        <input type="checkbox" class="custom-control-input hot" data-target="{{$product->id}}" id="hot{{$product->id}}" name="hot" {{$product->hot == 1 ? 'checked' : ''}}>--}}
{{--                                        <label class="custom-control-label" for="hot{{$product->id}}">HOT</label>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
                                <td>
                                    <a class="btn btn-circle btn-sm btn-warning" href="{{ route('ad.product.edit', $product) }}"><i class="fa fa-pencil"></i></a>
                                    <form action="{{ route('ad.product.destroy', $product) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-circle btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                    <form id="status-form" action="{{ route('ad.product.status', $product->id) }}" method="POST" style="display: inline-block">
                                        @csrf
                                        <button type="submit" class="btn btn-circle btn-sm btn-primary" data-url="">
                                            <i class="fa-solid {{ $product->status == '0' ? 'fa-eye-slash' : 'fa-eye'}}"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$products->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.2.1/dist/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->

@endpush

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.6/dist/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.2.1/dist/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script>
        $(".hot").change(function () {
            var pro_id = $(this).attr('data-target');
            console.log(pro_id);
            $.ajax({
                type: "GET",
                url: '{{route('ad.pro.hot')}}',
                data: {
                    pro_id : pro_id
                }, // serializes the form's elements.
                success: function(res)
                {

                }
            });
        });
    </script>
@endpush
