@extends('admin.layouts.master')

@section('page_title', 'Danh sách mua Voucher')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        {{-- button add new user --}}
{{--                        <a href="{{route('ad.user.create')}}" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Thêm mới</a>--}}
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                        <tr>
                            <th width="1%">Mã giao dịch</th>
                            <th width="10%">Thành viên mua</th>
                            <th width="10%">Số voucher</th>
                            <th width="10%">Thành viên VNĐ</th>
                            <th width="4%">Bill chuyển khoản</th>
                            <th width="7%">Trạng thái</th>
                            <th width="3%">Ngày tạo</th>
                            <th width="6%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($vouchers as $key => $voucher)
                            <tr>
                                <td >#{{ $voucher->id}}</td>
                                <td >{{ $voucher->user->name }} ({{ $voucher->user->username }})</td>
                                <td >{{number_format( $voucher->money/1000) }}</td>
                                <td >{{ number_format($voucher->money )}} VNĐ</td>
                                <td >
                                    @if($voucher->bill)
                                        <img style="width: 300px" src="{{ $voucher->bill }}" alt="">
                                    @else
                                        Chưa có
                                    @endif
                                </td>
                                <td >
                                    @if($voucher->status == 0)
                                       Chưa duyệt
                                    @elseif($voucher->status == 1)
                                        <span style="color: green">Đã duyệt</span>
                                    @else
                                        <span style="color: red">Đã hủy</span>
                                    @endif
                                </td>
                                <td >{{ \Carbon\Carbon::createFromDate($voucher->created_at)->format('d/m/Y') }}</td>
                                <td>
                                    @if($voucher->status == 0)
                                        <form action="{{ route('ad.voucher.confirm', $voucher->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn duyệt?');">
                                            @csrf
                                            <button class="btn btn-circle btn-sm btn-success" title="Xóa khách hàng">Duyệt</button>
                                        </form>
                                    <form action="{{ route('ad.voucher.destroy', $voucher->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn hủy?');">
                                        @csrf
                                        <button class="btn btn-circle btn-sm btn-danger" title="Xóa khách hàng">Hủy</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $vouchers->withQueryString()->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
