@extends('customer.layouts.master')

@section('page_title', 'Lịch sử giao dịch')

@section('page_id', 'page-order')

@section('content')
<section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
  <!-- Main content -->
  <section class="content" style="padding:0px;">
    <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
      <h4 class="mb-2">Lịch sử giao dịch</h4>
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-md-12"></div>
                <input type="hidden" name="ctl27$txtQuantity" id="ctl27_txtQuantity">
                {{-- <div class="col-md-12"> Đơn hàng của bạn </div> --}}
                <div style="min-width:100%;overflow-x:auto;">
                  <table id="thistory" class="table table-bordered table-striped bg-white table-hover" style="font-size:15px; min-width:1000px">
                    <thead>
                      <tr>
                        <th style="min-width:120px;">STT</th>
                        <th style="min-width:120px;">Số điểm trước thay đổi</th>
                        <th style="min-width:120px;">Số điểm thay đổi</th>
                        <th style="min-width:120px;">Số điểm sau thay đổi</th>
                        <th style="min-width:120px;">Lí do</th>
                        <th style="min-width:120px;">Thời gian</th>
                      </tr>
                    </thead>
                    <tbody>
                        @php
                            $i=1;
                        @endphp
                      @foreach ($histories as $item)
                          <tr>
                              <td>{{ $i }}</td>
                              <td>{{ number_format( $item->before, 0, ',','.')}}đ</td>
                              @if ($item->status)
                              <td class="bold text-success">+{{ number_format( $item->money, 0, ',','.')}}đ</td>
                              @else
                              <td class="bold text-danger">-{{ number_format( $item->money, 0, ',','.')}}đ</td>
                              @endif
                              <td>{{ number_format( $item->after, 0, ',','.')}}đ</td>
                              <td>{{ $item->reason}}</td>
                              <td>{{ \Carbon\Carbon::createFromDate($item->created_at)->format('H:i:s d/m/Y') }}</td>
                          </tr>
                          @php
                            $i++;
                          @endphp
                      @endforeach
                      @if ($histories->count()==0)
                      <tr>
                        <td class="text-center" colspan="10">Chưa có dữ liệu để hiển thị</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal" id="mdOrder" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999999">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content" style="width:100%;">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"> Thông tin đơn hàng </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 mb-2" style="line-height:40px;" id="orderInfo"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <style type="text/css">
        .dfl {
          display: flex;
          justify-content: space-between;
        }
      </style>
      <script src="/Scripts/pachis.js?v=0.1"></script>
    </div>
  </section>
</section>
@endsection
