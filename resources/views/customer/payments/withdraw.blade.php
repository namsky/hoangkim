@extends('customer.layouts.master')

@section('page_title', 'Rút điểm')

@section('page_id', 'page-order')

@section('content')
    <section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
        <!-- Main content -->
        <section class="content" style="padding:0px;">
            <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
                <h4>Rút điểm</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8 col-12 pt-2">
                                        <h4 class="text-success text-center">Rút điểm (Chi phí 15.6%)</h4>
                                        <p class="text-center"><i>15.6% bao gồm : 10% thuế TNCN + 5.6% phí rút điểm</i></p>
                                        <form class="form-info-bank new" method="POST" action="{{route('us.payment.post_withdraw')}}">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-3 col-form-label font-weight-bold">Số điểm muốn rút</label>
                                                <div class="col-sm-9">
                                                    <input type="number" min="100000" class="form-control" required name="amount" id="amount" placeholder="Số điểm rút tối thiểu : 100.000">
                                                    <ul>
                                                        <li style="list-style-type: circle;"><i>Thuế TNCN = <span class="ttncn">0</span> điểm</i></li>
                                                        <li style="list-style-type: circle;"><i>Phí rút điểm = <span class="prd">0</span> điểm</i></li>
                                                        <li style="list-style-type: circle;"><i>Tổng trừ = <span class="totalt">0</span> điểm</i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-primary my-3" value="Xác nhận">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
@push('css')
@endpush
@push('scripts')
    <script>
        $("#amount").keyup(function () {
            var amount = parseInt($(this).val());
            var ttncn = (amount*10)/100;
            var prd = (amount*6.5)/100;
            $(".ttncn").html(addCommas(ttncn));
            $(".prd").html(addCommas(prd));
            $(".totalt").html(addCommas(amount+ttncn+prd));

        })

        // Format Number
        function addCommas(str) {
            var amount = new String(str);
            amount = amount.split("").reverse();

            var output = "";
            for (var i = 0; i <= amount.length - 1; i++) {
                output = amount[i] + output;
                if ((i + 1) % 3 == 0 && (amount.length - 1) !== i)
                    output = '.' + output;
            }
            return output;
        }
    </script>


@endpush

