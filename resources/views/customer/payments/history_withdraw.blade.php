@extends('customer.layouts.master')

@section('page_title', 'Lịch sử rút điểm')

@section('page_id', 'page-order')

@section('content')
    <section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
        <!-- Main content -->
        <section class="content" style="padding:0px;">
            <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
                <h4>Lịch sử rút điểm</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12"></div>
                                    <input type="hidden" name="ctl27$txtQuantity" id="ctl27_txtQuantity">
                                    <div class="col-md-12">
                                        <table id="thistory" class="table table-bordered table-striped bg-white table-hover" style="font-size:15px; min-width:1000px">
                                            <thead>
                                            <tr>
                                                <th style="min-width:120px;">Mã rút điểm</th>
                                                {{--                        <th style="min-width:120px;">Tên sản phẩm</th>--}}
                                                <th style="min-width:120px;">Số điểm rút</th>
                                                <th style="min-width:120px;">Ngày rút</th>
                                                <th style="min-width:120px;">Order Id</th>
                                                <th style="min-width:120px;">Trạng thái</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($payments as $payment)
                                                <tr>
                                                    <td>{{ $payment->id }}</td>
                                                    {{--                              <td>{{ $order->product->name }}</td>--}}
                                                    <td>{{ number_format($payment->amount)}}</td>
                                                    <td>{{ \Carbon\Carbon::createFromDate($payment->created_at)->format('H:i:s d/m/Y') }}</td>
                                                    <td>{{$payment->oder_id}}</td>
                                                    <td>
                                                        @if($payment->status == 0)
                                                            <span class="text-info">Chờ duyệt</span>
                                                        @elseif($payment->status == 1)
                                                            <span class="text-success">Thành công</span>
                                                        @else
                                                            <span class="text-error">Đã hủy</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @if ($payments->count()==0)
                                                <tr>
                                                    <td class="text-center" colspan="10">Chưa có dữ liệu để hiển thị</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        {{$payments->links()}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal" id="mdOrder" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999999">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content" style="width:100%;">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"> Thông tin đơn hàng </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12 mb-2" style="line-height:40px;" id="orderInfo"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                    .dfl {
                        display: flex;
                        justify-content: space-between;
                    }
                </style>
                <script src="/Scripts/pachis.js?v=0.1"></script>
            </div>
        </section>
    </section>
@endsection
