@extends('customer.layouts.master')

@section('page_title', 'Thêm tài khoản ngân hàng')

@section('page_id', 'page-order')

@section('content')
    <section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
        <!-- Main content -->
        <section class="content" style="padding:0px;">
            <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
                <h4>Thêm tài khoản ngân hàng</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8 col-12 pt-2">
                                        <form class="form-info-bank new" method="POST" action="{{route('us.bank.store')}}">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-3 col-form-label font-weight-bold">Tên người hưởng thụ</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" required name="name" id="name" value="{{ old('name')}}" placeholder="Nhập Tên người hưởng thụ">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="bank" class="col-sm-3 col-form-label font-weight-bold">Ngân Hàng</label>
                                                <div class="col-sm-9">
                                                    <select class="form-control" id="bank" name="bank">
                                                        <option {{old('bank') == 'Viettin Bank' ? 'selected' : ''}} value="Viettin Bank">Viettin Bank</option>
                                                        <option {{old('bank') == 'Vietcom Bank' ? 'selected' : ''}} value="Vietcom Bank">Vietcom Bank</option>
                                                        <option {{old('bank') == 'Agri Bank' ? 'selected' : ''}} value="Agri Bank">Agri Bank</option>
                                                        <option {{old('bank') == 'ACB' ? 'selected' : ''}} value="ACB">ACB</option>
                                                        <option {{old('bank') == 'Techcom Bank' ? 'selected' : ''}} value="Techcom Bank">Techcom Bank</option>
                                                        <option {{old('bank') == 'BIDV' ? 'selected' : ''}} value="BIDV">BIDV</option>
                                                        <option {{old('bank') == 'MB Bank' ? 'selected' : ''}} value="MB Bank">MB Bank</option>
                                                        <option {{old('bank') == 'DongA Bank' ? 'selected' : ''}} value="DongA Bank">DongA Bank</option>
                                                        <option {{old('bank') == 'AB Bank' ? 'selected' : ''}} value="AB Bank">AB Bank</option>
                                                        <option {{old('bank') == 'EXIM Bank' ? 'selected' : ''}} value="EXIM Bank">EXIM Bank</option>
                                                        <option {{old('bank') == 'TP Bank' ? 'selected' : ''}} value="TP Bank">TP Bank</option>
                                                        <option {{old('bank') == 'VP Bank' ? 'selected' : ''}} value="VP Bank">VP Bank</option>
                                                        <option {{old('bank') == 'VIB' ? 'selected' : ''}} value="VIB">VIB</option>
                                                        <option {{old('bank') == 'MSB' ? 'selected' : ''}} value="MSB">MSB</option>
                                                        <option {{old('bank') == 'SHB' ? 'selected' : ''}} value="SHB">SHB</option>
                                                        <option {{old('bank') == 'HDB' ? 'selected' : ''}} value="HDB">HD Bank</option>
                                                        <option {{old('bank') == 'KLB' ? 'selected' : ''}} value="KLB">Kiên Long Bank</option>
                                                        <option {{old('bank') == 'SEAB' ? 'selected' : ''}} value="SEAB">NH TMCP Đông Nam Á (SEA BANK)</option>
                                                        <option {{old('bank') == 'SCB' ? 'selected' : ''}} value="SCB">Sacombank</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="number" class="col-sm-3 col-form-label font-weight-bold">Số Tài Khoản</label>
                                                <div class="col-sm-9">
                                                    <input type="text" required class="form-control" id="number" name="number" value="{{ old('number')}}" placeholder="Nhập số tài khoản ...">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="branch" class="col-sm-3 col-form-label font-weight-bold">Chi Nhánh</label>
                                                <div class="col-sm-9">
                                                    <input type="text" required class="form-control" id="branch" name="branch" value="{{ old('branch')}}" placeholder="Nhập chi nhánh ...">
                                                </div>
                                            </div>
                                            <input type="submit" class="btn btn-primary my-3" value="Thêm mới">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
@push('css')
@endpush

