@extends('customer.layouts.master')

@section('page_title', 'Thành viên')

@section('page_id', 'page-member')

@section('content')

    <section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
        <!-- Main content -->
        <section class="content" style="padding:0px;">
            <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
                <h4>Danh sách thành viên đã giới thiệu</h4>
                <div class="container-fluid">
                    <div class="col-md-12"> </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="margin-top: 10px;">
                            <div class="card card-widget widget-user-2">
                                <div class="card-footer p-0">
                                    <div>
                                        <div class="total-table table-responsive p-1" style="background: beige ">
                                            <h4>Danh sách thành viên</h4>
                                            <table class="table bg-white table-hover" style="margin: 0; min-width: 960px;">
                                                <thead>
                                                <tr class="text-left">
                                                    <th>STT</th>
                                                    <th>Ngày gia nhập</th>
                                                    <th>Trạng thái</th>
                                                    <th>Họ và tên</th>
                                                    <th>Tên đăng nhập</th>
                                                    <th>Số điện thoại</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($f1s->count() > 0)
                                                    @foreach($f1s as $key=>$user)
                                                        @php
                                                            $color = 'success';
                                                                  if($user->type == 0) {
                                                                      $color = 'danger';
                                                                  } else {
                                                                      $color = 'success';
                                                                  }
                                                        @endphp

                                                        <tr class="text-left text-{{$color}}">
                                                            <td class="text-{{$color}}"> {{$key+1}} </td>
                                                            <td class="text-{{$color}}"> {{ \Carbon\Carbon::createFromDate($user->created_at)->format('H:i:s d/m/Y') }} </td>
                                                            <td class="text-{{$color}}">
                                                                {{$user->type == 0 ? 'Chưa có doanh số' : 'Đã có doanh số'}}
                                                            </td>
                                                            <td class="text-{{$color}}"> {{$user->name}}</td>
                                                            <td class="text-{{$color}}"> {{$user->username}}</td>
                                                            <td class="text-{{$color}}"> {{$user->phone}}</td>
                                                        </tr>
                                                    @endforeach

                                                @else
                                                    <tr class="text-center no-data">
                                                        <td colspan="7"> Chưa có thành viên nào </td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
{{--                                            {{$f1s->links('web.layouts.paginate')}}--}}
                                        </div>
{{--                                        <hr>--}}
{{--                                        <div class="total-table table-responsive p-1" style="background: aquamarine ">--}}
{{--                                            <h4>Danh sách thành viên gián tiếp thế hệ 1 (@f2)</h4>--}}
{{--                                            <table class="table bg-white table-hover" style="margin: 0; min-width: 960px;">--}}
{{--                                                <thead>--}}
{{--                                                <tr class="text-left">--}}
{{--                                                    <th>STT</th>--}}
{{--                                                    <th>Ngày gia nhập</th>--}}
{{--                                                    <th>Người giới thiệu</th>--}}
{{--                                                    <th>Mã định danh</th>--}}
{{--                                                    <th>Trạng thái</th>--}}
{{--                                                    <th>Phí thành viên</th>--}}
{{--                                                    <th>Danh hiệu</th>--}}
{{--                                                    <th>Quà tặng</th>--}}
{{--                                                    <th>Đơn tham gia CLB</th>--}}
{{--                                                </tr>--}}
{{--                                                </thead>--}}
{{--                                                <tbody>--}}
{{--                                                @if($f2s->count() > 0)--}}
{{--                                                    @foreach($f2s as $key=>$user)--}}
{{--                                                        @php--}}
{{--                                                            $color = 'success';--}}
{{--                                                              if($user->level == 0) {--}}
{{--                                                                  $color = 'danger';--}}
{{--                                                              } else {--}}
{{--                                                                  if($user->type == 0) {--}}
{{--                                                                      $color = 'danger';--}}
{{--                                                                  } else {--}}
{{--                                                                      $color = 'success';--}}
{{--                                                                  }--}}
{{--                                                              }--}}
{{--                                                        @endphp--}}

{{--                                                        <tr class="text-left text-{{$color}}">--}}
{{--                                                            <td class="text-{{$color}}"> {{$key+1}} </td>--}}
{{--                                                            <td class="text-{{$color}}"> {{ \Carbon\Carbon::createFromDate($user->created_at)->format('H:i:s d/m/Y') }} </td>--}}
{{--                                                            <td class="text-{{$color}}"> {{$user->invite_user($user->invite_user)->username}} ({{$user->invite_user($user->invite_user)->name}})</td>--}}
{{--                                                            <td class="text-{{$color}}"> {{$user->username}} ({{$user->name}})</td>--}}
{{--                                                            <td class="text-{{$color}}">--}}
{{--                                                                @if($user->level == 0)--}}
{{--                                                                    Miễn phí--}}
{{--                                                                @else--}}
{{--                                                                    {{$user->type == 0 ? 'Chưa nộp phí' : 'Đã nộp phí'}}--}}
{{--                                                                @endif--}}
{{--                                                            </td>--}}
{{--                                                            <td class="text-{{$color}}"> {{number_format($user->title_sale($user->level))}} đ</td>--}}
{{--                                                            <td class="text-{{$color}}"> {{$user->title_user($user->level)}} </td>--}}
{{--                                                            <td class="text-{{$user->gift == 0 ? 'danger' : 'success'}}"> {{$user->gift == 0 ? 'Chưa nhận' : 'Đã nhận'}}</td>--}}
{{--                                                            <td class="text-{{$user->form ? 'success' : 'danger'}}"> {{$user->form ? 'Đã nộp' : 'Chưa nộp'}}</td>--}}
{{--                                                        </tr>--}}
{{--                                                    @endforeach--}}

{{--                                                @else--}}
{{--                                                    <tr class="text-center no-data">--}}
{{--                                                        <td colspan="7"> Chưa có thành viên nào </td>--}}
{{--                                                    </tr>--}}
{{--                                                @endif--}}
{{--                                                </tbody>--}}
{{--                                            </table>--}}
{{--                                            {{$f2s->links('web.layouts.paginate')}}--}}
{{--                                        </div>--}}
{{--                                        <hr>--}}
{{--                                        <div class="total-table table-responsive p-1" style="background: aqua ">--}}
{{--                                            <h4>Danh sách thành viên gián tiếp thế hệ 2 (@f3)</h4>--}}
{{--                                            <table class="table bg-white table-hover" style="margin: 0; min-width: 960px;">--}}
{{--                                                <thead>--}}
{{--                                                <tr class="text-left">--}}
{{--                                                    <th>STT</th>--}}
{{--                                                    <th>Ngày gia nhập</th>--}}
{{--                                                    <th>Người giới thiệu</th>--}}
{{--                                                    <th>Mã định danh</th>--}}
{{--                                                    <th>Trạng thái</th>--}}
{{--                                                    <th>Phí thành viên</th>--}}
{{--                                                    <th>Danh hiệu</th>--}}
{{--                                                    <th>Quà tặng</th>--}}
{{--                                                    <th>Đơn tham gia CLB</th>--}}
{{--                                                </tr>--}}
{{--                                                </thead>--}}
{{--                                                <tbody>--}}
{{--                                                @if($f3s->count() > 0)--}}
{{--                                                    @foreach($f3s as $key=>$user)--}}
{{--                                                        @php--}}
{{--                                                            $color = 'success';--}}
{{--                                                              if($user->level == 0) {--}}
{{--                                                                  $color = 'danger';--}}
{{--                                                              } else {--}}
{{--                                                                  if($user->type == 0) {--}}
{{--                                                                      $color = 'danger';--}}
{{--                                                                  } else {--}}
{{--                                                                      $color = 'success';--}}
{{--                                                                  }--}}
{{--                                                              }--}}
{{--                                                        @endphp--}}

{{--                                                        <tr class="text-left text-{{$color}}">--}}
{{--                                                            <td class="text-{{$color}}"> {{$key+1}} </td>--}}
{{--                                                            <td class="text-{{$color}}"> {{ \Carbon\Carbon::createFromDate($user->created_at)->format('H:i:s d/m/Y') }} </td>--}}
{{--                                                            <td class="text-{{$color}}"> {{$user->invite_user($user->invite_user)->username}} ({{$user->invite_user($user->invite_user)->name}})</td>--}}
{{--                                                            <td class="text-{{$color}}"> {{$user->username}} ({{$user->name}})</td>--}}
{{--                                                            <td class="text-{{$color}}">--}}
{{--                                                                @if($user->level == 0)--}}
{{--                                                                    Miễn phí--}}
{{--                                                                @else--}}
{{--                                                                    {{$user->type == 0 ? 'Chưa nộp phí' : 'Đã nộp phí'}}--}}
{{--                                                                @endif--}}
{{--                                                            </td>--}}
{{--                                                            <td class="text-{{$color}}"> {{number_format($user->title_sale($user->level))}} đ</td>--}}
{{--                                                            <td class="text-{{$color}}"> {{$user->title_user($user->level)}} </td>--}}
{{--                                                            <td class="text-{{$user->gift == 0 ? 'danger' : 'success'}}"> {{$user->gift == 0 ? 'Chưa nhận' : 'Đã nhận'}}</td>--}}
{{--                                                            <td class="text-{{$user->form ? 'success' : 'danger'}}"> {{$user->form ? 'Đã nộp' : 'Chưa nộp'}}</td>--}}
{{--                                                        </tr>--}}
{{--                                                    @endforeach--}}

{{--                                                @else--}}
{{--                                                    <tr class="text-center no-data">--}}
{{--                                                        <td colspan="7"> Chưa có thành viên nào </td>--}}
{{--                                                    </tr>--}}
{{--                                                @endif--}}
{{--                                                </tbody>--}}
{{--                                            </table>--}}
{{--                                            {{$f3s->links('web.layouts.paginate')}}--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
@push('css')
    <style>
        .image-box img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
        .image-box {
            width: 60px;
            height: 60px;
            border: 1px solid;
            border-radius: 50%;
            color: #c2c9db;
            overflow: hidden;
            margin: 5px;
            display: block;
            margin: 0 auto !important;
        }
        .google-visualization-orgchart-table {
            margin: 0 auto;
        }
    </style>
@endpush
@push('scripts')

@endpush
