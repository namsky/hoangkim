<!DOCTYPE html>
<html class="floating-labels">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <title>Thanh toán đơn hàng</title>
    <link rel="stylesheet" href="{{ asset('ionpia/assets/checkout/flag-icons.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('ionpia/assets/checkout/checkout.vendor.min.css')}}">
    <link rel="stylesheet" href="{{ asset('ionpia/assets/checkout/checkout.min.css')}}">
    <script src="{{ asset('ionpia/assets/checkout/checkout.vendor.min.js')}}"></script>
    <script src="{{ asset('ionpia/assets/checkout/checkout.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>

</head>
<body>
@include('sweetalert::alert')
<header class="banner">
    <div class="wrap">
        <div class="logo logo--left">
            <a href="{{ route('us.home') }}">
                <img class="logo__image  logo__image--medium " src="{{ asset('customer/logo.png')}}" />
            </a>
        </div>
    </div>
</header>
<aside>
    <button class="order-summary-toggle" >
        <span class="wrap">
          <span class="order-summary-toggle__inner">
            <span class="order-summary-toggle__text"> Đơn hàng</span>
          </span>
        </span>
    </button>
</aside>
<div id="checkout" class="content">
    <form  method="post"  action="{{ route('w.customer_pay.store')}}" >
        @csrf
        <div class="wrap">
            <main class="main">
                <header class="main__header">
                    <div class="logo logo--left">
                        <a href="{{route('w.index')}}">
                            <img class="logo__image  logo__image--medium " alt="" src="{{ asset('customer/logo.png')}}" />
                        </a>
                    </div>
                </header>
                <div class="main__content">
                    <article class="animate-floating-labels row">
                        <div class="col col--two">
                            <section class="section">
                                <div class="section__header">
                                    <div class="layout-flex">
                                        <h2 class="section__title layout-flex__item layout-flex__item--stretch">Thông tin nhận hàng</h2>
                                    </div>
                                </div>
                                <div class="section__content">
                                    <div class="fieldset">
                                        <div class="field " >
                                            <div class="field__input-wrapper">
                                                <input name="name" id="billingName" type="text" class="field__input"  placeholder="Họ và tên" value="{{ !empty(customer()->user()) ? customer()->user()->name : ''}}">
                                            </div>
                                        </div>
                                        <div class="field " >
                                            <div class="field__input-wrapper field__input-wrapper--connected" >
                                                <input name="phone" id="billingPhone" type="tel" class="field__input" placeholder="Số điện thoại" value="{{ !empty(customer()->user()) ? customer()->user()->phone : ''}}">
                                            </div>
                                        </div>
                                        <div class="field " >
                                            <div class="field__input-wrapper">
                                                <input name="address" id="billingAddress" type="text" class="field__input" placeholder="Địa chỉ" value="{{ !empty(customer()->user()) ? customer()->user()->address : ''}}" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                        </div>
                        <div class="col col--two">
                            <section class="section">
                                <div class="section__header">
                                    <div class="layout-flex">
                                        <h2 class="section__title layout-flex__item layout-flex__item--stretch">Thanh toán</h2>
                                    </div>
                                </div>
                                <div class="section__content">
                                    <div class="content-box" >
                                        <div class="content-box__row">
                                            <div class="radio-wrapper">
                                                <div class="radio__input">
                                                    <input name="paymentMethod" checked id="paymentMethod-505445" type="radio" class="input-radio" >
                                                </div>
                                                <label for="paymentMethod-505445" class="radio__label">
                                                    <span class="radio__label__primary">Thanh toán online qua Alepay</span>
                                                </label>
                                            </div>
                                            <div class="content-box__row__desc" >
                                                <p>Bạn chỉ phải thanh toán khi nhận được hàng</p>
                                            </div>

                                        </div>
                                    </div>
                                    <div style="padding: 0.5rem">
                                        <input type="checkbox" required name="confirm_rules" id="confirm"> <label for="confirm" style="font-weight: bold">Tôi đồng ý với
                                            <a target="_blank" href="/danh-sach-bai-viet/chinh-sach/chinh-sach-thanh-toan">chính sách và điều khoản của website</a></label>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </article>
                    <div class="field__input-btn-wrapper field__input-btn-wrapper--vertical hide-on-desktop">
                        <button type="submit" class="btn btn-checkout spinner" >
                            <span class="spinner-label">ĐẶT HÀNG</span>
                            <svg xmlns="http://www.w3.org/2000/svg" class="spinner-loader">
                                <use href="#spinner"></use>
                            </svg>
                        </button>
                        <a href="/cart" class="previous-link">
                            <i class="previous-link__arrow">❮</i>
                            <span class="previous-link__content">Quay về giỏ hàng</span>
                        </a>
                    </div>
                    <div id="common-alert" >
                        <div class="alert alert--danger hide-on-desktop"  ></div>
                    </div>
                </div>
            </main>
            <aside class="sidebar">
                <div class="sidebar__header">
                    <h2 class="sidebar__title"> Đơn hàng </h2>
                </div>
                <div class="sidebar__content">
                    <div id="order-summary" class="order-summary order-summary--is-collapsed">
                        <div class="order-summary__sections">
                            <div class="order-summary__section order-summary__section--product-list order-summary__section--is-scrollable order-summary--collapse-element">
                                <table class="product-table">
                                    <caption class="visually-hidden">Chi tiết đơn hàng</caption>
                                    <thead class="product-table__header">
                                    <tr>
                                        <th>
                                            <span class="visually-hidden">Ảnh sản phẩm</span>
                                        </th>
                                        <th>
                                            <span class="visually-hidden">Mô tả</span>
                                        </th>
                                        <th>
                                            <span class="visually-hidden">Sổ lượng</span>
                                        </th>
                                        <th>
                                            <span class="visually-hidden">Đơn giá</span>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($carts as $cart)
                                        <tr class="product">
                                            <td class="product__image">
                                                <div class="product-thumbnail">
                                                    <div class="product-thumbnail__wrapper" >
                                                        <img src="{{ $cart->options->image }}" alt="" class="product-thumbnail__image" />
                                                    </div>
                                                    <span class="product-thumbnail__quantity">{{ $cart->qty }}</span>
                                                </div>
                                            </td>
                                            <th class="product__description">
                                                <span class="product__description__name">{{ $cart->name }} </span>
                                            </th>
                                            <td class="product__quantity visually-hidden">
                                                <em>Số lượng:</em> {{ $cart->qty }}
                                            </td>
                                            <td class="product__price">{{number_format($cart->price)}}₫ </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Số lượng</td>
                                            <td colspan="2">{{ $cart->qty }}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Thành tiền</td>
                                            <td colspan="2">{{number_format($cart->price*$cart->qty)}}₫</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="order-summary__section order-summary__section--total-lines order-summary--collapse-element">
                                <table class="total-line-table">
                                    <caption class="visually-hidden">Tổng giá trị</caption>
                                    <thead>
                                    <tr>
                                        <td>
                                            <span class="visually-hidden">Mô tả</span>
                                        </td>
                                        <td>
                                            <span class="visually-hidden">Giá tiền</span>
                                        </td>
                                    </tr>
                                    </thead>
                                    <tbody class="total-line-table__tbody">
                                    @if (isset($customer))
                                        <tr class="total-line total-line--subtotal">
                                            <th class="total-line__name"> Chiết khấu </th>
                                            <td class="total-line__price">{{$customer->grant->percent}}%</td>
                                            <td class="total-line__price">{{number_format($cart->price*$cart->qty - $total)}}₫</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                    <tfoot class="total-line-table__footer">
                                    <tr class="total-line payment-due">
                                        <th class="total-line__name" colspan="2">
                                            <span class="payment-due__label-total"> Tổng cộng </span>
                                        </th>
                                        <td class="total-line__price">
                                            <span class="payment-due__price"> {{number_format($total)}}₫ </span>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="order-summary__nav field__input-btn-wrapper hide-on-mobile layout-flex--row-reverse">
                                <button type="submit" class="btn btn-checkout"  >
                                    <span class="spinner-label">ĐẶT HÀNG</span>
                                </button>
                                <a href="{{route('w.cart.index')}}"  class="previous-link">
                                    <i class="previous-link__arrow">❮</i>
                                    <span class="previous-link__content">Quay về giỏ hàng</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </form>
</div>
@error('name')
<script>
    Swal.fire(
        'Lỗi',
        '{{$message}}',
        'error'
    )
</script>
@enderror
@error('number')
<script>
    Swal.fire(
        'Lỗi',
        '{{$message}}',
        'error'
    )
</script>
@enderror
@error('address')
<script>
    Swal.fire(
        'Lỗi',
        '{{$message}}',
        'error'
    )
</script>
@enderror

</body>
</html>
