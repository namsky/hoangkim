@extends('customer.layouts.master')

@section('page_title', 'Tài khoản Ngân Hàng')

@section('page_id', 'page-account')
@section('content')
    <div class="container">
        <div class="row">
            @include('customer.layouts.includes.side-nav')
            <div class="col-md-8 col-sm-12">
                <div class="box-right">
                    <h3 class="mt-5">Lịch sử mua điểm</h3>
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                        <tr>
                            <th width="5%">ID</th>
                            <th width="15%">Hình thức</th>
                            <th width="10%">Số điểm</th>
                            <th width="10%">Thời gian</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($payments as $item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>
                                    @if($item->method == 'bank_qr')
                                        Chuyển khoản ngân hàng
                                    @elseif($item->method == 'momo_qr')
                                        Chuyển khoản Momo
                                    @endif
                                </td>
                                <td>{{ number_format($item->amount, 0, ',', '.') }} VNĐ</td>
                                <td>{{ $item->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $payments->links() }}
                </div>
            </div>

        </div>


    </div>
    @error('branch')
    <script>
        Swal.fire(
            'Lỗi',
            '{{$message}}',
            'error'
        )
    </script>
    @enderror
    @error('number')
    <script>
        Swal.fire(
            'Lỗi',
            '{{$message}}',
            'error'
        )
    </script>
    @enderror
    @error('bank')
    <script>
        Swal.fire(
            'Lỗi',
            '{{$message}}',
            'error'
        )
    </script>
    @enderror
    @error('name')
    <script>
        Swal.fire(
            'Lỗi',
            '{{$message}}',
            'error'
        )
    </script>
    @enderror
@endsection
