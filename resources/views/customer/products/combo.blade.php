@extends('customer.layouts.master')

@section('page_title', 'Sản phẩm đồng chia')

@section('page_id', 'page-order')

@section('content')
    <section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
        <!-- Main content -->
        <section class="content" style="padding:0px;">
            <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
                <div class="p-4 bg-white my-4 mx-1 rounded border ">
                    <h2 class="mb-3" style="color:var(--green); font-weight:bold; text-align: center;">
                        CHỌN SẢN PHẨM ĐỒNG CHIA
                    </h2>
                    <div class="row" id="customNavMenus">
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
                            <div class="small-box inner-daily bg-success bg-background" amount="10000000" pro_id="1">
                                <div class="inner ">
                                    <p>Sản phẩm đồng chia 1</p>
                                    <h3 style="font-size: 2rem !important;">10.000.000</h3> </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
                            <div class="small-box inner-daily bg-success bg-background" amount="50000000" pro_id="2">
                                <div class="inner ">
                                    <p>Sản phẩm đồng chia 2</p>
                                    <h3 style="font-size: 2rem !important;">50.000.000</h3> </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
                            <div class="small-box inner-daily bg-success bg-background" amount="100000000" pro_id="3">
                                <div class="inner ">
                                    <p>Sản phẩm đồng chia 3</p>
                                    <h3 style="font-size: 2rem !important;">100.000.000</h3> </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
                            <div class="small-box inner-daily bg-success bg-background" amount="300000000" pro_id="4">
                                <div class="inner ">
                                    <p>Sản phẩm đồng chia 4</p>
                                    <h3 style="font-size: 2rem !important;">300,000,000</h3> </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
                            <div class="small-box inner-daily bg-success bg-background" amount="600000000" pro_id="5">
                                <div class="inner ">
                                    <p>Sản phẩm đồng chia 5</p>
                                    <h3 style="font-size: 2rem !important;">600,000,000</h3> </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
                            <div class="small-box inner-daily bg-success bg-background" amount="1000000000" pro_id="6">
                                <div class="inner ">
                                    <p>Sản phẩm đồng chia 6</p>
                                    <h3 style="font-size: 2rem !important;">1.000.000.000</h3> </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12"></div>
                    </div>
                    <div class="d-flex justify-content-center my-4"> <img src="{{asset('web/images/a.png')}}" class="img-fluid" alt=""> </div>
                    <div class="custom-price text-center"> <b style="color: red;">Vui lòng chọn Sản phẩm đồng chia để xác nhận đơn hàng</b>
                        <h1>TỔNG: <span class="total">--</span>VNĐ</h1>
                        <a href="javascript:void(0)" class="">
                            <a href="javascript:;" class="boxed-child-price btnBuyPackage bg-background" id="btn-buy" style="max-width: 250px; margin-top: 20px; border:none; padding: .5rem">Tiếp tục thanh toán</a>
                        </a>
                    </div>
                </div>
                <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLabel">Thông tin đơn hàng</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body" id="orderInfo">

                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                    .dfl {
                        display: flex;
                        justify-content: space-between;
                    }
                </style>

            </div>
        </section>
    </section>
@endsection
@push('css')
    <style>
        .modal-backdrop.in {
            opacity: .5 !important;
        }
    </style>
@endpush
@push('scripts')
    <script>
        $(".inner-daily").click(function () {
            var money = $(this).attr('amount');
            $(".total").html(addCommas(money));
            var id = $(this).attr('pro_id');
            // console.log(id);
            $.ajax({
                type: "GET",
                url: '{{route('us.buycombo.info')}}',
                data: {
                    id : id
                },
                success: function(data)
                {
                    const res = JSON.parse(data);
                    var html = '';
                    var amount = money;
                    var pro_number = 6;
                    var voucher = '10.000.000';
                    var dongchia = 130;
                    var buy = res.buy;
                    if(id == 1) {
                        pro_number = 6;
                        voucher = '10.000.000';
                        dongchia = 130;
                    } else if(id == 2) {
                        pro_number = 30;
                        voucher = '50.000.000';
                        dongchia = 136;
                    } else if(id == 3) {
                        pro_number = 60;
                        voucher = '100.000.000';
                        dongchia = 145;
                    } else if(id == 4) {
                        pro_number = 180;
                        voucher = '300.000.000';
                        dongchia = 155;
                    } else if(id == 5) {
                        pro_number = 360;
                        voucher = '600.000.000';
                        dongchia = 165;
                    } else if(id == 6) {
                        pro_number = 600;
                        voucher = '1.000.000.000';
                        dongchia = 185;
                    }
                    html += '<h4>GIÁ TRỊ : <strong>'+addCommas(res.product.price)+'</strong></h4> <hr>\
                            <h4>NHẬN SẢN PHẨM : <strong>'+pro_number+'</strong></h4><hr>\
                            <h4>VOUCHER : <strong>'+voucher+'</strong></h4><hr>\
                            <h4>TRI ÂN 10% ĐỒNG CHIA : <strong>'+addCommas( (amount*dongchia)/100)+'</strong></h4><hr>\
                            <h4>THÀNH VIÊN ĐÃ MUA : <strong>'+addCommas(buy)+'</strong></h4>';
                    $("#orderInfo").html(html);

                    var buy_button = '<a href="/customers/san-pham-dau-tu/mua/'+id+'" type="button" class="btn btn-primary">Thanh toán ngay</a>';
                    $(".modal-footer").html(buy_button);
                    $("#btn-buy").attr('href','/customers/san-pham-dau-tu/mua/'+id+'');

                    $("#exampleModal").modal('show');
                    // console.log(res);
                }
            });
        })
        // Format Number
        function addCommas(str) {
            var amount = new String(str);
            amount = amount.split("").reverse();

            var output = "";
            for (var i = 0; i <= amount.length - 1; i++) {
                output = amount[i] + output;
                if ((i + 1) % 3 == 0 && (amount.length - 1) !== i)
                    output = '.' + output;
            }
            return output;
        }
    </script>
@endpush
