@extends('customer.layouts.master')

@section('page_title', 'Sản phẩm đầu tư')

@section('page_id', 'page-order')

@section('content')
    <section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
        <!-- Main content -->
        <section class="content" style="padding:0px;">
            <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
                <div class="p-4 bg-white my-4 mx-1 rounded border ">
                    <h2 class="mb-3" style="color:var(--green); font-weight:bold; text-align: center;">
                        THANH TOÁN SẢN PHẨM ĐẦU TƯ
                    </h2>
                    <div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Ngân Hàng:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-building"></i></span>
                                        </div>
                                        <input readonly type="text" value="Vietcombank" class="form-control" style="text-transform:uppercase;">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Số tài khoản:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-hashtag"></i></span>
                                        </div>
                                        <input readonly type="text" value="0451000291401" class="form-control" style="text-transform:uppercase;">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Người hưởng thụ:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input readonly type="text" value="LE HUY HOANG" class="form-control" style="text-transform:uppercase;">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Số tiền:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-money-bill"></i></span>
                                        </div>
                                        <input readonly type="text" value="{{number_format($store->total_money)}}" class="form-control" style="text-transform:uppercase;">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Nội dung:</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-notes-medical"></i></span>
                                        </div>
                                        <input readonly type="text" value="{{(customer()->user()->username)}} tt san pham dau tu" class="form-control" style="text-transform:uppercase;">
                                    </div>
                                </div>
                            </div>
                            <a type="button" href="{{route('us.home')}}" class="btn btn-block bg-background col-md-2 mt-4" style="float: left; margin-bottom: 10px; margin-right: 5px; font-size: 20px;color: white; font-weight: bold;">Đã thanh toán</a>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                    .dfl {
                        display: flex;
                        justify-content: space-between;
                    }
                </style>

            </div>
        </section>
    </section>
@endsection
@push('css')
    <style>
        .modal-backdrop.in {
            opacity: .5 !important;
        }
    </style>
@endpush
@push('scripts')
@endpush
