@extends('customer.layouts.master')

@section('page_title', 'Quản lý sản phẩm')

@section('page_id', 'page-order')

@section('content')
    <section class="content-wrapper bg-background-main">
        <!-- Main content -->
        <section class="content" style="padding:0px;">
            <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
                <h4>Quản lý sản phẩm</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12"></div>
                                    <div class="actions">
                                        <a href="{{ route('us.product.create')}}" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Thêm mới</a>
                                    </div>
                                    <div style="width: 100%">
                                        <table id="thistory" class="table table-bordered table-striped bg-white table-hover" style="font-size:15px; min-width:1000px">
                                            <thead>
                                            <tr>
                                                <th style="min-width:120px;">ID</th>
                                                {{--                        <th style="min-width:120px;">Tên sản phẩm</th>--}}
{{--                                                <th style="min-width:120px;">Ảnh</th>--}}
                                                <th style="min-width:120px;">Trạng thái</th>
                                                <th style="min-width:120px;">Tên sản phẩm</th>
                                                <th style="min-width:120px;">Giá bán cho thành viên</th>
                                                <th style="min-width:120px;">Giá bán cho sàn</th>
                                                <th style="min-width:120px;">Danh mục sản phẩm</th>
                                                <th style="min-width:120px;">Đã bán</th>
                                                <th style="min-width:120px;">Tồn kho</th>
                                                <th style="min-width:120px;">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($products as $product)
                                                <tr>
                                                    <td>{{ $product->id }}</td>
                                                    {{--                              <td>{{ $order->product->name }}</td>--}}
{{--                                                    <td><img src="{{ $product->image}}" style="max-width: 6rem" alt=""></td>--}}
                                                    <td class="color-{{ $product->status == 0 ? 'red' : 'green'}}">{{ $product->status == 0 ? 'Chờ duyệt' : 'Đã duyệt'}}</td>
                                                    <td>{{ $product->name}}</td>
                                                    <td>{{ number_format( $product->price, 0, ',','.')}}đ</td>
                                                    <td>{{ number_format( $product->price_system, 0, ',','.')}}đ</td>
                                                    <td>{{ $product->category_id !=0 ? $product->category->category_name : "Chưa phân loại" }}</td>
                                                    <td>{{$product->product_buy($product->id)}}</td>
                                                    <td>{{$product->stock - $product->product_buy($product->id)}}</td>
                                                    <td>
                                                        <a class="btn btn-circle btn-sm btn-warning" href="{{ route('us.product.edit', $product) }}"><i class="fa fa-edit"></i></a>
                                                        <form action="{{ route('us.product.destroy', $product) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-circle btn-sm btn-danger" ><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @if ($products->count()==0)
                                                <tr>
                                                    <td class="text-center" colspan="10">Chưa có dữ liệu để hiển thị</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                        {{$products->links()}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal" id="mdOrder" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999999">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content" style="width:100%;">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"> Thông tin đơn hàng </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12 mb-2" style="line-height:40px;" id="orderInfo"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                    .dfl {
                        display: flex;
                        justify-content: space-between;
                    }
                </style>
                <script src="/Scripts/pachis.js?v=0.1"></script>
            </div>
        </section>
    </section>
@endsection
@push('css')
    <style>
        .color-red {
            color: red;
        }
        .color-green {
            color: green;
        }
    </style>
@endpush

