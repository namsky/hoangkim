@extends('customer.layouts.master')

@section('page_title', 'Thêm mới sản phẩm')

@section('page_id', 'page-order')

@section('content')
    <section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
        <!-- Main content -->
        <section class="content" style="padding:0px;">
            <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
                <h4>Thêm mới sản phẩm</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12"></div>
                                    <div class="pt-2">
                                        <form action="{{ route('us.product.store') }}" method="POST" @submit.prevent="onSubmit" enctype="multipart/form-data">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group" :class="[errors.has('name') ? 'has-error' : '']">
                                                        <label for="name">Tên sản phẩm <span class="required">*</span></label>
                                                        <input type="text" id="name" class="form-control" name="name" required v-validate="'required'" data-vv-as="&quot;Tên sản phẩm&quot;" value="{{ old('name') }}">
                                                    </div>
                                                    <div class="form-group" :class="[errors.has('category_id') ? 'has-error' : '']">
                                                        <label for="name">Danh mục sản phẩm<span class="required">*</span></label>
                                                        <select name="category_id" id="" class="form-control">
                                                            <option value="0">Không phân loại</option>
                                                            @foreach ($categories as $item)
                                                                <option value="{{ $item->id }}">{{ $item->category_name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-group" :class="[errors.has('price') ? 'has-error' : '']">
                                                        <label for="price" >Giá bán cho thành viên <span class="required">*</span></label>
                                                        <input type="number" required id="price" class="form-control" name="price"  value="{{ old('price') }}" v-validate="'required'" >
                                                    </div>
                                                    <div class="form-group" :class="[errors.has('price_sale') ? 'has-error' : '']">
                                                        <label for="price_sale" >Giá bán khuyến mãi </label>
                                                        <input type="number" required id="price_sale" class="form-control" name="price_sale"  value="{{ old('price_sale') }}" v-validate="'required'" >
                                                    </div>
                                                    <div class="form-group" :class="[errors.has('price') ? 'has-error' : '']">
                                                        <label for="price_system" >Giá bán cho sàn <span class="required">*</span></label>
                                                        <input type="number" required id="price_system" class="form-control" name="price_system"  value="{{ old('price_system') }}" v-validate="'required'" >
                                                    </div>
                                                    <div class="form-group" :class="[errors.has('expiry') ? 'has-error' : '']">
                                                        <label for="expiry" >Hạn sử dụng<span class="required">*</span></label>
                                                        <input type="datetime-local" required id="expiry" class="form-control" name="expiry"  value="{{ old('expiry') }}" v-validate="'required'" >
                                                    </div>
                                                    <div class="form-group" :class="[errors.has('expiry') ? 'has-error' : '']">
                                                        <label for="origin" >Xuất xứ<span class="required">*</span></label>
                                                        <input type="text" required id="origin" class="form-control" name="origin"  value="{{ old('origin') }}" v-validate="'required'" >
                                                    </div>
                                                    <div class="form-group" :class="[errors.has('price') ? 'has-error' : '']">
                                                        <label for="stock" >Số lượng hàng có trong kho <span class="required">*</span></label>
                                                        <input type="number" required id="stock" class="form-control" name="stock"  value="{{ old('stock') }}" v-validate="'required'" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="content" required>Mô tả sản phẩm<span class="required">*</span></label>
                                                        <textarea  type="text" id="content" class="form-control tinymce" name="content" >{!! old('content') !!}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="image">Hình ảnh<span class="required">*</span></label>
                                                        <div>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px;">
                                                                    <img class="img-responsive" src="{{ asset('images/no_image.png') }}" alt="" />
                                                                </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="height: 200px"></div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new">Chọn ảnh</span>
                                                                        <span class="fileinput-exists">Đổi ảnh</span>
                                                                        <input type="file" accept="image/*" name="image">
                                                                    </span>
                                                                    <a href="javascript:void(0);" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Xóa</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="image">Mã QR vạch của sản phẩm<span class="required">*</span></label>
                                                        <div>
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px;">
                                                                    <img class="img-responsive" src="{{ asset('images/no_image.png') }}" alt="" />
                                                                </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="height: 200px"></div>
                                                                <div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new">Chọn ảnh</span>
                                                                        <span class="fileinput-exists">Đổi ảnh</span>
                                                                        <input type="file" accept="image/*" name="qrcode">
                                                                    </span>
                                                                    <a href="javascript:void(0);" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Xóa</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="description">Mô tả ngắn</label>
                                                        <textarea  type="text" id="description" class="form-control tinymce" name="description" >{!!   old('description') !!}</textarea>
                                                        {{--                                    <textarea  type="text" id="description" class="form-control" name="description">{{ old('description') }}</textarea>--}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions">
                                                <button class="btn btn-primary">Tạo mới</button>
                                                <a href="{{ route('us.product.index') }}" class="btn btn-default">Quay lại</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal" id="mdOrder" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999999">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content" style="width:100%;">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"> Thông tin đơn hàng </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12 mb-2" style="line-height:40px;" id="orderInfo"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                    .dfl {
                        display: flex;
                        justify-content: space-between;
                    }
                </style>
                <script src="/Scripts/pachis.js?v=0.1"></script>
            </div>
        </section>
    </section>
@endsection
@push('css')
    <link href="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endpush

@prepend('scripts')
    <script src="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    @include('admin.lib.tinymce-setup')

@endprepend
