@extends('customer.layouts.master')
@section('page_title', 'Trang tài khoản')
@section('page_id', 'page-home')
@section('content')
@php
    use Carbon\Carbon;
    use App\Models\Order;
@endphp

<section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
  <!-- Main content -->
  <section class="content" style="padding:0px;">
    <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
        <div class="row">
            <div class="container-fluid mb-1 m-0">
{{--                @if (auth('user')->user()->level < 3)--}}
                    <div class="link-introduce bg-background p-3 rounded text-white row">
                        <div class="col-12 col-md-4">
                            <span>Link Giới Thiệu:</span>
                            <strong class="">{{ request()->getHttpHost().'/customers'.'/'.auth('user')->user()->url }}</strong>
                            <button class="btn btn-light " id="linkIntroduce">Copy</button>
                        </div>
                        <div class="col-12 col-md-4 mt-2">
                            <span>Mã Giới Thiệu:</span>
                            <strong class="">{{ auth('user')->user()->url }}</strong>
                        </div>
                        <div class="col-12 col-md-4 mt-2">
                            Mã QR code: &ensp;
                            {{QrCode::size(100)->generate(request()->getHttpHost().'/customers'.'/'.auth('user')->user()->url)}}
                        </div>
                        <span class="copy text-success"></span>
                    </div>
{{--                @endif--}}
            </div>
        </div>
      <div class="row bg-white my-4 mx-1 rounded border justify-content-center" style="margin: 5px -10px 20px -10px !important; position: relative;">
        <div class="col-12 col-sm-6 col-md-6 col-lg-4" style="margin-top:5px;">
          <div class="small-box bg-success bg-background" style="">
            <img src="{{ asset('customer/logo.png')}}" style="width: 12%; position: absolute; right: 5px; top: 5px;" class="">
            <div class="inner inner-box" style="display: inline-block; width:100%">
              <div class="content-box1" style="padding-left: 0px !important;">
                <h6>
                  <strong>
                    @switch($customer->level)
                        @case(1)
                            Thành viên
                            @break
                        @case(2)
                            Nhà phân phối
                            @break
                        @case(3)
                            Nhà sản xuất
                            @break
                        @default
                            Thành viên
                    @endswitch
                  </strong>
                </h6>
                <h3>{{ $customer->name }}</h3>
                <h5>Tên đăng nhập : {{ $customer->username }}</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    <div class="row mb-4">
        <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12  ">
            <div class="small-box bg-success bg-background ">
                <div class="inner inner-box">
                    <h6>Số điểm có thể rút</h6>
                    <h3>{{ number_format($customer->wallet1 , 0, ',', '.') }} ₫</h3>
                </div>
                <div class="icon">
                    <i class="fas fa-wallet"></i>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12  ">
            <div class="small-box bg-success bg-background rounded">
                <div class="inner inner-box">
                    <h6>Hoa hồng trực tiếp đã nhận</h6>
                    <h3>{{ number_format($hhtt , 0, ',', '.') }} ₫</h3>
                </div>
                <div class="icon">
                    <i class="fas fa-wallet"></i>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 ">
            <div class="small-box bg-success bg-background rounded">
                <div class="inner inner-box">
                    <h6>Hoa hồng quản lý đã nhận</h6>
                    <h3>{{ number_format($hhql , 0, ',', '.') }} ₫</h3>
                </div>
                <div class="icon">
                    <i class="fas fa-wallet"></i>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12  ">
            <div class="small-box bg-success bg-background rounded">
                <div class="inner inner-box">
                    <h6>Điểm có thể dùng</h6>
                    <h3>{{ number_format($customer->point , 0, ',', '.') }} ₫</h3>
                </div>
                <div class="icon">
                    <i class="fas fa-wallet"></i>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12  ">
            <div class="small-box bg-success bg-background rounded">
                <div class="inner inner-box">
                    <h6>Tổng Max Thu Nhập hiện tại</h6>
                    <h3>{{ number_format($customer->maxtt($customer->id) , 0, ',', '.') }} ₫</h3>
                </div>
                <div class="icon">
                    <i class="fas fa-wallet"></i>
                </div>
            </div>
        </div>
    </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row d-flex justify-content-center">
        <div id="exTab2" style="width: 100%; max-width: 911px;">
          <ul class="nav nav-tabs justify-content-center active-tab-custom">
            <li class="active w-50" data-type="1">
              <a href="#1" data-toggle="tab">Thống kê đại lý</a>
            </li>
{{--            <li data-type="2" class="">--}}
{{--              <a href="#2" data-toggle="tab">Thống kê hoa hồng</a>--}}
{{--            </li>--}}
            <li class="w-50" data-type="3">
              <a href="#3" data-toggle="tab">Thống kê doanh số</a>
            </li>
          </ul>
        </div>
        <div class="d-none" style="min-width:400px; width: 100% !important; text-align:center; display:inline-block;">
          <div style="margin-bottom: 10px;">Chọn khoảng thời gian xem</div>
          <input type="date" id="txtFromDate" style="width: 140px;text-align:center; display: inline;height:38px;line-height:38px;" placeholder="Từ ngày" class="">
          <input type="date" id="txtToDate" style="width: 140px; text-align: center; display: inline;height:38px;line-height:38px;" placeholder="Đến ngày" class="">
          <a href="javascript:void(0);" class="btn btn-sm btn-primary btnViewByDate" style="padding-bottom: 10px; margin-top: -5px;">Xem</a>
        </div>
        <div class="tab-content" style="transform: scale(1.0) !important;">
          <div class="tab-pane active" id="1">
            <div class="row bg-white my-4 mx-1 rounded border" style="padding-top: 8px;">
              <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Đại lý đăng ký <br>
                      <small>Đại lý trực thuộc</small>
                    </p>
                    <h3 class="resetText" id="lbTotalMember">{{ $childs->count() }}</h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-user-plus"></i>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Đại lý hoạt động <br>
                      <small>Đại lý kích hoạt</small>
                    </p>
                    <h3 class="resetText" id="lbMemberActive">{{ $childs->where('type', 1)->count() }}</h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-user-check"></i>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Đơn hàng cá nhân <br>
                      <small>Đơn hàng đã nhận</small>
                    </p>
                    <h3 class="resetText" id="lbDSCN">{{ Auth('user')->user()->order()->where('status', 1)->count() }}</h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-shopping-basket"></i>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Đơn hàng trực tiếp <br>
                      <small>Doanh số trực tiếp</small>
                    </p>
                    <h3 class="resetText" id="lbDSTT">0</h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-shopping-basket"></i>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Doanh số nhóm <br>
                      <small>Tổng doanh số đội nhóm</small>
                    </p>
                    <h3 class="resetText" id="lbDSN">{{ number_format( $customer->group_money($customer), 0, ',','.') }}đ</h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-users"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="2">
            <div class="row bg-white my-4 mx-1 rounded border" style="padding-top: 8px;">
              <div class="col-xl-4 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Thưởng nhóm theo ngày</p>
                    <h3 class="resetText" id="lbCommissionType_0">{{ number_format($customer->log_money()->where('type',5)->sum('money') , 0, ',', '.') }} ₫</h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-dollar-sign"></i>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Cổ phần</p>
                    <h3 class="resetText" id="lbCommissionType_1">{{ number_format($customer->shares , 0, ',', '.') }} ₫</h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-dollar-sign"></i>
                  </div>
                </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Thưởng thu nhập/ thu nhập</p>
                    <h3 class="resetText" id="lbCommissionType_5">{{ number_format($customer->log_money()->where('type',6)->sum('money') , 0, ',', '.') }} ₫</h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-dollar-sign"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="3">
            <div class="row bg-white my-4 mx-1 rounded border" style="padding-top:8px;">
              <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Hôm nay</p>
                    <h3>
                      @php
                          $today = Carbon::today();
                          $todayRecords = Order::whereDate('created_at', $today)->where('user_id', $customer->id)->where('status','1')->sum('total_money');
                          echo number_format($todayRecords, 0, ',', '.').'₫';
                      @endphp

                    </h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-calendar-check"></i>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Hôm qua</p>
                    <h3>
                      @php
                          $yesterday = Carbon::yesterday();
                          $yesterdayRecords = Order::whereDate('created_at', $yesterday)->where('user_id', $customer->id)->where('status','1')->sum('total_money');
                          echo number_format($yesterdayRecords, 0, ',', '.').'₫';
                      @endphp
                    </h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-calendar"></i>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Tháng này</p>
                    <h3>
                      @php
                          $currentMonth = Carbon::now()->month;
                          $thisMonthRecords  = Order::whereMonth('created_at', $currentMonth)->where('user_id', $customer->id)->where('status','1')->sum('total_money');
                          echo number_format($thisMonthRecords , 0, ',', '.').'₫';
                      @endphp
                    </h3>
                  </div>
                  <div class="icon">
                    <i class="fa fa-calendar-alt"></i>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12 ">
                <div class="small-box bg-success bg-background">
                  <div class="inner">
                    <p>Tháng trước</p>
                    <h3>
                      @php
                          $lastMonth = Carbon::now()->subMonth()->month;
                          $lastMonthRecords  = Order::whereMonth('created_at', $lastMonth)->where('user_id', $customer->id)->where('status','1')->sum('total_money');
                          echo number_format($lastMonthRecords , 0, ',', '.').'₫';
                      @endphp
                    </h3>
                  </div>
                  <div class="icon">
                    <i class="fas fa-calendar-plus"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  </div>
</section>
@endsection
@push('scripts')
<script>
  $('#linkIntroduce').click(() => {
    navigator.clipboard.writeText(`{{ request()->getHttpHost().'/customers'.'/'.auth('user')->user()->url}}`);
    $('.copy').text('Copy thành công');
    Swal.fire('Thành Công', 'Copy thành công link!', 'success', )
  });
</script>


@endpush
