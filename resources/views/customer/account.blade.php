@extends('customer.layouts.master')
@section('page_title', 'Tài khoản')
@section('page_id', 'page-account')
@section('content')
<section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
  <!-- Main content -->
  <section class="content" style="padding:0px;">
    <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title ">
                <i class="fa fa-user-circle"></i> Thông tin cá nhân
              </h4>
            </div>
            <div class="card-body">
              <div class="ribbon-wrapper ribbon-lg">
                <div class="ribbon bg-success">Hoàng Kim Mart</div>
              </div>
              <form action="{{route('us.account.update',$customer)}}" method="post" >
                @method('PUT')
                @csrf
                <div class="col-md-12">
                  <h5>THÔNG TIN QUẢN LÝ</h5>
                  <div class="form-group">
                    <label>Đại lý cấp trên</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-user"></i>
                        </span>
                      </div>
                      <input type="text" value="{{(customer()->user()->f1_id()) ? customer()->user()->f1_id()->username : ''}}" readonly="readonly"  class="form-control">
                    </div>
                  </div>

                  <h5>THÔNG TIN CÁ NHÂN</h5>
                  <div class="form-group">
                    <label>Tên truy cập</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-user"></i>
                        </span>
                      </div>
                      <input type="text" name="username" value="{{ $customer->username }}" readonly=""  class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Họ và tên (*)</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-user"></i>
                        </span>
                      </div>
                      <input name="name" type="text" value="{{ $customer->name }}"  class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Giới tính (*)</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-venus-mars"></i>
                        </span>
                      </div>
                      <select name="gender"  class="form-control">
                        <option value="1" {{$customer->gender == 1 ? 'selected' : ''}}>Nam</option>
                        <option value="0" {{$customer->gender == 0 ? 'selected' : ''}}>Nữ</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Điện thoại (*)</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-phone"></i>
                        </span>
                      </div>
                      <input name="phone" type="text" value="{{ $customer->phone }}" maxlength="10" class="form-control numbers10" required="" placeholder="Phone" autocomplete="off">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Email</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-envelope"></i>
                        </span>
                      </div>
                      <input name="email" type="text" value="{{ $customer->email }}" maxlength="50"  class="form-control" placeholder="Nếu có">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Địa chỉ</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-envelope"></i>
                        </span>
                      </div>
                      <input name="address" type="text" value="{{ $customer->address }}" maxlength="50"  class="form-control" placeholder="Chưa cập nhật">
                    </div>
                  </div>
                  {{-- <h5>THÔNG TIN NGÂN HÀNG</h5>
                  <div class="form-group">
                    <label>Ngân hàng</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-university"></i>
                        </span>
                      </div>
                      <select name="bank"  class="form-control">
                        <option value="">-- Chọn ngân hàng --</option>
                        <option value="{{ $customer->bankAccount ?  $customer->bankAccount->bank : '' }}" selected></option>
                        <option value="ACB">ACB</option>
                        <option value="AGRIBANK">AGRIBANK</option>
                        <option value="BIDV">BIDV</option>
                        <option value="DONGA BANK">DONGA BANK</option>
                        <option value="EXIMBANK">EXIMBANK</option>
                        <option value="HDBANK">HDBANK</option>
                        <option value="MB BANK">MB BANK</option>
                        <option value="MSB">MSB</option>
                        <option value="NAM A BANK">NAM A BANK</option>
                        <option value="OCB">OCB</option>
                        <option value="SACOMBANK">SACOMBANK</option>
                        <option value="SCB">SCB</option>
                        <option value="SHB">SHB</option>
                        <option value="SHINHANBANK">SHINHANBANK</option>
                        <option value="TECHCOMBANK">TECHCOMBANK</option>
                        <option value="TPBank">TPBank</option>
                        <option value="VIB">VIB</option>
                        <option value="VIETABANK">VIETABANK</option>
                        <option value="VIETCOMBANK">VIETCOMBANK</option>
                        <option value="VIETINBANK">VIETINBANK</option>
                        <option value="VPBank">VPBank</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Số tài khoản</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-money-check-alt"></i>
                        </span>
                      </div>
                      <input name="number" type="text" maxlength="20"  class="form-control" placeholder="Số tài khoản" autocomplete="off" value="{{ $customer->bankAccount ? $customer->bank->number : '' }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Chủ tài khoản</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-user"></i>
                        </span>
                      </div>
                      <input name="name1" type="text" value="{{ $customer->bankAccount ? $customer->bank->name : '' }}" maxlength="30"  class="form-control" placeholder="Tên chủ tài khoản">
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Chi nhánh</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-map-marker"></i>
                        </span>
                      </div>
                      <input name="branch" type="text" value="{{ $customer->bankAccount ? $customer->bank->branch : '' }}" maxlength="30"  class="form-control" placeholder="Chi nhánh nếu có" autocomplete="off">
                    </div>
                  </div> --}}
                  <center>
                    <button type="submit" value="submit" class="btn-submit-form btn  bg-background" style="width:200px;color:white; font-weight:bold;">CẬP NHẬT</button>
                  </center>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<script>
  $(".btn-submit-form").click(function(){
    console.log(1);
  });
</script>
@endsection
