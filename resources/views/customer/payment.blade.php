@extends('customer.layouts.master')

@section('page_title', 'Trang thanh toán')

@section('page_id', 'page-pay')

@section('content')
    <div class="container">
        <div class="bg-white card-body rounded payment-view ">
            <div class="row">
                <div class="col-4 col-lg-4">
                    <div class="payment-widget active" data-target="banking">
                        <i class="icon fas fa-university" aria-hidden="true"></i>
                        <div class="text">Banking</div>
                    </div>
                </div>
                <div class="col-4 col-lg-4">
                    <div class="payment-widget" data-target="momo">
                        <i class="icon fa-solid fa-wallet"></i>
                        <div class="text">MoMo</div>
                    </div>
                </div>
                <div class="col-4 col-lg-4">
                    <div class="payment-widget" data-target="gift_code">
                        <i class="icon fa-solid fa-gift"></i>
                        <div class="text">Chuyển khoản</div>
                    </div>
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-body">
                    <div class="content-toggle show" id="banking" style="">
                        <div class="row">
                            <div class="col-md-7 mb-2">
                                <div class="card card-body">
                                    <div class="text-center">
{{--                                        <div class="text-intro-payment">Vui lòng chọn ngân hàng</div>--}}

                                        <div class="payment-content alert alert-success">
                                            <span id="payment-message">Vui lòng chọn ngân hàng nhận tiền</span>
                                        </div>
                                    </div>
                                    <div class="has-border-top">
                                        <div class="d-flex justify-content-center">
                                            <a class="payment-method d-flex p-1 m-1 border" style="width: 30%" href="javascript:;" data-target="VCB">
                                                <img class="w-100" style="margin-top: auto; margin-bottom: auto" src="{{asset('web/images/vietcombank.png')}}" alt="Vietcombank">
                                            </a>
                                            <a class="payment-method d-flex p-1 m-1 border" style="width: 30%" href="javascript:;" data-target="VTB">
                                                <img class="w-100" style="margin-top: auto; margin-bottom: auto" src="{{asset('web/images/viettinbank.png')}}" alt="Viettinbank">
                                            </a>
{{--                                            <a class="payment-method d-flex p-1 m-1 border" style="width: 30%" href="javascript:;" data-target="TCB">--}}
{{--                                                <img class="w-100" style="margin-top: auto; margin-bottom: auto" src="{{asset('web/images/techcombank.png')}}" alt="Techcombank">--}}
{{--                                            </a>--}}
                                            <a class="payment-method d-flex p-1 m-1 border" style="width: 30%" href="javascript:;" data-target="MB">
                                                <img class="w-100" style="margin-top: auto; margin-bottom: auto" src="{{asset('web/images/mbbank.png')}}" alt="Vietcombank">
                                            </a>
                                        </div>
{{--                                        <div class="d-flex justify-content-center">--}}
{{--                                            <a class="payment-method d-flex p-1 m-1 border" style="width: 30%" href="javascript:;" data-target="MB">--}}
{{--                                                <img class="w-100" style="margin-top: auto; margin-bottom: auto" src="{{asset('web/images/mbbank.png')}}" alt="Vietcombank">--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
                                        <div class="mt-3 border p-2 input-amount" style="display: none">
                                            <div class="text-intro-payment text-center">Vui lòng chọn nhập số tiền</div>
                                            <div class="form-group row ">
                                                <label class="col-xl-3 col-lg-3 col-form-label text-danger">Số tiền</label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input class="form-control amount" type="number" id="amount" name="amount" value="">
                                                </div>
                                            </div>
                                            <div class="kt-form__actions">
                                                <div class="row">
                                                    <div class="col-lg-3 col-xl-3"> </div>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <button type="submit" onclick="submit_pay();" class="btn btn-success">Xác nhận</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5 mb-2">
                                <div class="card card-body">
                                    <h5 class="text-success">Ghi Chú</h5>

                                    <div class="payment-note">
                                        <p><strong>Lưu ý:</strong> Chọn ngân hàng sau đó nhập số tiền. 1đ = 1 điểm VNĐ, Nếu quá 30 phút chưa có điểm vào tài khoản vui lòng inbox Admin xử lý.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                    </div>
                    <div class="content-toggle" id="momo" style="display: none;">
                        <img src="{{asset('web/images/momo.svg')}}" style="width: 100px" class=" d-block m-auto" alt="">
                        <div class="text-intro-payment text-center mt-4">Vui lòng chọn nhập số tiền</div>
                        <div class="form-group row ">
                            <label class="col-xl-3 col-lg-3 col-form-label text-right text-danger">Số tiền</label>
                            <div class="col-lg-9 col-xl-6">
                                <input class="form-control amount_momo" type="number" id="amount_momo" name="amount_momo" value="">
                            </div>
                        </div>
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-3 col-xl-3"> </div>
                                <div class="col-lg-9 col-xl-9">
                                    <button type="submit" onclick="submit_momo_pay();" class="btn btn-success">Xác nhận</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-toggle" id="gift_code" style="display: none;">
                        <img src="{{asset('web/images/qrcode.jpg')}}" style="" class="qr_bank_image d-block m-auto" alt="">
                        <div class="row pl-3 pr-3">
                            <p class="col-lg-3 col-12 mb-0">Số TK</p>
                            <input class="col-lg-6 col-8" type="text" id="value_number_bank" disabled value="19032183333666">
                            <button class="col-lg-3 col-4 btn btn-success" id="number_bank">Coppy</button>
                        </div>
                        <div class="row pl-3 pr-3 mt-2">
                            <p class="col-lg-3 col-12 mb-0">Tên người hưởng thụ</p>
                            <input class="col-lg-6 col-12" type="text" disabled value="PHUNG VAN TUAN">
                        </div>
                        <div class="row pl-3 pr-3 mt-2">
                            <p class="col-lg-3 col-12 mb-0">Ngân hàng</p>
                            <input class="col-lg-6 col-12" type="text" disabled value="Techcombank">
                        </div>
                        <div class="row pl-3 pr-3 mt-2">
                            <p class="col-lg-3 col-12 mb-0">Nội dung</p>
                            <input class="col-lg-6 col-8" type="text" disabled id="value_content_bank" value="muahang id {{customer()->user()->id}}">
                            <button class="col-lg-3 col-4 btn btn-success" id="content_bank">Coppy</button>
                        </div>
                        <p class="text-center mt-1" style="color: red; font-weight: bold">Lưu ý : Chuyển khoản đúng nội dung (Số tiền tối thiểu : 20.000 VNĐ)</p>
                        <div class="text-intro-payment text-center mt-4">Vui lòng chọn nhập số tiền</div>
                        <form action="{{route('us.payment.offline')}}" method="post">
                            @csrf
                            <div class="form-group row ">
                                <label class="col-xl-3 col-lg-3 col-form-label text-left text-danger">Số tiền</label>
                                <div class="col-lg-9 col-xl-6">
                                    <input class="form-control amount_momo" type="number" id="amount_off" name="amount_off" value="">
                                </div>
                            </div>
                            <div class="kt-form__actions">
                                <div class="row">
                                    <div class="col-lg-3 col-xl-3"> </div>
                                    <div class="col-lg-9 col-xl-9">
                                        <button type="submit" class="btn btn-success">Xác nhận đã chuyển khoản</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- <section id="pay">
    </section> --}}


@endsection
@push('css')
    <style>
        .payment-widget.active, .payment-widget:hover {
            border-color: green;
            color: green;
        }
        .payment-widget {
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
            border: 1px solid #ddd;
            cursor: pointer;
        }
        .payment-widget .text {
            font-size: 18px;
        }
        .payment-content {
            font-weight: bold;
            font-size: 1em;
        }
        .payment-view {
            box-shadow: 0 0 13px 0 rgb(82 63 105 / 5%);
            background-color: #fff;
        }
        .payment-method.active, .payment-method:hover {
            border: 1px solid green !important;
        }

    </style>
@endpush

@push('scripts')
    <script>
        $(".payment-widget").click(function (){
            $(".payment-widget").removeClass('active');
            $(this).addClass('active');
            var target = $(this).attr('data-target');
            $(".content-toggle").removeClass('show');
            $(".content-toggle").hide();
            $('#'+target+'').addClass('show');
            $('#'+target+'').show();
        });
        var method = '';
        var amount = 0;
        $(".payment-method").click(function (){
            method = $(this).attr('data-target');
            $(".payment-method").removeClass('active');
            $(this).addClass('active');
            $(".input-amount").show('slow');
        });

        $('#number_bank').click(()=>{
            navigator.clipboard.writeText($("#value_number_bank").val());
            Swal.fire(
                'Thành Công',
                'Copy số tài khoản thành công!',
                'success',
            )
        });
        $('#content_bank').click(()=>{
            navigator.clipboard.writeText($("#value_content_bank").val());
            Swal.fire(
                'Thành Công',
                'Copy nội dung thành công!',
                'success',
            )
        });
        function submit_pay() {
            // var e = $("#code").val();
            amount = $('.amount').val();
            // console.log(amount);
            if(method == '') {
                Swal.fire(
                    'Cảnh báo',
                    'Vui lòng chọn ngân hàng!',
                    'warning'
                )
            } else if(amount < 20000) {
                Swal.fire(
                    'Cảnh báo',
                    'Số tiền nạp tối thiểu là 20.000 vnđ!',
                    'warning'
                )
            } else if(amount > 90000000) {
                Swal.fire(
                    'Cảnh báo',
                    'Số tiền nạp tối đã là 90.000.000 vnđ!',
                    'warning'
                )
            } else if(amount%10 != 0) {
                Swal.fire(
                    'Cảnh báo',
                    'Số tiền nạp phải chia hết cho 10!',
                    'warning'
                )
            } else {
                Swal.fire(
                    'Thông báo',
                    'Vui lòng quét mã QR code trên cửa số mới để nạp tiền và đợi đến khi nhận được thông báo thành công, nạp xong vui lòng refesh lại trang để làm mới số tiền!',
                    'info'
                )
                let baseUrl = '<?php echo e(url('/')); ?>';
                window.open(""+baseUrl+"/payment/success?amount="+amount+"&type=bank_qr&code="+method, "_blank", "toolbar = yes, top = 500, left = 500, width = 1280, height = 1280");
            }
        }

        function submit_momo_pay() {
            // var e = $("#code").val();
            var amount_momo = $('.amount_momo').val();
            // console.log(amount);
            if(amount_momo < 20000) {
                Swal.fire(
                    'Cảnh báo',
                    'Số tiền nạp tối thiểu là 20.000 vnđ!',
                    'warning'
                )
            } else if(amount_momo%10 != 0) {
                Swal.fire(
                    'Cảnh báo',
                    'Số tiền nạp phải chia hết cho 10!',
                    'warning'
                )
            } else {
                Swal.fire(
                    'Thông báo',
                    'Vui lòng quét mã QR code trên cửa số mới để nạp tiền và đợi đến khi nhận được thông báo thành công, nạp xong vui lòng refesh lại trang để làm mới số tiền!',
                    'info'
                )
                let baseUrl = '<?php echo e(url('/')); ?>';
                window.open(""+baseUrl+"/payment/success?amount="+amount_momo+"&type=momo_qr", "_blank", "toolbar = yes, top = 500, left = 500, width = 900, height = 900");
            }
        }

    </script>
@endpush
