@extends('customer.layouts.master')

@section('page_title', 'Tài khoản Ngân Hàng')

@section('page_id', 'page-account')

@section('content')
    <section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
        <!-- Main content -->
        <section class="content" style="padding:0px;">
            <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
                <h4>Chuyển nội bộ</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{route('us.bank.bank_out')}}" class="row" method="post" id="formTransfer">
                                    @csrf
                                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Số dư tài khoản</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                      <i class="fas fa-credit-card"></i>
                                                    </span>
                                                </div>
                                                <input type="text" readonly="readonly" value="{{ number_format($user->wallet1 , 0, ',', '.') }} ₫" class="form-control" style="color:Blue;">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Người nhận (Nhập Tên đăng nhập người nhận)</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                      <i class="fa fa-user"></i>
                                                    </span>
                                                </div>
                                                <input name="receiver_id" type="text" maxlength="50" id="receiver_id" class="form-control" autocomplete="off">
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary text-white" type="button" id="check">Kiểm tra</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Họ tên</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                      <i class="fa fa-user"></i>
                                                    </span>
                                                </div>
                                                <input type="text" maxlength="50" name="receiver_name" id="receiver_name" class="form-control fullname" placeholder="Họ tên - Tự động hiển thị" >
                                            </div>
                                        </div>
                                        <div class="form-group">Kiểm tra đúng họ tên hãy thực hiện giao dịch, tránh nhầm sang người khác</div>
                                        <div class="form-group">
                                            <label>Số điểm chuyển(*)</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                      <i class="fa fa-usd">$</i>
                                                    </span>
                                                </div>
                                                <input name="money" id="money" type="text" maxlength="14" class="form-control number" autocomplete="off">
                                            </div>
                                        </div>
                                        <center>
                                            <button type="submit" class="btn bg-background" style="width: 180px; margin-bottom:10px; color:white; font-weight:bold;">Xác nhận chuyển</button>
                                        </center>
                                    </div>
                                </form>
                                <div class="card-footer" style="margin-top:10px;">
                                    <div class="" style="color: orangered;">
                                        <div>Lưu ý: Số tiền chuyển tối thiểu: 10,000 đ</div>
                                        <div>Số tiền duy trì tối thiểu ví thanh toán: 10,000</div>
                                        <div>Tài khoản của bạn cần phải được xác minh định danh tài khoản mới có thể chuyển điểm. <a href="/user/kyc">Click vào đây</a> để xác minh ngay nếu chưa xác minh </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="total-table table-responsive" style="margin-top: 20px; overflow-x: auto;">
                        <table class="table table-bordered table-striped bg-white table-hover" style="margin: 0;font-size:15px; min-width:1000px">
                            <thead>
                            <tr>
                                <th>Thời gian</th>
                                <th>Số tiền</th>
                                <th>Phí</th>
                                <th>Người gửi / Người nhận</th>
                                <th>Ghi chú</th>
                                <th>Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="text-center no-data">
                                <td colspan="10"> Chưa có dữ liệu hiển thị </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <div class="pagination-container">
                            <ul class="pagination"></ul>
                        </div>
                    </div>
                </div>
                <script>
                    $('[name="txtUserName"]').change(function() {
                        var username = $(this).val();
                        $.ajax({
                            url: "/user/GetFullName",
                            data: {
                                username: username
                            }
                        }).done(function(data) {
                            $('.fullname').val(data);
                        });
                    });

                    function getBalance2(type) {
                        var t = $('[name="txtWallet"]').val();
                        if (t == '4') {
                            $('.lbTrian').removeClass('d-none');
                        } else $('.lbTrian').addClass('d-none');
                        $.ajax({
                            url: "/transaction/GetBalance",
                            data: {
                                wallet: $('[name="txtWallet"]').val(),
                                type: type
                            },
                            type: "POST",
                            success: function(data) {
                                $('#txtAvailable').val(formatMoney(data.balance, 0));
                                $('.fee').text(data.fee);
                                $('[name="amount"]').attr('placeholder', 'Số tiền tối thiểu ' + data.min + ' ' + data.currency);
                                $('[name="amount"]').attr('min', data.min);
                            }
                        });
                    }
                </script>
            </div>
        </section>
    </section>

    <script>
        $('#check').click(function(e){
            var receiver_id = $('#receiver_id').val();
            // alert(receiver_id);
            if(receiver_id != '') //kiểm tra khác rỗng thì thực hiện đoạn lệnh bên dưới
            {
                var _token = $('input[name="_token"]').val(); // token để mã hóa dữ liệu
                $.ajax({
                    url:"{{ route('us.bank.search_receiver') }}", // đường dẫn khi gửi dữ liệu đi 'search' là tên route mình đặt bạn mở route lên xem là hiểu nó là cái j.
                    method:"GET", // phương thức gửi dữ liệu.
                    data:{receiver_id:receiver_id, _token:_token},
                    success:function(data){ //dữ liệu nhận về
                        if (data!=0) {
                            console.log(data);
                            $('#receiver_name').val(data).removeClass('bg-danger').addClass('bg-success text-white');
                        } else {
                            $('#receiver_name').val('Không tồn tại người nhận phù hợp').removeClass('bg-success').addClass('bg-danger text-white');
                        }
                    }
                });
            }
        });

    </script>

    @error('branch')
    <script>
        Swal.fire(
            'Lỗi',
            '{{$message}}',
            'error'
        )
    </script>
    @enderror
    @error('number')
    <script>
        Swal.fire(
            'Lỗi',
            '{{$message}}',
            'error'
        )
    </script>
    @enderror
    @error('bank')
    <script>
        Swal.fire(
            'Lỗi',
            '{{$message}}',
            'error'
        )
    </script>
    @enderror
    @error('name')
    <script>
        Swal.fire(
            'Lỗi',
            '{{$message}}',
            'error'
        )
    </script>
    @enderror
@endsection
