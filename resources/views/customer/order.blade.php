@extends('customer.layouts.master')

@section('page_title', 'Đơn hàng')

@section('page_id', 'page-order')

@section('content')
<section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
  <!-- Main content -->
  <section class="content" style="padding:0px;">
    <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
      <h4>Lịch sử đặt hàng</h4>
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-md-12"></div>
                <input type="hidden" name="ctl27$txtQuantity" id="ctl27_txtQuantity">
                <div class="col-md-12"> Đơn hàng của bạn </div>
                <div style="min-width:100%;overflow-x:auto;">
                  <table id="thistory" class="table table-bordered table-striped bg-white table-hover" style="font-size:15px; min-width:1000px">
                    <thead>
                      <tr>
                        <th style="min-width:120px;">Mã đơn hàng</th>
{{--                        <th style="min-width:120px;">Tên sản phẩm</th>--}}
                        <th style="min-width:120px;">Số lượng</th>
                        <th style="min-width:120px;">Ngày Mua</th>
                        <th style="min-width:120px;">Tổng tiền</th>
                          <th style="min-width:120px;">Số điểm được trừ</th>
                        <th class="font-weight-bold" style="min-width:120px;">Tổng tiền thanh toán</th>
                        <th style="min-width:120px;">Trạng thái</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($orders as $order)
                          <tr>
                              <td>{{ $order->id }}</td>
{{--                              <td>{{ $order->product->name }}</td>--}}
                              <td>{{ $order->amount}}</td>
                              <td>{{ \Carbon\Carbon::createFromDate($order->created_at)->format('H:i:s d/m/Y') }}</td>
                              <td>{{ number_format( $order->total_money, 0, ',','.')}}đ</td>
                              <td>{{ number_format( $order->point, 0, ',','.')}}đ</td>
                              <td class="font-weight-bold">{{ number_format( $order->pay_money, 0, ',','.')}}đ</td>
                              <td>{{ $order->status ? 'Đã thanh toán' : 'Chưa thanh toán'}}</td>
                          </tr>
                      @endforeach
                      @if ($orders->count()==0)
                      <tr>
                        <td class="text-center" colspan="10">Chưa có dữ liệu để hiển thị</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal" id="mdOrder" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 99999999">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content" style="width:100%;">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"> Thông tin đơn hàng </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 mb-2" style="line-height:40px;" id="orderInfo"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <style type="text/css">
        .dfl {
          display: flex;
          justify-content: space-between;
        }
      </style>
      <script src="/Scripts/pachis.js?v=0.1"></script>
    </div>
  </section>
</section>
@endsection
