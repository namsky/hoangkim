<aside class="main-sidebar sidebar-dark-primary elevation-4 bg-sidebar" style="background-image: url({{ asset('customer/sidebar.jpg') }});
background-size:cover; position: fixed;
top: 0;
left:0;">
    <!-- Brand Logo -->
    <a href="{{route('us.home')}}" class="brand-link img-logo">
      <img src="{{asset('customer/logo.png')}}" alt="TTTN Logo" class=" ">
    </a>
    <!-- Sidebar -->
    <div class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition">
      <div class="os-resize-observer-host observed">
        <div class="os-resize-observer" style="left: 0px; right: auto;"></div>
      </div>
      <div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;">
        <div class="os-resize-observer"></div>
      </div>
      <div class="os-content-glue" style="margin: 0px -8px; width: 249px; height: 696px;"></div>
      <div class="os-padding">
        <div class="os-viewport os-viewport-native-scrollbars-invisible" style="overflow-y: scroll;">
          <div class="os-content" style="padding: 0px 8px; height: 100%; width: 100%;">
            <!-- Sidebar Menu -->
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item my-2">
                  <a href="{{ route('us.home') }}" class="nav-link {{request()->is('customers') ? 'active' : ''}}">
                    <i class="nav-icon fas fa-th"></i>
                    <p>Báo Cáo Nhanh</p>
                  </a>
                </li>
                <li class="nav-item my-2">
                  <a href="{{ route('w.products') }}" class="nav-link">
                    <i class="nav-icon fas fa-th"></i>
                    <p>Mua sản phẩm lẻ</p>
                  </a>
                </li>
                <li class="nav-item my-2">
                  <a href="{{ route('us.pro.combo') }}" class="nav-link {{request()->is('customers/san-pham-dau-tu') ? 'active' : ''}}">
                    <i class="nav-icon fas fa-th"></i>
                    <p>Mua sản phẩm đồng chia</p>
                  </a>
                </li>
                <li class="nav-item my-2">
                  <a href="{{ route('us.voucher.index') }}" class="nav-link {{request()->is('customers/san-voucher') ? 'active' : ''}}">
                    <i class="nav-icon fas fa-money-bill"></i>
                    <p>Săn Voucher</p>
                  </a>
                </li>
                <li class="nav-item my-2">
                  <a href="{{ route('us.checkmember') }}" class="nav-link {{request()->is('customers/san-pham-dau') ? 'active' : ''}}">
                    <i class="nav-icon fas fa-money-bill"></i>
                    <p>Tăng Max thu nhập</p>
                  </a>
                </li>
                  @if(customer()->user()->level >= 2)
                  <li class="nav-item my-2 {{request()->is(['customers/product*']) ? 'menu-is-opening menu-open' : ''}}">
                      <a href="#" class="nav-link {{request()->is('customers/product*') ? 'active' : ''}}">
                          <i class="nav-icon fas fa-store"></i>
                          <p> Bán Hàng <i class="fas fa-angle-left right"></i>
                          </p>
                      </a>
                      <ul class="nav nav-treeview" style="display: ">
                          <li class="nav-item my-2 ">
                              <a href="{{ route('us.product.index') }}" class="nav-link {{request()->is('customers/product') ? 'active' : ''}}">
                                  <i class="far fa-circle nav-icon"></i>
                                  <p>Quản lý sản phẩm</p>
                              </a>
                          </li>
                      </ul>
                  </li>
                  @endif
              <li class="nav-item my-2">
                  <a href="{{ route('us.customer.index') }}" class="nav-link {{request()->is('customers/customer') ? 'active' : ''}}">
                      <i class="nav-icon fas fa-user"></i>
                      <p>Thành viên</p>
                  </a>
              </li>
              <li class="nav-item my-2">
                  <a href="{{ route('us.aff.index') }}" class="nav-link {{request()->is('customers/network/affiliate') ? 'active' : ''}}">
                      <i class="nav-icon fa fa-sign-out-alt"></i>
                      <p>Link kinh doanh</p>
                  </a>
              </li>
              <li class="nav-item my-2 {{request()->is(['customers/order']) ? 'menu-is-opening menu-open' : ''}}">
                  <a href="#" class="nav-link {{request()->is('customers/order') ? 'active' : ''}}">
                    <i class="nav-icon fas fa-copy"></i>
                    <p> Đơn Hàng <i class="fas fa-angle-left right"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview" style="display: ">
                    <li class="nav-item my-2 ">
                      <a href="{{ route('us.order.index') }}" class="nav-link {{request()->is('customers/order') ? 'active' : ''}}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Lịch sử đặt hàng</p>
                      </a>
                    </li>
                  </ul>
                </li>
              <li class="nav-item my-2 {{request()->is('customers/history') ? 'menu-is-opening menu-open' : ''}}">
                  <a href="#" class="nav-link ">
                    <i class="nav-icon fas fa-exchange-alt"></i>
                    <p> Giao dịch <i class="fas fa-angle-left right"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview" style="display: ">
                    <li class="nav-item my-2">
                      <a href="{{ route('us.bank.bank_out_view') }}" class="nav-link {{request()->is('customers/chuyen-diem') ? 'active' : ''}}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Chuyển điểm</p>
                      </a>
                    </li>
                      <li class="nav-item my-2">
                          <a href="{{ route('us.history') }}" class="nav-link {{request()->is('customers/history') ? 'active' : ''}}">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Lịch sử giao dịch</p>
                          </a>
                      </li>
                  </ul>
                </li>
              <li class="nav-item my-2 {{request()->is('customers/account') ? 'menu-is-opening menu-open' : ''}}">
                  <a href="#" class="nav-link ">
                    <i class="nav-icon fas fa-edit"></i>
                    <p> Cá nhân <i class="fas fa-angle-left right"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview" style="display: ">
                    <li class="nav-item my-2">
                      <a href="{{ route('us.account.index') }}" class="nav-link {{request()->is('customers/account') ? 'active' : ''}}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Thông tin cá nhân</p>
                      </a>
                    </li>
                  </ul>
                </li>
              <li class="nav-item my-2 {{request()->is(['customers/rut-diem','customers/lich-su-rut-diem']) ? 'menu-is-opening menu-open' : ''}}">
                  <a href="#" class="nav-link {{request()->is(['customers/rut-diem', 'customers/lich-su-rut-diem']) ? 'active' : ''}}">
                      <i class="nav-icon fas fa-money-bill"></i>
                      <p> Rút điểm <i class="fas fa-angle-left right"></i>
                      </p>
                  </a>
                  <ul class="nav nav-treeview" style="display: ">
                      <li class="nav-item my-2 ">
                          <a href="{{ route('us.payment.withdraw') }}" class="nav-link {{request()->is('customers/rut-diem') ? 'active' : ''}}">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Rút điểm</p>
                          </a>
                      </li>
                      <li class="nav-item my-2 ">
                          <a href="{{ route('us.payment.history_withdraw') }}" class="nav-link {{request()->is('customers/lich-su-rut-diem') ? 'active' : ''}}">
                              <i class="far fa-circle nav-icon"></i>
                              <p>Lịch sử rút điểm</p>
                          </a>
                      </li>
                  </ul>
              </li>
                <li class="nav-item my-2">
                  <a href="{{ route('us.logout') }}" class="nav-link">
                    <i class="nav-icon fa fa-sign-out-alt"></i>
                    <p>Đăng xuất</p>
                  </a>
                </li>
{{--                <li class="nav-item my-2">--}}
{{--                  <a href="tel:0982768265" class="nav-link">--}}
{{--                    <i class="nav-icon fa fa-phone"></i>--}}
{{--                    <p>Hotline: 0982.768.265</p>--}}
{{--                  </a>--}}
{{--                </li>--}}
{{--                <li class="nav-item my-2">--}}
{{--                  <a target="_blank" href="https://zalo.me/0982768265" class="nav-link">--}}
{{--                    <i class="nav-icon fa fa-info"></i>--}}
{{--                    <p>Zalo hỗ trợ</p>--}}
{{--                  </a>--}}
{{--                </li>--}}
              </ul>
            </nav>
            <!-- /.sidebar-menu -->
          </div>
        </div>
      </div>
      <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable os-scrollbar-auto-hidden">
        <div class="os-scrollbar-track">
          <div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div>
        </div>
      </div>
      <div class="os-scrollbar os-scrollbar-vertical os-scrollbar-auto-hidden">
        <div class="os-scrollbar-track">
          <div class="os-scrollbar-handle" style="height: 97.0752%; transform: translate(0px, 0px);"></div>
        </div>
      </div>
      <div class="os-scrollbar-corner"></div>
    </div>
    <!-- /.sidebar -->
  </aside>
