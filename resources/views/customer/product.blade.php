@extends('customer.layouts.master')

@section('page_title', 'Trang sản phẩm')

@section('page_id', 'page-product')

@section('content')
<div class="container">
    <div class="row align-self-stretch">
        @foreach ($products as $product)
            <div class="col-xl-4 col-lg-4 col-md-6 mb-4">
                <div class="bg-white rounded shadow-sm border">
                    <div class="p-4 p-md-0" id="box-product">
                        <img src="{{asset($product->image)}}" alt="" class="h-100 img-fluid card-img-top">
                    </div>
                    
                    <div class="p-2 pb-0 text-center" id="info-product">
                        <h5 class="text-dark "> <a href="" class="text-decoration-none text-dark">{{$product->name}}</a></h5>
                        <h6 class=" text-muted mx-1">Giá: <span class="text-danger" style="font-size:120%;text-decoration-line: {{$product->price_sales ? 'line-through':''}}">{{number_format($product->price, 0, ',', '.')}} vnđ</span></h6>
                        {{-- <p class="small text-muted mb-0">{!!$product->content!!}</p> --}}
                        @if ($product->price_sales)
                            <h6 class=" text-muted mx-1">Giá khuyến mãi: <span style="font-size:125%;" class="text-danger">{{number_format($product->price_sales, 0, ',', '.')}} vnđ</span></h6>
                        @endif
                        {!!$product->description!!}
                    </div>
                    <div class=" text-center">
                        <a href="{{route('us.pay.product',$product)}}" class="text-decoration-none  my-3 btn btn-primary col-5">Mua ngay</a>
                    </div>
                </div>
            </div>            
        @endforeach
    </div>
</div>        
@push('scripts')
<script>
</script>
@endpush
  
@endsection
