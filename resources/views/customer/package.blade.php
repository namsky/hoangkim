@extends('customer.layouts.master')

@section('page_title', 'Trang gói sản phẩm')

@section('page_id', 'page-product-package')

@section('content')
<div class="container">
    @if ($package)
        <div class="row pb-4 ">
            <div class="col-sm-12">
                <h2 class="title text-center">{{ $package->name}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-sm-12">
                <div class="images ">
                    <img src="{{ $package->image}}" class="rounded" style="width:100%" alt="ảnh gói sản phẩm">
                </div>
            </div>
            <div class="col-md-5 col-sm-12">
                <p class="mt-3">{{$package->description}}</p>
                {!!$package->content!!}
                <div class="price">
                    <span style="font-size:100%; font-weight:500">Giá chỉ: </span>
                    {{-- @if ($package->price)
                        <span style="font-size:125%; color:red;text-decoration-line: {{$package->price_sales ? 'line-through':''}}" class="">{{number_format($package->price, 0, ',', '.')}} vnđ</span>
                    @endif --}}
                    @switch(customer()->user()->level)
                        @case(1)
                        <span style="font-size:125%; color:red;" class="">{{number_format($package->f1_price, 0, ',', '.')}} vnđ</span>
                        @break
                        @case(2)
                        <span style="font-size:125%; color:red;" class="">{{number_format($package->f2_price, 0, ',', '.')}} vnđ</span>
                        @break
                        @case(3)
                        <span style="font-size:125%; color:red;" class="">{{number_format($package->f3_price, 0, ',', '.')}} vnđ</span>
                        @break
                    @endswitch
                </div>
                <div class="price">
                    @if ($package->price_sales)
                        <span style="font-size:100%; font-weight:500">Giá khuyến mãi: </span>
                        <span style="font-size:125%; color:red" class="">{{number_format($package->price_sales, 0, ',', '.')}} vnđ</span>
                    @endif
                </div>
                <a href="{{route('us.pay.package',$package)}}" class="btn btn-primary buy col-4 col-md-7 col-lg-6 col-xl-5 my-3">Mua Ngay</a>
            </div>
        </div>        
    @endif
</div>        
@push('scripts')
<script>
</script>
@endpush
  
@endsection