@extends('customer.layouts.master')

@section('page_title', 'Trang thanh toán')

@section('page_id', 'page-pay')

@section('content')
<section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
    <!-- Main content -->
    <section class="content" style="padding:0px;">
      <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
        <h4>Xác nhận đơn hàng </h4>
        <div class="row">
          <div class="col-md-12">
            <form class="card" id="formCheckout" method="post">
              <div class="card-body">
                <div class="row">
                  <div id="divDetail" style="min-width: 100%; overflow-x: auto;">
                    <div class="form-group">
                      <label>Chọn hình thức thanh toán</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fas fa-wallet"></i>
                          </span>
                        </div>
                        <select name="txtWallet" id="dlWallet" class="form-control" onchange="getBalance('transfer')">
                          <option value="">Chọn ví</option>
                          <option selected="" value="0">Ví tiền thanh toán</option>
                        </select>
                      </div>
                      <div class="d-none" style=" text-align: justify; background-color: orange; color: white; padding: 10px; margin-top: 5px; border-radius: 5px; ">
                        <i>Ví mua hàng chỉ áp dụng cho những tài khoản đã kích hoạt đơn hàng trước đó, không áp dụng cho lần đầu tiên kích hoạt đơn hàng.</i>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Số dư ví</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fas fa-wallet"></i>
                          </span>
                        </div>
                        <input id="txtAvailable" type="text" value="0" readonly="readonly" disabled="disabled" class="aspNetDisabled form-control" style="color:Blue;font-weight:bold;">
                      </div>
                      <span style="display:none">Bạn cần chuyển số dư giữa các ví, <a href="/transaction/transfer">bấm vào đây</a>
                      </span>
                    </div>
                    <label>Thông tin sản phẩm</label>
                    <table id="example1" class="table table-bordered table-striped" style="font-size: 14px;">
                      <thead>
                        <tr>
                          <th>Tên sản phẩm</th>
                          <th style="text-align: right;">Giá X SL = Thành tiền</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>COMBO TAM THÔNG DƯỠNG SINH 1</td>
                          <td style="text-align:right;color:blue;">100,000,000 x 1 = 100,000,000</td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th colspan="2" style="text-align: right; font-size:15px;"> Tổng tiền: <span id="lblTotal" style="color:Blue;">100,000,000</span>
                          </th>
                        </tr>
                      </tfoot>
                    </table>
                    <h5>THÔNG TIN NHẬN HÀNG</h5>
                    <div class="form-group">
                      <label>Họ và tên người nhận (*)</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fas fa-user"></i>
                          </span>
                        </div>
                        <input name="name" type="text" value="{{auth('user')->user()->name}}" class="form-control" placeholder="Bắt buộc nhập" required="" style="text-transform:uppercase;">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Điện thoại người nhận (*)</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fas fa-phone"></i>
                          </span>
                        </div>
                        <input name="phone" type="text" value="{{auth('user')->user()->phone}}" maxlength="10" class="form-control numbers" placeholder="SĐT người nhận">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Địa chỉ</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="fas fa-map-marked-alt"></i>
                          </span>
                        </div>
                        <input name="address" type="text" value="{{auth('user')->user()->address}}" class="form-control " placeholder="Địa chỉ nhận hàng">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row" style="margin: 0px !important;">
                        <div class="col-md-12">
                          <div class="mt-2" style="text-align:justify;">
                            <div>Lưu ý: <br>- Mọi thông tin đặt đơn hàng phải chính xác để công ty giao hàng, nếu sai thông tin công ty không chịu trách nhiệm về tình trạng giao hàng trễ. </div>
                            <div>- Số điện thoại phải là số của người nhận hàng để công ty hoặc kho giao hàng liên hệ được để xác nhận đơn.</div>
                            <div>- Nhận hàng tại công ty: Quý khách sẽ lên văn phòng tại các cơ sở của công ty để nhận hàng trực tiếp.</div>
                            <div> - Nhận hàng tại kho: Quý khách sẽ đến kho đã chọn để nhận hàng trực tiếp. </div>
                            <div> - Nhận hàng tại nhà: Hàng sẽ được gửi về địa chỉ nhận hàng của Quý khách, công ty hoặc kho gần nhất sẽ vận chuyển đơn hàng tới Quý khách. </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="row">
                    <div class="col-md-12 text-center">
                      <input type="hidden" value="1" name="congHH">
                      <button type="submit" class="btn btn-block bg-background col-md-2" style="font-weight:bold;float: right; color:white; font-size:20px; margin-top: 0px !important;">Xác nhận</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
<style type="text/css">
.divPolicy p {
text-align: justify;
line-height: 30px;
}
</style>


@endsection
