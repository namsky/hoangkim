@extends('customer.layouts.master')

@section('page_title', 'Link kinh doanh')

@section('page_id', 'page-order')

@section('content')
    <section class="content-wrapper bg-background-main" style="min-height: 682.8px;">
        <!-- Main content -->
        <section class="content" style="padding:0px;">
            <div class="container-fluid" style="padding-top:10px; margin-bottom: 50px; padding: 10px !important;">
                <h4>Danh sách phát triển của bạn</h4>
                <div class="container-fluid">
                    <div class="col-md-12"> </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="margin-top: 10px; text-align: right;">
                            <span id="spanlink" class="btn btn-sm bg-background text-white" style="margin-bottom: 5px;" onclick="copyToClipboard('https://{{ request()->getHttpHost().'/ref'.'/'.customer()->user()->username }}');">
                                Link bán hàng của bạn, chạm để copy
                                <span >https://{{ request()->getHttpHost().'/ref'.'/'.customer()->user()->username }}</span>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12" style="margin-top: 10px;">
                            <div class="card card-widget widget-user-2">
                                <div class="widget-user-header bg-default" style="padding: 10px;">
                                    <h5>Danh sách đơn hàng</h5> </div>
                                <div class="card-footer p-0">
                                    <div>
                                        <div class="total-table table-responsive">
                                            <table class="table bg-white table-hover" style="margin: 0; min-width: 960px;">
                                                <thead>
                                                <tr class="text-center">
                                                    <th>Mã đơn hàng</th>
                                                    <th>Trạng thái</th>
                                                    <th>Ngày tạo</th>
                                                    <th>Số lượng</th>
                                                    <th>Tổng thanh toán</th>
                                                    <th>Họ tên KH</th>
                                                    <th>Số điện thoại KH</th>
                                                    <th>Địa chỉ</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($orders)
                                                    @foreach($orders as $order)
                                                    <tr class="text-center no-data">
                                                        <td> {{$order->id}} </td>
                                                        <td>
                                                            @if($order->status == 0)
                                                                <span>Chưa thanh toán</span>
                                                            @else
                                                                <span class="text-success">Đã thanh toán</span>
                                                            @endif
                                                        </td>
                                                        <td> {{ \Carbon\Carbon::createFromDate($order->created_at)->format('H:i:s d/m/Y') }} </td>
{{--                                                        <td> {{$order->product->name}} </td>--}}
                                                        <td> {{$order->amount}} </td>
                                                        <td> {{number_format($order->total_money)}} </td>
{{--                                                        <td> {{number_format($order->pay_money)}} </td>--}}
                                                        <td> {{$order->name}} </td>
                                                        <td> {{$order->phone}} </td>
                                                        <td> {{$order->address}} </td>
                                                    </tr>
                                                    @endforeach
                                                @else
                                                    <tr class="text-center no-data">
                                                        <td colspan="7"> Chưa có dữ liệu </td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
@push('scripts')
    <script>
        $('#spanlink').click(() => {
            navigator.clipboard.writeText(`{{ request()->getHttpHost().'/ref'.'/'.customer()->user()->username }}`);
            $('.copy').text('Copy thành công');
            Swal.fire('Thành Công', 'Copy thành công link!', 'success', )
        });
    </script>
@endpush
