@extends('web.layouts.master')
@section('page_title', 'Giỏ hàng')
@section('page_thumb', asset('hoangviet/img/vechungtoi.jpg'))
@section('content')
    <div class="container pt-5 mb-5">
        <h2 class="cart-title">Giỏ hàng</h2>
        <form id="formPay" action="{{route('w.cart.update')}}" method="post" class="mb-5 pb-5">
            @csrf
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="15%">Ảnh sản phẩm</th>
                        <th width="20%">Tên sản Phẩm</th>
                        <th width="15%">Giá</th>
                        <th width="10%">Số lượng</th>
{{--                        @if (customer()->check())--}}
{{--                        <th width="20%">Chiết khấu</th>--}}
{{--                        @endif--}}
                        <th width="20%">Thành tiền</th>
                        <th width="10%">Hành động</th>
                    </tr>
                </thead>
                <tbody>

                @csrf
                @foreach($carts as $cart)
                    <tr>
                        <td><img class="product-image" src="{{$cart->options->image}}" alt=""></td>
                        <td>{{$cart->name}}</td>
                        <td>{{number_format($cart->price)}} VNĐ</td>
                        <td>
                            {{-- <input class="minus is-form" type="button" value="-" > --}}
                            <input style="padding: 0" aria-label="quantity" class="input-qty" max="1000" min="1" form="formPay"  name="cart[{{$cart->rowId}}]" type="number" value="{{number_format($cart->qty)}}" >
                            {{-- <input class="plus is-form" type="button" value="+"> --}}
                        </td>
{{--                        @if (customer()->check())--}}
{{--                        <td>{{ auth('user')->user()->grant->percent }}%</td>--}}
{{--                        @endif--}}
                        <td>{{number_format($cart->price*$cart->qty)}} VNĐ</td>
                        <td>
                            <a href="{{route('w.cart.destroy',$cart->rowId)}}"  onClick="alert('Bạn có chắc chắn xóa sản phẩm khỏi giỏ hàng ?')" class="btn btn btn-sm btn-danger rounded">Xóa</a>
                        </td>
                    </tr>

                @endforeach

                <tr>
                    <td colspan="4"></td>
                    <td class="font-weight-bold">Tổng : {{$total}} VNĐ</td>
{{--                    <td class="font-weight-bold">--}}
{{--                        <input class="form-check-input" type="checkbox"> Thanh toán bằng điểm--}}
{{--                    </td>--}}
                </tr>
                </tbody>
            </table>
{{--            @if(customer()->user() && customer()->user()->point >= 10000)--}}
{{--            <p class="form-check-label d-flex justify-content-end align-center" for="pay_point">--}}
{{--                <input class="form-check-input" id="pay_point" type="checkbox" name="pay_point" style="width: auto; height: auto; opacity: 1; position: inherit; margin-right: 1rem">--}}
{{--                <span>Thanh toán bằng điểm (<span class="text-danger">{{number_format(customer()->user()->point)}} đ</span>)</span>--}}
{{--            </p>--}}
{{--            @endif--}}
            <div class="d-flex justify-content-between">
                <a class="btn btn-info" href="{{route('w.products')}}">Tiếp tục mua hàng</a>
                <div class="d-flex">
                    <button tyle="submit" class="btn btn-primary mx-2" href="">Cập nhật giỏ hàng</button>
                    <a class="btn btn-success mx-2" href="{{ route('w.customer_pay') }}">Đặt hàng</a>
                </div>
            </div>
        </form>
    </div>
@endsection
@push('css')
<style>
    .justify-content-between {
        justify-content: space-between;
    }
    .mx-2 {
        margin-left: 0.5em;
        margin-right: 0.5em;
    }
    .cart-title {
        padding-top: 20px;
        margin-bottom: 20px;
    }
    .font-weight-bold {
        font-weight: bold;
    }
    .product-image {
        width: 100%;
    }

    .buttons_added {
        opacity: 1;
        display: inline-block;
        display: -ms-inline-flexbox;
        display: inline-flex;
        white-space: nowrap;
        vertical-align: top;
    }

    .is-form {
        overflow: hidden;
        position: relative;
        background-color: #f9f9f9;
        height: 2.5rem;
        width: 2.5rem;
        padding: 0;
        text-shadow: 1px 1px 1px #fff;
        border: 1px solid #ddd;
    }

    .is-form:focus,
    .input-text:focus {
        outline: none;
    }

    .is-form.minus {
        border-radius: 4px 0 0 4px;
    }

    .is-form.plus {
        border-radius: 0 4px 4px 0;
    }

    .input-qty {
        background-color: #fff;
        height: 2.5rem;
        text-align: center;
        /*font-size: 2rem;*/
        display: inline-block;
        vertical-align: top;
        margin: 0;
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
        border-left: 0;
        border-right: 0;
        padding: 0;
    }

    .input-qty::-webkit-outer-spin-button,
    .input-qty::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
@endpush

@push('scripts')
    <script>
        $(document).ready(function(){
            $('input.input-qty').each(function() {
                var $this = $(this),
                    qty = $this.parent().find('.is-form'),
                    min = Number($this.attr('min')),
                    max = Number($this.attr('max'))
                if (min == 0) {
                    var d = 0
                } else d = min
                $(qty).on('click', function() {
                    d = Number($this.val());

                    if ($(this).hasClass('minus')) {
                        if (d > min) d += -1
                    } else if ($(this).hasClass('plus')) {
                        var x = Number($this.val()) + 1
                        if (x <= max) d +=  1
                    }
                    // console.log(d);

                    $this.attr('value', d).val(d)
                })
                $('input.input-qty').change(function(){
                    if ($(this).val() < 1 ) {
                        $(this).val(1);
                    }
                })
            })



            // $("#pay_point").change(function () {
            //     if ($('#pay_point').is(":checked"))
            //     {
            //         console.log($(this).val());
            //     }
            // })
        });
    </script>
@endpush

