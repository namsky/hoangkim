<section class="doitac">
  <div class="container">
    <h2>Đội ngũ chuyên gia</h2>
    <div class="owl-carousel owl-theme slider-doitac">
      <div class="item">
        <div class="box_chuyengia">
          <div class="picture_chuyengia">
            <div class="imgdoitac">
              <img src="{{ asset('hoangviet/img/chuyengia/nguyenlandung.png') }}" alt="dt1">
            </div>
            <div class="name_chuyengia">GS. TS. NGND. NGUYỄN LÂN DŨNG</div>
          </div>
          <div class="text_chuyengia">
            <p><i>Vị nhà giáo của toàn dân cả cuộc đời nghiên cứu khoa học và trăn trở về sản phẩm Tảo Xoắn Spirulina, sản phẩm rất quý và hiếm. GS. TS. NGND. NGUYỄN LÂN DŨNG chia sẻ: Tôi chỉ mong mỗi người dân Việt Nam có Tảo Xoắn Spirulina để sử dụng mỗi ngày. Tôi tin chắc sẻ nâng cao sức đề kháng, nâng cao sức khỏe, nâng cao tuổi thọ và đẩy lùi bệnh tật cho Người dân Việt Nam.</i></p>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="box_chuyengia">
          <div class="picture_chuyengia">
            <div class="imgdoitac">
              <img src="{{ asset('hoangviet/img/chuyengia/nguyenvokyanh.jpg') }}" alt="dt1">
            </div>
            <div class="name_chuyengia">PGS. TS. NGND NGUYỄN VÕ KỲ ANH. PHÓ CHỦ TỊCH HỘI GDCSSK CỘNG ĐỒNG VIỆT NAM</div>
          </div>
          <div class="text_chuyengia">
            <p><i>Sữa non là rất quý tuy nhiên sữa non còn được kết hợp với tảo xoắn spirulina và đông trùng hạ thảo thì còn tốt hơn rất nhiều. tảo xoắn spirulina có rất nhiều công dụng bổ ích cho con người, đông trùng hạ thảo cũng vậy. tôi tin chắc rằng sự kết hợp giữa sữa non, tảo xoắn spirulina và đông trùng hạ thảo là một dòng sữa rất khác biệt hiện nay và đáp ứng nhu cầu rất tốt cho sức khỏe cộng đồng người dân việt nam</i></p>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="box_chuyengia">
          <div class="picture_chuyengia">
            <div class="imgdoitac">
              <img src="{{ asset('hoangviet/img/chuyengia/ledinhtien.jpg') }}" alt="dt1">
            </div>
            <div class="name_chuyengia">TS. NGND. LÊ ĐÌNH TIẾN - NGUYÊN THỨ TRƯỞNG BỘ KHOA HỌC CÔNG NGHỆ</div>
          </div>
          <div class="text_chuyengia">
            <p><i>Tôi đánh giá rất cao về tầm nhìn, sứ mệnh và chiến lược của công ty cổ phần tập đoàn khoa học công nghệ hoàng việt. với đội ngũ lãnh đạo trẻ có tâm, có tầm và một mục tiêu rất cụ thể chúng tôi tin chắc rằng tập đoàn hoàng việt sẻ là một doanh nghiệp tiên phong trong lĩnh vực sản xuất, kinh doanh sản phẩm phục vụ sức khỏe cộng đồng việt nam</i></p>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="box_chuyengia">
          <div class="picture_chuyengia">
            <div class="imgdoitac">
              <img src="{{ asset('hoangviet/img/chuyengia/trandinglong.jpg') }}" alt="dt1">
            </div>
            <div class="name_chuyengia">GS. VS. TSKH. TRẦN ĐÌNH LONG CHỦ TỊCH HỘI GIỐNG CÂY TRỒNG VIỆT NAM</div>
          </div>
          <div class="text_chuyengia">
            <p><i>Đông Trùng Hạ Thảo là một sản phẩm rất quý, có rất nhiều thành phần và dưỡng chất để nâng sức khỏe con người. Hỗ trợ phòng ngừa và đẩy lùi bệnh tật đặc biệt là những căn bệnh nguy hiểm, nan y. Tuy nhiên chúng ta lưu ý nên sử dụng sản phẩm của các đơn vị sản xuất có uy tín và thương hiệu. Đương nhiên đơn vị uy tín thương hiệu thì đảm bảo sản phẩm chất lượng và giá thành sẻ cao hơn. Theo tôi chúng ta không nên vì giá rẻ để mua và sử dụng sản phẩm trôi nỗi, không rõ nguồn gốc có thể làm ảnh hưởng đến sức khỏe của chúng ta</i></p>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="box_chuyengia">
          <div class="picture_chuyengia">
            <div class="imgdoitac">
              <img src="{{ asset('hoangviet/img/chuyengia/ngoquocluat.png') }}" alt="dt1">
            </div>
            <div class="name_chuyengia">THS. NGÔ QUỐC LUẬT</div>
          </div>
          <div class="text_chuyengia">
            <p><i>Sau nhiều năm làm việc cùng lãnh đạo TẬP ĐOÀN KHOA HỌC CÔNG NGHỆ HOÀNG VIỆT. Tôi đánh giá rất cao về năng lực của lãnh đạo Hoàng Việt Group. Họ là những người trẻ, năng động sáng tạo, có đam mê, khát vọng sản xuất kinh doanh những sản phẩm "VÌ SỨC KHỎE CỘNG ĐỒNG VIỆT". Sự ra đời của TẬP ĐOÀN KHOA HỌC CÔNG NGHỆ HOÀNG VIỆT sẻ là một bước đột phá mới cho lĩnh vực TẢO XOẮN SPIRULINA VÀ ĐÔNG TRÙNG HẠ THẢO trong thời gian tới.</i></p>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</section>
