<section class="danhmuc-1">
    <div class="container">
      <div class="row wrap_pro">
        <div class="title-danhmuc">
          <h2>
            <img src="{{ asset('hoangviet/img/sau-covit.png') }}" alt="">Sản phẩm mới nhất
          </h2>
          <p class="xemthem">
            <a href="https://thuanphongpharma.com/san-pham-moi-nhat">Xem thêm</a>
          </p>
        </div>
          @foreach($product_sales as $product)
        <div class="col-md-2 col-sm-4 col-xs-6 col_pro">
          <div class="itme-2-danhmuc">
            <div class="img-sanpham">
              <a href="{{route('w.pro.index', $product->id)}}">
                <img src="{{$product->image}}" alt="Kem Ủ Tóc Nuôi Dưỡng Sâu Vatika 500g">
              </a>
            </div>
            <div class="tt-sanpham">
              <div class="name-product">
                <h3>
                  <a href="{{route('w.pro.index', $product->id)}}"> {{$product->name}} </a>
                </h3>
              </div>
              <div class="lienhe-product">
                <p>
                  <b>Giá: {{number_format($product->price)}} đ </b>
                  <img src="{{ asset('hoangviet/img/icon-cart.png') }}" alt="{{$product->name}}">
                </p>
              </div>
            </div>
          </div>
        </div>
          @endforeach
      </div>
    </div>
  </section>
