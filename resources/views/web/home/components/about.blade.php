<section class="about_home">
	<div class="container">
		
		<div class="row">
			<div class="col-md-4">
				<div class="desc_content_home">
					<h2 class="title_gioithieu">TẦM NHÌN VÀ SỨ MỆNH</h2>
					<p class=""><strong>Tầm nhìn:</strong>: Trở thành doanh nghiệp khoa học công nghệ chuyên nghiệp</p>
					<p class="text-center"><strong>"Đủ tâm - đủ tầm"</strong></p>
                    <p class=""><strong>Sứ mệnh:</strong>: Vì sức khỏe cộng đồng & hạnh phúc gia đình Việt</p>
                    <p class=""><strong>Mục tiêu:</strong>: Cùng các doanh nghiệp trong lĩnh vực chăm sóc sức khỏe cộng đồng</p>
                    <p class="text-center"><strong>"Kết nối - hợp tác - đồng hành"</strong></p>
                    <p class=""><strong>Chiến lược:</strong>: Trở thành doanh nghiệp khoa học công nghệ đến năm 2025
                        <br>Trở thành công ty đại chúng đến năm 2028</p>

{{--					<p><img src="img/Thiet-ke-chua-co-ten.png" class="w_100" alt=""></p>--}}
{{--					<p class="text-center"><em>Tảo xoắn Đại Việt bước vào ngành tảo xoắn Spirulina với số vốn khổng lồ</em></p>--}}
{{--					<p class="text-center"><a href="" class="btn btn-success">Tìm hiểu thêm</a></p>--}}
				</div>
			</div>
			<div class="col-md-8">
				<div class="slide_gioithieu owl-carousel">
					<div class="item">
						<div class="box_image_cn">
							<img src="{{ asset('hoangviet/img/tamnhin.jpg') }}" alt="">
						</div>
					</div>
					<div class="item">
						<div class="box_image_cn">
							<img src="{{ asset('hoangviet/img/tamnhin2.jpg') }}" alt="">
						</div>
					</div>
{{--					<div class="item">--}}
{{--						<div class="box_image_cn">--}}
{{--							<img src="{{ asset('hoangviet/img/tamnhin.jpg') }}" alt="">--}}
{{--						</div>--}}
{{--					</div>--}}
{{--					<div class="item">--}}
{{--						<div class="box_image_cn">--}}
{{--							<img src="{{ asset('hoangviet/img/poster-thanh-phan-kho-doc-1.jpg') }}" alt="">--}}
{{--						</div>--}}
{{--					</div>--}}
				</div>
			</div>
		</div>
	</div>
</section>


<style type="text/css">
section.about_home {
    padding: 30px 0;
	/* background: #f5f5f5; */
}
h2.title_gioithieu {
    font-size: 20px;
    margin: 0 0 7px;
    color: #007732;
    font-weight: 700;
    text-transform: uppercase;
}
.desc_content_home p {
    font-size: 16px;
    line-height: 24px;
    font-family: 'UTM Helve';
        padding: 5px 0;
}
.desc_content_home p em {
    font-size: 14px;
}
.box_image_cn {
    border: 1px solid #ddd;
    border-radius: 12px;
    height: 485px;
    overflow: hidden;
}
.box_image_cn img {
    border-radius: 12px;
    height: 100%;
    object-fit: cover;
}
</style>
