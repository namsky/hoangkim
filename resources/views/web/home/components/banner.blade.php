<section class="banner_home">
  <div class="box_slide_banner">
    <div class="silide_banner">
      @foreach ($banners as $banner)
        <div class="item">
          <div class="banner_picture">
            <a href="{{ $banner->url === null ? '#' : $banner->url }}" style="display: block">
              <img src="{{ $banner->image }}" alt="{{$banner->name}}">
            </a>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</section>