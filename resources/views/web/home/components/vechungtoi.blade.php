<section class="noidung-dmuc-chinh">
  <div class="container">
    <div class="row flex-dmuc" style="height: 100%;">
      <div class="col-12 col-lg-6">
        <div class="noidung-dmuc">
          <div>
              <p><strong>THƯ NGỎ</strong></p>
              <p><i>Kính thưa quý vị khách hàng, đối tác !</i></p>
            <p>CÔNG TY CỔ PHẦN TẬP ĐOÀN KHOA HỌC CÔNG NGHỆ HOÀNG VIỆT,
              Công ty ra đời trong bối cảnh được Đảng, Chính Phủ ủng hộ tuyệt đối về việc xây dựng và phát triển Doanh
              nghiệp đổi mới sáng tạo, ứng dụng khoa học, công nghệ vào việc tổ chức nghiên cứu, sản xuất, kinh doanh.
              Công ty chúng tôi với định hướng, chiến lược phát triển sản phẩm và xây dựng thương hiệu;
              với sứ mệnh cam kết cung cấp những sản phẩm chất lượng cao,
              hiệu quả và an toàn trong suốt hành trình <strong>“chăm sóc sức khỏe cộng đồng”. </strong>
            </p>
          </div>
            <a class="btn btn-success" href="{{route('w.letter')}}">Xem thêm</a>
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <div class="img-left-nd-dmuc" style="height: 100%">
          <img src="{{ asset('hoangviet/img/vechungtoi.jpg') }}" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
