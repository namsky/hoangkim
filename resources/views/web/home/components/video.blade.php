<section class="video_home">
	<div class="container">
		<div class="title-danhmuc">
      <h2><img src="{{ asset('hoangviet/img/video-play-icon-11.png') }}" alt="">Video review</h2>
      <p class="xemthem"><a href="https://thuanphongpharma.com/goc-suc-khoe">Xem thêm</a></p>
    </div>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="box_iframe_video">
					<iframe width="100%" height="250" src="https://www.youtube.com/embed/tpUEsWLFKB8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="box_iframe_video">
					<iframe width="100%" height="250" src="https://www.youtube.com/embed/6uW7wFkNky4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="box_iframe_video">
					<iframe width="100%" height="250" src="https://www.youtube.com/embed/46v8rNWWzGM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>

