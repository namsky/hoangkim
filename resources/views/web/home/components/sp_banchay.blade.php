<section class="sanphambanchay">
    <div class="container">
      <div class="">
        <div class="title-danhmuc">
          <h2>
            {{-- <img src="{{ asset('hoangviet/img/sau-covit.png') }}" alt=""> --}}
            Sản phẩm bán chạy
          </h2>
          {{-- <p class="xemthem">
            <a href="https://thuanphongpharma.com/thuc-pham-chuc-nang">Xem thêm</a>
          </p> --}}
        </div>
      </div>
      <div class="row">
        <div class="" id="box-product">
            @foreach($product_sales as $product)
              <div class="col-12 col-sm-6 col-lg-3">
                <div class="itme-2-danhmuc">
                  <div class="img-sanpham">
                    <a href="{{route('w.pro.index', ['category' =>  $product->category->category_name , 'product' => $product->slug])}}">
                      <img src="{{$product->image}}" alt="{{$product->name}}">
                    </a>
                      @if($product->hot == 1)
                    <span class="image_hot"><img src="{{ asset('hoangviet/img/hot-icon.gif') }}"></span>
                      @endif
                  </div>
                  <div class="tt-sanpham">
                    <div class="name-product">
                      <h3>
                        <a href="{{route('w.pro.index', ['category' => $product->category->slug, 'product' => $product->slug])}}"> {{$product->name}} </a>
                      </h3>
                    </div>
                    <div class="lienhe-product">
                      <p>
                        <b>Giá: {{number_format($product->price)}} đ </b>
                          <a href="{{route('w.cart.create', $product->id)}}">
                              <img src="{{ asset('hoangviet/img/icon-cart.png') }}" alt="{{$product->name}}">
                          </a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          <!-- end tem pro home -->
        </div>
      </div>
    </div>
  </section>
