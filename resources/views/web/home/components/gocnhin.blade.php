<section class="chuyenmuc-suckhoe">
  <div class="container">
    <div class="title-danhmuc">
      <h2>
        <img src="{{ asset('hoangviet/img/chuyenmuc.png') }}" alt="">Góc sức khỏe
      </h2>
      <p class="xemthem">
        <a href="{{ route('w.blogs') }}">Xem thêm</a>
      </p>
    </div>
    <div class="noidung-chuyenmuc">
      <div class="row">
        <div class="col-md-5">
          <div class="item-chuyenmuc-left">
            <div class="img-chuyenmuc-left">
              <a href="{{ route('w.single_blog', ['category_slug'=>$blog->category->slug, 'blog_slug' => $blog->slug]) }}">
                <img src="{{ $blog->image }}" alt="{{ $blog->name }}">
              </a>
            </div>
            <div class="noidung-chuyenmuc-left">
              <h3>
                <a href="{{ route('w.single_blog', ['category_slug'=>$blog->category->slug, 'blog_slug' => $blog->slug]) }}">{{ $blog->name }}</a>
              </h3>
                <div>
                    <p style="text-align:justify">
                    <span style="font-size:14px">{{ $blog->description}}
                    </span>
                    </p>
                </div>
              <div class="date-chuyenmuc">
                <b>Admin | </b>
                <span> {{ $blog->created_at }}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <div class="sum-nd-item">
            @foreach ($blogs as $blog)
            <div class="item-chuyenmuc-right">
              <div class="img-chuyenmuc-right">
                <a href="{{route('w.single_blog',['category_slug' => $blog->category->slug,'blog_slug' => $blog->slug])}}">
                  <img src="{{ $blog->image }}" alt="{{ $blog->name }}">
                </a>
              </div>
              <div class="noidung-chuyenmuc-right">
                <h3>
                  <a href="{{route('w.single_blog',['category_slug' => $blog->category->slug,'blog_slug' => $blog->slug])}}">{{ $blog->name }}</a>
                </h3>
                <div class="date-chuyenmuc">
                  <b></b>
                  <span>{{ $blog->created_at }}</span>
                </div>
                <div>
                  <p style="text-align:justify">
                    <span style="font-size:14px" class="text-blog-description">{{ $blog->description}}
                    </span>
                  </p>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<style>
    .text-blog-description {
        display: block;
        display: -webkit-box;
        height: 16px*1.3*3;
        font-size: 16px;
        line-height: 1.3;
        -webkit-line-clamp: 1;  /* số dòng hiển thị */
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
        margin-top:10px;
    }
</style>
