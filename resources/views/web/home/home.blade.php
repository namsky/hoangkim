@extends('web.layouts.master')

@section('page_title', 'Hoàng Kim - Hạnh Phúc Muôn Nơi')
@section('page_thumb', asset('customer/logo.png'))

@section('content')
<h1 class="d-none">Hoàng Kim - Hạnh Phúc Muôn Nơi</h1>
<section class="awe-section-1">
  <div class="section_slider swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <a href="#" class="clearfix" title="Slider 1">
          <picture>
            <source media="(min-width: 1200px)" srcset="{{asset('web/images/banner/banner2.jpg')}}">
            <source media="(min-width: 992px)" srcset="{{asset('web/images/banner/banner2.jpg')}}">
            <source media="(min-width: 569px)" srcset="{{asset('web/images/banner/banner2.jpg')}}">
            <source media="(max-width: 480px)" srcset="{{asset('web/images/banner/banner2.jpg')}}">
            <img width="1920" height="700" src="{{asset('web/images/banner/banner2.jpg')}}" alt="Slider 1" class="img-responsive center-block" />
          </picture>
        </a>
      </div>
    </div>
  </div>
  <script>
    window.addEventListener('DOMContentLoaded', (event) => {
      var swiperSlider = new Swiper('.section_slider', {
        autoplay: {
          delay: 4000,
        },
        loop: true,
        navigation: {
          nextEl: '.next_slider',
          prevEl: '.prev_slider',
        },
      });
    });
  </script>
</section>
<section class="awe-section-2">
  <section class="section_about clearfix lazyload" data-src="{{asset('web/images/about-left.png')}}">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-12">
          <div class="image_left_about">
            <img class="imageload lazyload" src="{{asset('web/images/about-left.png')}}" data-src="{{asset('web/images/about-left.png')}}" alt="">
          </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-12">
          <div class="heading clearfix">
            <h2>
              <span>Tại sao chọn chúng tôi?</span>
            </h2>
            <p class="text_small">Chúng tôi đã dành hơn 20 năm để nghiên cứu, và thử nghiệm, làm việc với nhiều đơn vị sản xuất, tổng hợp kết quả thử nghiệm lâm sàng, khảo sát ý kiến người sử dụng để tìm ra sản phẩm phục vụ người tiêu dùng toàn cầu.</p>
          </div>
          <div class="swiper_about swiper-container">
            <div class="swiper-wrapper">
              <div class="col-item-srv swiper-slide">
                <div class="service_item_ed">
                  <span class="iconx">
                    <img alt="Sản phẩm chất lượng " class="" src="{{asset('ionpia/assets/icon_about_1.webp')}}" data-src="{{asset('ionpia/assets/icon_about_1.png?16779060595')}}" />
                  </span>
                  <div class="content_srv">
                    <span class="title_service">Sản phẩm chất lượng </span>
                    <span class="content_service">Sản phẩm cao cấp, chất lượng tốt cho sức khỏe trong thời đại mới </span>
                  </div>
                </div>
              </div>
              <div class="col-item-srv swiper-slide">
                <div class="service_item_ed">
                  <span class="iconx">
                    <img alt="Hỗ trợ 24/7" class="lazyload" src="{{asset('web/images/icon_about_2.webp')}}" data-src="{{asset('ionpia/assets/icon_about_2.png?16779060595')}}" />
                  </span>
                  <div class="content_srv">
                    <span class="title_service">Hỗ trợ 24/7</span>
                    <span class="content_service">Chỉ cần nhấc máy lên và gọi tới hotline: 0982 768 265</span>
                  </div>
                </div>
              </div>
              <div class="col-item-srv swiper-slide">
                <div class="service_item_ed">
                  <span class="iconx">
                    <img alt="Bảo hành chu đáo" class="lazyload" src="{{asset('web/images/icon_about_3.webp')}}" data-src="{{asset('web/images/icon_about_3.webp')}}" />
                  </span>
                  <div class="content_srv">
                    <span class="title_service">Bảo hành chu đáo</span>
                    <span class="content_service">Tất cả các trường hợp phát sinh lỗi do chúng tôi, Quý khách sẽ được hỗ trợ miễn phí</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-pagination"></div>
          </div>
        </div>
      </div>
    </div>
  </section>

</section>
<section class="awe-section-3">
  <section class="section_deal_hot clearfix">
    <div class="container">
      <div class="title_modules a-center">
        <h2>
          <a href="san-pham-noi-bat.html" title="Deal hot  trong tuần">Deal hot <span> trong tuần</span>
          </a>
        </h2>
        <p class="span">{{ $all_products->count() }} sản phẩm được khuyến mãi giá sốc</p>
      </div>
    </div>
    <div class="deal_today">
      <div class="container">
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-12">
            <div class="content_deal">
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-12">
                  <div class="item_product_deal clearfix">
                      @if($product1)
                    <form action="" method="post" class="variants product-action">
                      <div class="product-thumbnail">
                        <a class="product_overlay" href="ionpia-5100.html" title="IONPIA 5100-7"></a>
                        <a class="image_thumb" href="{{ route('w.pro.index', ['category'=>$product1->category->slug, 'product'=>$product1->slug]) }}" title="IONPIA 5100-7">
                          <img class="lazyload" src="{{ $product1->image }}" alt="$product1->name">
                        </a>
                      </div>
                      <div class="product-info">
                        <h3 class="product-name">
                          <a href="{{ route('w.pro.index', ['category'=>$product1->category->slug, 'product'=>$product1->slug]) }}" title="IONPIA 5100-7">{{ $product1->name }}</a>
                        </h3>
                        <div class="price-box">
                          <span class="price">{{ number_format($product1->price , 0, ',', '.') }} ₫</span>
                        </div>
                        <div class="product-action">
                          <div class="group_action">
                            <input type="hidden" name="variantId" value="47470373" />
                            <a href="{{route('w.cart.create', $product1->id)}}" class="btn-buy btn-cart btn-left btn btn-views left-to add_to_cart active " title="Mua ngay">
                              <span>Thêm giỏ hàng</span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </form>
                      @endif
                  </div>
                </div>
                @foreach ($two_products as $product)
                <div class="col-xl-3 col-lg-3 col-md-6 col-6">
                  <div class="item_product_main">
                    <form class="variants product-action" >
                      <div class="product-thumbnail">
                        <a class="image_thumb scale_hover" href="{{ route('w.pro.index', ['category'=>$product->category->slug, 'product'=>$product->slug]) }}" title="{{ $product->name }}">
                          <img class="lazyload" src="{{ $product->image }}"  alt="{{ $product->name }}">
                        </a>
                        <div class="action1">
                          <a href="{{route('w.cart.create', $product->id)}}" class="hidden-xs btn-buy btn-cart btn btn-views left-to add_to_cart active " title="Thêm vào giỏ hàng">
                            <span>Thêm giỏ hàng</span>
                          </a>
                        </div>
                      </div>
                      <div class="product-info">
                        <h3 class="product-name">
                          <a href="{{ route('w.pro.index', ['category'=>$product->category->slug, 'product'=>$product->slug]) }}" title="{{ $product->name }}">{{ $product->name }}</a>
                        </h3>
                        <div class="price-box"> {{ number_format($product->price , 0, ',', '.') }} ₫</div>
                      </div>
                    </form>
                  </div>
                </div>

                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<section class="awe-section-5">
  <section class="section_banner clearfix">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-12">
          <div class="banner-img">
            <a href="" title="Banner 1">
              <img class="imageload lazyload" src="{{asset('web/images/banner/banner.jpg')}}" data-src="{{asset('web/images/banner/banner.jpg')}}" alt="Banner 1">
            </a>
          </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-12">
          <div class="banner-img">
            <a href="" title="Banner 2">
              <img class="imageload lazyload" src="{{asset('web/images/banner/banner2.jpg')}}" data-src="{{asset('web/images/banner/banner2.jpg')}}" alt="Banner 2">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<section class="awe-section-6">
  <section class="section_product_feature clearfix">
    <div class="container">
      <div class="title_modules a-center">
        <h2>
          <a href="san-pham-noi-bat.html" title="Sản phẩm nổi bật">Sản phẩm nổi bật</a>
        </h2>
        <p class="text_small">{{ $all_products->count() }} sản phẩm mới</p>
      </div>
    </div>
    <div class="content_feature lazyload" data-src="{{asset('ionpia/assets/bg-feature.jpg')}}">
      <div class="container">
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-12">
            <div class="row">
              @foreach ($all_products as $product)
              <div class="col-lg-3 col-6 col-sm-6 col-xs-6">
                <div class="item">
                  <div class="item_product_main">
                    <form>
                      <div class="product-thumbnail">
                        <a class="image_thumb scale_hover" href="{{ route('w.pro.index', ['category'=>$product->category->slug, 'product'=>$product->slug]) }}" title="{{ $product->name }}">
                          <img class="lazyload" src="{{ $product->image }}" >
                        </a>
                        <div class="action1">
                          <input type="hidden" name="variantId" value="47470373" />
                          <a href="{{route('w.cart.create', $product->id)}}" class="hidden-xs btn-buy btn-cart btn btn-views left-to add_to_cart active " title="Thêm vào giỏ hàng">
                            <span>Thêm giỏ hàng</span>
                          </a>
                        </div>
                      </div>
                      <div class="product-info">
                        <h3 class="product-name">
                          <a href="{{ route('w.pro.index', ['category'=>$product->category->slug, 'product'=>$product->slug]) }}" title="{{ $product->name }}">{{ $product->name }}</a>
                        </h3>
                        <div class="price-box"> {{ number_format($product->price , 0, ',', '.') }} ₫</div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<section class="awe-section-8">
  <div class="section_blog clearfix">
    <div class="container">
      <div class="title_modules a-center">
        <h2>
          <a href="{{ route('w.blogs') }}" title="KIẾN THỨC SỨC KHỎE">KIẾN THỨC SỨC KHỎE</a>
        </h2>
        <p></p>
      </div>
      <div class="row">
        @foreach ($blogs as $blog)
        <div class="col-lg-4 col-md-6 col-12 col-blog">
          <div class="item">
            <div class="blogwp clearfix">
              <div class="blog_thumbnail">
                <a class="image-blog" href="{{route('w.single_blog',['category_slug' => $blog->category->slug,'blog_slug' => $blog->slug])}}" title="">
                  <img width="480" height="356" src="{{ $blog->image }}" data-src="{{ $blog->image }}" alt="{{ $blog->name }}" class="imageload lazyload img-responsive" style="aspect-ratio: 4/3;
                  object-fit: cover;"/>
                </a>
              </div>
              <div class="content_blog clearfix">
                <h3>
                  <a href="{{route('w.single_blog',['category_slug' => $blog->category->slug,'blog_slug' => $blog->slug])}}" title="{{ $blog->name }}">{{ $blog->name }}</a>
                </h3>
                <div class="blog-bottom"></div>
                <a class="btn_see" href="hanh-trinh-cham-soc-suc-khoe-chu-dong-07-08-2022-tai-to-hop-y-te-mediplus-99-tan-mai.html" title="Xem thêm">Xem thêm <svg width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0)">
                      <path d="M8.92311 4.29963L6.22325 1.29979C6.16566 1.23619 6.08526 1.2002 6.00006 1.2002H4.20016C4.08197 1.2002 3.97457 1.26979 3.92598 1.37779C3.87798 1.48638 3.89778 1.61297 3.97697 1.70057L6.49624 4.50002L3.97697 7.29887C3.89778 7.38706 3.87738 7.51365 3.92598 7.62165C3.97457 7.73024 4.08197 7.79984 4.20016 7.79984H6.00006C6.08526 7.79984 6.16566 7.76324 6.22325 7.70084L8.92311 4.70101C9.0257 4.58701 9.0257 4.41302 8.92311 4.29963Z" fill="#2A3E55" />
                      <path d="M5.02327 4.29963L2.32341 1.29979C2.26581 1.23619 2.18542 1.2002 2.10022 1.2002H0.300321C0.182127 1.2002 0.0747333 1.26979 0.0261359 1.37779C-0.0218614 1.48638 -0.00206252 1.61297 0.0771332 1.70057L2.5964 4.50002L0.0771332 7.29887C-0.00206252 7.38706 -0.0224614 7.51365 0.0261359 7.62165C0.0747333 7.73024 0.182127 7.79984 0.300321 7.79984H2.10022C2.18542 7.79984 2.26581 7.76324 2.32341 7.70084L5.02327 4.70101C5.12586 4.58701 5.12586 4.41302 5.02327 4.29963Z" fill="#2A3E55" />
                    </g>
                    <defs>
                      <rect width="9" height="9" fill="white" />
                    </defs>
                  </svg>
                </a>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
{{--<section class="awe-section-9">--}}
{{--  <div class="section_question">--}}
{{--    <div class="container">--}}
{{--      <div class="title_modules a-center">--}}
{{--        <h2>--}}
{{--          <span>Câu hỏi thường gặp</span>--}}
{{--        </h2>--}}
{{--        <p> Thắc mắc của khách hàng chúng tôi sẽ trả lời nhanh chóng </p>--}}
{{--      </div>--}}
{{--      <div class="row">--}}
{{--        <div class="col-lg-6 col-12 z-index">--}}
{{--          <div class="block-content">--}}
{{--            <div class="question visited">--}}
{{--              <h4> Tại sao nước lại quan trọng? <i class=""></i>--}}
{{--              </h4>--}}
{{--              <p> Một phần lớn cơ thể của một người được tạo ra từ nước. 70% người lớn, trẻ nhỏ 90% là độ ẩm, và độ ẩm cần thiết cho quá trình trao đổi chất. Khiến bạn uống nước. Đối với người lớn, ví dụ, 1500ml mỗi giờ chạy bộ, làm việc văn phòng 480 ml trong 8 giờ, 300 ml trong 1 giờ mua sắm, 150 ml trong 30 phút làm sạch, v.v. Độ ẩm đến da được bài tiết ra khỏi cơ thể do mồ hôi qua hơi thở. Vì vậy, như một bữa ăn. Không bao gồm 1500ml và 100ml để tái chế, bạn cần uống 1500ml nước mỗi ngày. </p>--}}
{{--            </div>--}}
{{--            <div class="question">--}}
{{--              <h4> Tôi nên uống loại nước nào? <i class=""></i>--}}
{{--              </h4>--}}
{{--              <p> Không chỉ có nước (H2O) đổ mồ hôi. Các chất điện giải thực hiện các chức năng kiểm soát cơ thể khác nhau như canxi, kali, magiê, natri, v.v. Đi ra ngoài cùng nhau. Thiếu các chất điện giải này khiến chất lỏng trong cơ thể có tính axit, gây chuột rút ở các cơ. Nó thức dậy, nhịp tim trở nên không đều và khả năng tập thể dục bị rối loạn. Rất giàu khoáng chất Bạn phải uống nước. <br>--}}
{{--                <b>Hỏi một ion là gì?</b>--}}
{{--                <br> Để hiểu “ion”, trước tiên chúng ta cần xem xét các nguyên tử tạo nên vật chất. Nguyên tử được tạo thành từ các electron và hạt nhân mang điện khác nhau, và giữa electron và hạt nhân có lực hút nhau. Lấy đi một nguyên tử của điện tử ban đầu của nó đó là "ion" được tạo ra khi nó bị lấy đi hoặc các điện tử khác.--}}
{{--              </p>--}}
{{--            </div>--}}
{{--            <div class="question">--}}
{{--              <h4> Thức uống ion là gì? <i class=""></i>--}}
{{--              </h4>--}}
{{--              <p> Khi cho muối vào nước, các cation natri và anion clo sẽ bị ion hóa thành nước. Tập thể dục làm cho bạn cảm thấy khát vì các ion điện giải này thoát ra ngoài theo mồ hôi. Nó có thể. Nguyên tắc này được sử dụng để pha chế đồ uống ion. Đó là, nước trái cây trong nước muối. Nếu bạn thấy nó bị cháy là chính xác. Tuy nhiên, ngày nay đồ uống ion có hàm lượng và hàm lượng chất điện giải. Nó gần giống như dịch cơ thể. Do đó, tốc độ hấp thụ từ ruột nhanh hơn nước gần ba lần, vì vậy hãy tập. Uống nước xong không bị đau bụng do chậm hấp thu nên trong ba môn phối hợp và marathon. Rất hữu ích. Ngoài ra, vì các chất lỏng trong cơ thể trở nên có tính axit khi bạn mệt mỏi, Nó cũng bổ sung khả năng duy trì độ axit bình thường của chất lỏng (Ph 7,4). Tuy nhiên, thức uống ion 100ml chứa 20 ~ 30 kcal (khoảng 40 kcal đối với đồ uống có ga), vì vậy bạn có thể kiểm soát cân nặng của mình. </p>--}}
{{--            </div>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--        <div class="col-lg-6 col-12 d-none d-md-block">--}}
{{--          <div class="block_img lazyload" src="{{asset('images/bg-question-right.png')}}">--}}
{{--          <img src="{{asset('images/bg-question-right.png')}}" alt=""></div>--}}
{{--        </div>--}}
{{--      </div>--}}
{{--    </div>--}}
{{--  </div>--}}
{{--</section>--}}


@endsection
@push('css')
@endpush
@push('scripts')
        <script>
            var swiperService = new Swiper('.swiper_about', {
                slidesPerView: 1,
                spaceBetween: 0,
                touchRatio: 0,
                slidesPerColumnFill: 'row',
                slidesPerColumn: 3,
                pagination: {
                    el: '.section_about .swiper-pagination',
                    clickable: true,
                },
                breakpoints: {
                    300: {
                        slidesPerView: 1,
                        slidesPerColumn: 1,
                        touchRatio: 1,
                        slidesPerColumnFill: 'column',
                        spaceBetween: 0
                    },
                    640: {
                        slidesPerView: 1,
                        slidesPerColumn: 1,
                        touchRatio: 1,
                        slidesPerColumnFill: 'column',
                        spaceBetween: 0
                    },
                    768: {
                        slidesPerView: 1,
                        slidesPerColumn: 1,
                        touchRatio: 1,
                        slidesPerColumnFill: 'column',
                        spaceBetween: 0
                    },
                    1024: {
                        slidesPerView: 1,
                        spaceBetween: 0
                    },
                    1200: {
                        slidesPerView: 1,
                        spaceBetween: 0
                    }
                }
            });
            window.addEventListener('DOMContentLoaded', (event) => {
                var swiperFeature = new Swiper('.swiper_feature', {
                    slidesPerView: 4,
                    spaceBetween: 30,
                    pagination: {
                        el: '.section_product_feature .swiper-pagination',
                        clickable: true,
                    },
                    breakpoints: {
                        300: {
                            slidesPerView: 2,
                            spaceBetween: 30
                        },
                        640: {
                            slidesPerView: 2,
                            spaceBetween: 30
                        },
                        768: {
                            slidesPerView: 3,
                            spaceBetween: 30
                        },
                        1024: {
                            slidesPerView: 4,
                            spaceBetween: 30
                        },
                        1200: {
                            slidesPerView: 4,
                            spaceBetween: 30
                        }
                    }
                });
            });
            var swiper = new Swiper('.swiper_blog', {
                slidesPerView: 3,
                spaceBetween: 47,
                slidesPerGroup: 1,
                autoHeight: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                breakpoints: {
                    300: {
                        slidesPerView: 1,
                        spaceBetween: 15
                    },
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 15
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 30
                    },
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    },
                    1200: {
                        slidesPerView: 3,
                        spaceBetween: 43
                    }
                }
            });
        </script>
@endpush





