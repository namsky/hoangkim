@extends('web.layouts.master')

@section('content')
  <div class="page_banner">
    <div class="container">
      <ul class="page_list_head">
        <li>
          <a href="">Trang chủ</a>
        </li>
        <li>Tuyển dụng</li>
      </ul>
    </div>
  </div>
  <section class="danhmuc_product ">
    <div class="container container_feature">
      <div class="row">
        <div class="col-md-9">
          <div class="cate_box_new">
            <div class="banner_cate">
              <div class="title_blog_new">
                <h1 style="display: none;">
                  <span>Tuyển dụng</span>
                </h1>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 col_image">
              <div class=" box_new_list" data-aos="fade-up" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
                <div class="picture_tuvan">
                  <a href="https://thuanphongpharma.com/nhung-thoi-quen-tot-bao-ve-da-mat-khoe-manh-trong-mua-he.html">
                    <img src="https://thuanphongpharma.com/upload/img/news/a.jpeg" class="w_100" alt="Những thói quen tốt bảo vệ da mặt khỏe mạnh trong mùa hè">
                  </a>
                </div>
                <div class="content_tuvan">
                  <h3>
                    <a href="https://thuanphongpharma.com/nhung-thoi-quen-tot-bao-ve-da-mat-khoe-manh-trong-mua-he.html">Những thói quen tốt bảo vệ da mặt khỏe mạnh trong mùa hè</a>
                  </h3>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 col_image">
              <div class=" box_new_list" data-aos="fade-up" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
                <div class="picture_tuvan">
                  <a href="https://thuanphongpharma.com/tac-dung-tuyet-voi-cua-nuoc-ep-can-tay-va-nhung-dieu-can-luu-y.html">
                    <img src="https://thuanphongpharma.com/upload/img/news/tai-xuong1.jpg" class="w_100" alt="Tác dụng tuyệt vời của nước ép cần tây và những điều cần lưu ý">
                  </a>
                </div>
                <div class="content_tuvan">
                  <h3>
                    <a href="https://thuanphongpharma.com/tac-dung-tuyet-voi-cua-nuoc-ep-can-tay-va-nhung-dieu-can-luu-y.html">Tác dụng tuyệt vời của nước ép cần tây và những điều cần lưu ý</a>
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="box_sidebar_new">
            <div class="write_news_sidebar">
              <h3>
                <span>Bài viết nổi bật</span>
              </h3>
              <ul class="post_news">
                <li>
                  <span>
                    <a href="https://thuanphongpharma.com/bi-quyet-song-khoe-cho-benh-nhan-dai-thao-duong.html">Bí quyết sống khỏe cho bệnh nhân đái tháo đường</a>
                  </span>
                </li>
                <li>
                  <span>
                    <a href="https://thuanphongpharma.com/benh-dau-mua-khi-va-nhung-dieu-ban-nen-biet-de-phong-tranh.html">Bệnh đậu mùa khỉ và những điều bạn nên biết để phòng tránh </a>
                  </span>
                </li>
                <li>
                  <span>
                    <a href="https://thuanphongpharma.com/da-tre-so-sinh-noi-man-do-va-cach-cham-soc.html">Da trẻ sơ sinh nổi mẩn đỏ và cách chăm sóc </a>
                  </span>
                </li>
                <li>
                  <span>
                    <a href="https://thuanphongpharma.com/nguyen-nhan-trieu-chung-va-cach-nhan-biet-benh-tang-huyet-ap.html">Nguyên nhân, triệu chứng và cách nhận biết bệnh tăng huyết áp</a>
                  </span>
                </li>
                <li>
                  <span>
                    <a href="https://thuanphongpharma.com/huyet-ap-cao-la-bao-nhieu-nhung-dieu-can-luu-y-doi-voi-benh-nhan-tang-huyet-ap.html">Huyết áp cao là bao nhiêu? Những điều cần lưu ý đối với bệnh nhân tăng huyết áp</a>
                  </span>
                </li>
                <li>
                  <span>
                    <a href="https://thuanphongpharma.com/bat-mi-nhung-bai-thuoc-dan-gian-tri-ho-cho-be-don-gian-va-hieu-qua.html">Bật mí những bài thuốc dân gian trị ho cho bé đơn giản và hiệu quả</a>
                  </span>
                </li>
                <li>
                  <span>
                    <a href="https://thuanphongpharma.com/hong-tre-so-sinh-co-dom-ba-me-phai-lam-nhu-the-nao.html">Họng trẻ sơ sinh có đờm ba mẹ phải làm như thế nào?</a>
                  </span>
                </li>
                <li>
                  <span>
                    <a href="https://thuanphongpharma.com/tre-so-sinh-xi-hoi-co-mui-thi-co-lam-sao-khong.html"> Trẻ sơ sinh xì hơi có mùi thì có làm sao không?</a>
                  </span>
                </li>
                <li>
                  <span>
                    <a href="https://thuanphongpharma.com/top-5-sua-rua-mat-cho-da-dau-pho-bien-trong-mua-he.html">Top 5 sữa rửa mặt cho da dầu phổ biến trong mùa hè</a>
                  </span>
                </li>
                <li>
                  <span>
                    <a href="https://thuanphongpharma.com/goi-y-mot-so-kem-tri-quang-tham-mat-duoc-nhieu-nguoi-tin-dung.html">Gợi ý một số kem trị quầng thâm mắt được nhiều người tin dùng</a>
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </section>
@endsection
