@extends('web.layouts.master')
@section('page_title', $product->name)
@section('page_thumb', $product->image)
@section('content')
<div class="breadcrumb_background">
  <div class="title_full">
    <div class="container a-center">
      <span class="title_page">{{ $product->name }}</span>
    </div>
  </div>
  <section class="bread-crumb">
    <span class="crumb-border"></span>
    <div class="container">
      <div class="row">
        <div class="col-12 a-left">
          <ul class="breadcrumb">
            <li class="home">
              <a href="{{ route('w.index') }}">
                <span>Trang chủ</span>
              </a>
              <span class="mr_lr">&nbsp; <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-angle-right fa-w-8">
                  <path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" class=""></path>
                </svg>&nbsp; </span>
            </li>
            <li>
              <a class="changeurl" href="#">
                <span>{{ $product->category->category_name }}</span>
              </a>
              <span class="mr_lr">&nbsp; <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-angle-right fa-w-8">
                  <path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" class=""></path>
                </svg>&nbsp; </span>
            </li>
            <li>
              <strong>
                <span>{{ $product->name }}</span>
              </strong>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
</div>
<section class="product details-main" itemscope="" itemtype="https://schema.org/Product">

  <div id="dvProductPricing" class="ProductDetailsPricing d-none" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">

    <div class="inventory_quantity d-none" itemscope="" itemtype="http://schema.org/ItemAvailability">
      <span class="a-stock" itemprop="supersededBy"> Còn hàng </span>
    </div>

  </div>
  <!-- review -->
  <div class="d-none" itemprop="review" itemscope="" itemtype="http://schema.org/Review">
  </div>
  <form id="add-to-cart-form" action="/cart/add" method="post" class="form_background form-inline margin-bottom-0">
    <div class="container">
      <div class="section wrap-padding-15 wp_product_main">
        <div class="details-product section">
          <div class="row ">
            <div class="product-detail-left product-images col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
              <div class="col_large_default large-image">
                <a href="" data-rel="prettyPhoto[product-gallery]">
                  <div style="height:355px;width:453px;" class="zoomWrapper">
                    <div style="height:355px;width:453px;" class="zoomWrapper">
                      <img class="checkurl img-responsive" id="img_01" src="{{ $product->image }}" alt="{{ $product->name }}" style="position: absolute;">
                    </div>
                  </div>
                </a>
              </div>
              {{-- <div class="section slickthumb_relative_product_1">
                <div id="gallery_02" class="swiper-container slickproduct thumb_product_details swiper-container-initialized swiper-container-horizontal">
                  <div class="swiper-wrapper" id="swiper-wrapper-a99f0a3910c055db5" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                    <div class="item swiper-slide swiper-slide-active" role="group" aria-label="1 / 5" style="width: 104px; margin-right: 13px;">
                      <a href="javascript:void(0)" data-zoom-image="//bizweb.dktcdn.net/thumb/1024x1024/100/428/530/products/z4419631365289-7501f1d69ca262fd1026842b136be422.jpg?v=1688187948863">
                        <img class="lazyload loaded" src="//bizweb.dktcdn.net/thumb/compact/100/428/530/products/z4419631365289-7501f1d69ca262fd1026842b136be422.jpg?v=1688187948863" data-src="//bizweb.dktcdn.net/thumb/compact/100/428/530/products/z4419631365289-7501f1d69ca262fd1026842b136be422.jpg?v=1688187948863" alt="{{ $product->name }}" data-image="//bizweb.dktcdn.net/thumb/1024x1024/100/428/530/products/z4419631365289-7501f1d69ca262fd1026842b136be422.jpg?v=1688187948863" data-was-processed="true">
                      </a>
                    </div>
                    <div class="item swiper-slide swiper-slide-next" role="group" aria-label="2 / 5" style="width: 104px; margin-right: 13px;">
                      <a href="javascript:void(0)" data-zoom-image="//bizweb.dktcdn.net/thumb/1024x1024/100/428/530/products/z4419633685092-36df92ba497f57963571a4c776bd7094.jpg?v=1688187948863">
                        <img class="lazyload loaded" src="//bizweb.dktcdn.net/thumb/compact/100/428/530/products/z4419633685092-36df92ba497f57963571a4c776bd7094.jpg?v=1688187948863" data-src="//bizweb.dktcdn.net/thumb/compact/100/428/530/products/z4419633685092-36df92ba497f57963571a4c776bd7094.jpg?v=1688187948863" alt="{{ $product->name }}" data-image="//bizweb.dktcdn.net/thumb/1024x1024/100/428/530/products/z4419633685092-36df92ba497f57963571a4c776bd7094.jpg?v=1688187948863" data-was-processed="true">
                      </a>
                    </div>
                    <div class="item swiper-slide" role="group" aria-label="3 / 5" style="width: 104px; margin-right: 13px;">
                      <a href="javascript:void(0)" data-zoom-image="//bizweb.dktcdn.net/thumb/1024x1024/100/428/530/products/z4419630616033-020001a135fe1119f75b4365fa71703b.jpg?v=1688187948863">
                        <img class="lazyload loaded" src="//bizweb.dktcdn.net/thumb/compact/100/428/530/products/z4419630616033-020001a135fe1119f75b4365fa71703b.jpg?v=1688187948863" data-src="//bizweb.dktcdn.net/thumb/compact/100/428/530/products/z4419630616033-020001a135fe1119f75b4365fa71703b.jpg?v=1688187948863" alt="{{ $product->name }}" data-image="//bizweb.dktcdn.net/thumb/1024x1024/100/428/530/products/z4419630616033-020001a135fe1119f75b4365fa71703b.jpg?v=1688187948863" data-was-processed="true">
                      </a>
                    </div>
                    <div class="item swiper-slide" role="group" aria-label="4 / 5" style="width: 104px; margin-right: 13px;">
                      <a href="javascript:void(0)" data-zoom-image="//bizweb.dktcdn.net/thumb/1024x1024/100/428/530/products/z4445303841118-9007d2e77c058c90c73ad968cf185e77.jpg?v=1687167175280">
                        <img class="lazyload loaded" src="//bizweb.dktcdn.net/thumb/compact/100/428/530/products/z4445303841118-9007d2e77c058c90c73ad968cf185e77.jpg?v=1687167175280" data-src="//bizweb.dktcdn.net/thumb/compact/100/428/530/products/z4445303841118-9007d2e77c058c90c73ad968cf185e77.jpg?v=1687167175280" alt="{{ $product->name }}" data-image="//bizweb.dktcdn.net/thumb/1024x1024/100/428/530/products/z4445303841118-9007d2e77c058c90c73ad968cf185e77.jpg?v=1687167175280" data-was-processed="true">
                      </a>
                    </div>
                    <div class="item swiper-slide" role="group" aria-label="5 / 5" style="width: 104px; margin-right: 13px;">
                      <a href="javascript:void(0)" data-zoom-image="//bizweb.dktcdn.net/thumb/1024x1024/100/428/530/products/z4445304197144-adbd884435c1a2292e039d0f21775478.jpg?v=1687167176613">
                        <img class="lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-src="//bizweb.dktcdn.net/thumb/compact/100/428/530/products/z4445304197144-adbd884435c1a2292e039d0f21775478.jpg?v=1687167176613" alt="{{ $product->name }}" data-image="//bizweb.dktcdn.net/thumb/1024x1024/100/428/530/products/z4445304197144-adbd884435c1a2292e039d0f21775478.jpg?v=1687167176613">
                      </a>
                    </div>
                  </div>
                  <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
              </div> --}}
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 details-pro">
              <h1 class="title-product">{{ $product->name }}</h1>
              <div class="price-box clearfix">
                <div class="special-price">
                  <span class="price product-price">{{ number_format($product->price , 0, ',', '.') }} ₫</span>
                  <meta itemprop="price" content="{{ number_format($product->price , 0, ',', '.') }} ₫">
                  <meta itemprop="priceCurrency" content="VND">
                </div>
                <!-- Giá -->
              </div>
              <div class="product-summary">
                <div class="rte">
                  {!! $product->description !!}
                </div>
              </div>
              <div class="product-summary">
                <div class="rte">
                 <p>Xuất sứ : {{$product->origin}}</p>
                    @if($product->expiry)
                    <p>Hạn sử dụng : {{ \Carbon\Carbon::createFromDate($product->expiry)->format('d/m/Y') }}</p>
                    @endif
                    @if($product->qrcode)
                    <p>QR  mã vạch của sản phẩm</p>
                        <img style="display: block; max-width: 8rem" src="{{$product->qrcode}}" alt="">
                    @endif
                </div>
              </div>
              <div class="form-product col-sm-12 clearfix">
                <div class="form-group form_button_details margin-top-0">
                  <div class="form_product_content type1 ">
                    <div class="soluong soluong_type_1 show">
                      <label class="sl section">Số lượng</label>
                      <div class="custom input_number_product custom-btn-number form-controls">
                        <button class="btn_num num_1 button button_qty" onclick="var result = document.getElementById('qtym'); var qtypro = result.value; if( !isNaN( qtypro ) &amp;&amp; qtypro > 1 ) result.value--;return false;" type="button">
                          <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-down" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-angle-down fa-w-10">
                            <path fill="currentColor" d="M143 352.3L7 216.3c-9.4-9.4-9.4-24.6 0-33.9l22.6-22.6c9.4-9.4 24.6-9.4 33.9 0l96.4 96.4 96.4-96.4c9.4-9.4 24.6-9.4 33.9 0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9l-136 136c-9.2 9.4-24.4 9.4-33.8 0z" class=""></path>
                          </svg>
                        </button>
                        <input type="text" id="qtym" name="quantity" value="1" maxlength="3" class="form-control prd_quantity" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;" onchange="if(this.value == 0)this.value=1;">
                        <button class="btn_num num_2 button button_qty" onclick="var result = document.getElementById('qtym'); var qtypro = result.value; if( !isNaN( qtypro )) result.value++;return false;" type="button">
                          <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-up" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" class="svg-inline--fa fa-angle-up fa-w-10">
                            <path fill="currentColor" d="M177 159.7l136 136c9.4 9.4 9.4 24.6 0 33.9l-22.6 22.6c-9.4 9.4-24.6 9.4-33.9 0L160 255.9l-96.4 96.4c-9.4 9.4-24.6 9.4-33.9 0L7 329.7c-9.4-9.4-9.4-24.6 0-33.9l136-136c9.4-9.5 24.6-9.5 34-.1z" class=""></path>
                          </svg>
                        </button>
                      </div>
                    </div>
                    <div class="btn-mua button_actions clearfix">
                      <a href="{{route('w.cart.create', $product->id)}}"  class="btn btn_base normal_button btn_add_cart add_to_cart btn-cart">
                        <span class="txt-main text_1">Thêm giỏ hàng</span>
                      </a>
                      <a href="{{route('w.cart.create', $product->id)}}" class="btn fast btn_base btn_add_cart btn-cart btn_buynow">
                        <span class="txt-main text_1">Mua ngay</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="contact_p clearfix">
                <p class="text_hotline">Gọi để đặt trước</p>
                <div class="phone clearfix">
                  <div class="icon_phone">
                    <svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M14.963 0C18.5535 0.500854 21.8062 2.12415 24.377 4.69495C26.815 7.13892 28.4141 10.2167 28.9995 13.608L26.7727 13.9883C26.2719 11.0615 24.8899 8.40015 22.7838 6.29407C20.5631 4.07947 17.7509 2.67334 14.6491 2.23889L14.963 0ZM22.0474 19.1841C21.1 18.2487 19.9172 18.2487 18.9758 19.1841C18.2576 19.8961 17.5396 20.6083 16.8335 21.3324C16.6404 21.5315 16.4774 21.5737 16.2421 21.441C16.058 21.3407 15.8692 21.2478 15.6805 21.155C15.3929 21.0137 15.1057 20.8724 14.836 20.7048C12.7541 19.3953 11.0101 17.7117 9.46522 15.8168C8.6988 14.8754 8.01692 13.8676 7.54017 12.733C7.44361 12.5038 7.46168 12.3529 7.64875 12.1658C7.89186 11.9309 8.13282 11.694 8.37269 11.4557C8.7414 11.0892 9.10749 10.7196 9.47419 10.3492L9.77296 10.0476C10.7566 9.05798 10.7566 7.89929 9.76692 6.90356C9.57386 6.70837 9.38081 6.51465 9.18775 6.32141C8.81959 5.95288 8.45143 5.58594 8.08326 5.21387C7.50398 4.63464 6.93067 4.04919 6.34529 3.47595C5.39784 2.55261 4.2151 2.55261 3.27369 3.48193C2.98622 3.76465 2.70356 4.05017 2.42109 4.33545L2.4145 4.34204C1.98756 4.77332 1.56104 5.20422 1.11933 5.62427C0.437387 6.27002 0.0934543 7.06055 0.0210056 7.98376C-0.0936184 9.48645 0.274485 10.9045 0.793467 12.2865C1.85554 15.1469 3.47285 17.6875 5.43409 20.0168C8.08326 23.1669 11.2454 25.6592 14.9447 27.4575C16.6102 28.2662 18.3361 28.8877 20.2129 28.9904C21.5043 29.0627 22.6267 28.7369 23.5259 27.7291C23.9303 27.2771 24.3687 26.8511 24.8066 26.4255C25.0352 26.2034 25.2636 25.9813 25.4871 25.7557C26.4526 24.7782 26.4587 23.5953 25.4992 22.6299C24.3583 21.4829 23.2116 20.3422 22.0648 19.2013L22.0474 19.1841ZM20.8949 14.3745L23.1217 13.9943C22.7717 11.9486 21.8061 10.0959 20.3397 8.62354C18.7888 7.07263 16.8276 6.09497 14.6672 5.79321L14.3533 8.0321C16.025 8.26746 17.5457 9.02173 18.7466 10.2227C19.8811 11.3572 20.6234 12.7935 20.8949 14.3745Z" fill="#024E98"></path>
                    </svg>
                  </div>
                  <a href="tel:0973154701">0973 154 701</a>
                  <span class="time">(Thời gian: 8h00 - 20h00)</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
  <section class="section sec_tab ">
    <div class="container">
      <div class="row">
        <div class="tab_h col-xs-12 col-lg-12 col-sm-12 col-md-12">
          <div class="section bg_white">
            <!-- Nav tabs -->
            <div class="product-tab e-tabs not-dqtab">
              <ul class="tabs tabs-title clearfix">
                <li class="tab-link current" data-tab="tab-1">
                  <h3>
                    <span>Mô tả sản phẩm</span>
                  </h3>
                </li>
{{--                <li class="tab-link" data-tab="tab-3" id="tab-review">--}}
{{--                  <h3>--}}
{{--                    <span>Khách hàng đánh giá</span>--}}
{{--                  </h3>--}}
{{--                </li>--}}
              </ul>
              <div class="tab-float">
                <div id="tab-1" class="tab-content content_extab current">
                  <div class="rte product_getcontent">
                    <div id="content">
                      {!! $product->content !!}
                    </div>
                  </div>
                </div>
                <div id="tab-3" class="tab-content content_extab tab-review-c">
                  <div class="rte">
                    <div id="sapo-product-reviews" class="sapo-product-reviews" data-id="31460698">
                      <div>
                        <div id="sapo-product-reviews-noitem">
                          <div>
                            <div class="content">
                              <p>Hiện tại sản phẩm chưa có đánh giá nào, bạn hãy trở thành người đầu tiên đánh giá cho sản phẩm này</p>
                              <div class="product-reviews-summary-actions">
                                <button type="button" class="btn-new-review" onclick="BPR.newReview(this); return false;">Gửi đánh giá của bạn</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div id="popup-bpr-form_31460698" class="popup-bpr-form jquerymodal">
                          <div class="jquerymodal-content">
                            <div class="jquerymodal-body">
                              <a href="javascript:;" rel="jquerymodal:close" class="close-modal ">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24px" height="24px">
                                  <path d="M 4.7070312 3.2929688 L 3.2929688 4.7070312 L 10.585938 12 L 3.2929688 19.292969 L 4.7070312 20.707031 L 12 13.414062 L 19.292969 20.707031 L 20.707031 19.292969 L 13.414062 12 L 20.707031 4.7070312 L 19.292969 3.2929688 L 12 10.585938 L 4.7070312 3.2929688 z"></path>
                                </svg>
                              </a>
                              <div class="sapo-product-reviews-form">
                                <div>
                                  <form method="post" action="https://newproductreviews.sapoapps.vn/reviews/create" id="sapo-product-reviews-frm" name="sapo-product-reviews-frm" class="MultiFile-intercepted">
                                    <input type="hidden" name="productId" id="review_product_id" value="31460698">
                                    <div class="title-form">Đánh giá sản phẩm</div>
                                    <div class="review-product-name">{{ $product->name }}</div>
                                    <div class="bpr-form-rating">
                                      <div class="form-group">
                                        <label>Đánh giá của bạn về sản phẩm:</label>
                                        <div id="dvRating" class="sapo-product-reviews-star"></div>
                                      </div>
                                      <input type="hidden" name="rating" id="review_rate" value="5">
                                      <span class="bpr-form-message-error"></span>
                                    </div>
                                    <div class="bpr-form-contact">
                                      <div class="form-group form-group__multiple">
                                        <div class="bpr-form-contact-name require">
                                          <input type="text" maxlength="100" id="review_author" name="author" value="" placeholder="Nhập họ tên của bạn">
                                          <span class="bpr-form-message-error"></span>
                                        </div>
                                        <div class="bpr-form-contact-email option">
                                          <input type="text" maxlength="50" id="review_email" name="email" value="" placeholder="Nhập email của bạn">
                                          <span class="bpr-form-message-error"></span>
                                        </div>
                                        <div class="bpr-form-contact-phone option">
                                          <input type="tel" maxlength="15" id="review_phone" name="phone" value="" placeholder="Nhập số điện thoại của bạn">
                                          <span class="bpr-form-message-error"></span>
                                        </div>
                                      </div>
                                      <div class="form-group__textarea">
                                        <div class="form-group">
                                          <div class="bpr-form-review-body">
                                            <textarea maxlength="1000" id="review_body" name="body" rows="5" placeholder="Nhập nội dung đánh giá của bạn về sản phẩm này"></textarea>
                                          </div>
                                        </div>
                                        <div id="fileAttach" class="bpr-file-attach option">
                                          <label class="next-label" for="inputFileAttach">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
                                              <path d="M 4 5 C 2.895 5 2 5.895 2 7 L 2 23 C 2 24.105 2.895 25 4 25 L 14.230469 25 C 14.083469 24.356 14 23.688 14 23 C 14 22.662 14.021594 22.329 14.058594 22 L 5 22 L 5 15 L 7.2890625 12.710938 C 8.2340625 11.765937 9.7659375 11.765937 10.710938 12.710938 L 15.720703 17.720703 C 17.356703 15.469703 20.004 14 23 14 C 24.851 14 26.57 14.559578 28 15.517578 L 28 7 C 28 5.895 27.105 5 26 5 L 4 5 z M 23 8 C 24.105 8 25 8.895 25 10 C 25 11.105 24.105 12 23 12 C 21.895 12 21 11.105 21 10 C 21 8.895 21.895 8 23 8 z M 23 16 C 19.134 16 16 19.134 16 23 C 16 26.866 19.134 30 23 30 C 26.866 30 30 26.866 30 23 C 30 19.134 26.866 16 23 16 z M 23 19 C 23.552 19 24 19.447 24 20 L 24 22 L 26 22 C 26.552 22 27 22.447 27 23 C 27 23.553 26.552 24 26 24 L 24 24 L 24 26 C 24 26.553 23.552 27 23 27 C 22.448 27 22 26.553 22 26 L 22 24 L 20 24 C 19.448 24 19 23.553 19 23 C 19 22.447 19.448 22 20 22 L 22 22 L 22 20 C 22 19.447 22.448 19 23 19 z"></path>
                                            </svg>
                                            <span>Đính kèm hình ảnh (chọn tối đa 3 hình)</span>
                                          </label>
                                          <div class="MultiFile-wrap" id="inputFileAttach-wrap">
                                            <input style="color:#fff;opacity:0;cursor:pointer;" type="file" id="inputFileAttach" name="file" class="with-preview MultiFile-applied" value="">
                                            <div class="MultiFile-list" id="inputFileAttach_list"></div>
                                          </div>
                                        </div>
                                        <div class="bpr-form-actions">
                                          <button type="button" onclick="BPR.submitForm(); return false;" class="bpr-button-submit">
                                            <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                                            <span>Gửi đánh giá</span>
                                          </button>
                                        </div>
                                      </div>
                                      <span class="bpr-form-message-error bpr-form-message-error-body"></span>
                                      <span class="bpr-form-message-error bpr-form-message-error-file"></span>
                                    </div>
                                    <div class="bpr-form-review-error">
                                      <p class="error"></p>
                                    </div>
                                    <div class="bpr-form-actions">
                                      <button type="button" onclick="BPR.submitForm(); return false;" class="bpr-button-submit">
                                        <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                                        <span>Gửi đánh giá</span>
                                      </button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div id="bpr-review-thanks" class="bpr-success-popup jquerymodal">
                          <div class="jquerymodal-content">
                            <div class="jquerymodal-body">
                              <a href="javascript:;" rel="jquerymodal:close" class="close-modal">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24px" height="24px">
                                  <path d="M 4.7070312 3.2929688 L 3.2929688 4.7070312 L 10.585938 12 L 3.2929688 19.292969 L 4.7070312 20.707031 L 12 13.414062 L 19.292969 20.707031 L 20.707031 19.292969 L 13.414062 12 L 20.707031 4.7070312 L 19.292969 3.2929688 L 12 10.585938 L 4.7070312 3.2929688 z"></path>
                                </svg>
                              </a>
                              <div class="icon-checked">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                  <g>
                                    <path d="M12,0A12,12,0,1,1,0,12,12,12,0,0,1,12,0Zm0,1A11,11,0,1,1,1,12,11,11,0,0,1,12,1Zm7,7.46L10,18,5,12.16l.76-.65,4.27,5,8.24-8.75Z" fill-rule="evenodd"></path>
                                  </g>
                                </svg>
                              </div>
                              <b>Cảm ơn bạn đã đánh giá sản phẩm!</b>
                            </div>
                          </div>
                        </div>
                        <div id="bpr-reply-thanks" class="bpr-success-popup jquerymodal">
                          <div class="jquerymodal-content">
                            <div class="jquerymodal-body">
                              <a href="javascript:;" rel="jquerymodal:close" class="close-modal">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24px" height="24px">
                                  <path d="M 4.7070312 3.2929688 L 3.2929688 4.7070312 L 10.585938 12 L 3.2929688 19.292969 L 4.7070312 20.707031 L 12 13.414062 L 19.292969 20.707031 L 20.707031 19.292969 L 13.414062 12 L 20.707031 4.7070312 L 19.292969 3.2929688 L 12 10.585938 L 4.7070312 3.2929688 z"></path>
                                </svg>
                              </a>
                              <div class="icon-checked">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                  <g>
                                    <path d="M12,0A12,12,0,1,1,0,12,12,12,0,0,1,12,0Zm0,1A11,11,0,1,1,1,12,11,11,0,0,1,12,1Zm7,7.46L10,18,5,12.16l.76-.65,4.27,5,8.24-8.75Z" fill-rule="evenodd"></path>
                                  </g>
                                </svg>
                              </div>
                              <b>Cảm ơn bạn đã trả lời đánh giá!</b>
                            </div>
                          </div>
                        </div>
                        <style>
                          .sapo-product-reviews-summary,
                          #sapo-product-reviews-noitem {
                            background-color: rgba(128, 187, 53, 0.1)
                          }

                          .bpr-summary-average {
                            color: #80BB35
                          }

                          .sapo-product-reviews-star {
                            color: #ffbe00
                          }

                          .btn-new-review {
                            background-color: #80BB35;
                            border-color: #80BB35
                          }

                          #sapo-product-reviews .sapo-product-reviews-summary .sapo-product-reviews-filter input:checked~.checkmark {
                            color: #80BB35;
                            border-color: #80BB35
                          }

                          #sapo-product-reviews-frm #fileAttach svg path {
                            fill: #80BB35
                          }

                          #sapo-product-reviews-frm .bpr-button-submit {
                            background-color: #80BB35;
                            border-color: #80BB35
                          }

                          .sapo-review-verified,
                          .sapo-review-actions {
                            color: #80BB35
                          }

                          .icon-verified path {
                            fill: #80BB35
                          }

                          #sapo-product-reviews .sapo-product-reviews-list .sapo-review-reply-list .sapo-review-reply-item .sapo-review-reply-author .is-admin {
                            background-color: #80BB35;
                            border-color: #80BB35
                          }

                          .simple-pagination li span.current,
                          .simple-pagination li a.current {
                            color: #80BB35;
                            border-color: #80BB35;
                          }

                          #sapo-product-reviews .sapo-product-reviews-summary .sapo-product-reviews-filter h4,
                          #sapo-product-reviews .sapo-product-reviews-summary .sapo-product-reviews-filter p {
                            background-color: #80BB35;
                            border-color: #80BB35;
                          }

                          #sapo-product-reviews .sapo-product-reviews-summary .sapo-product-reviews-filter h4.active,
                          #sapo-product-reviews .sapo-product-reviews-summary .sapo-product-reviews-filter p.active {
                            color: #80BB35
                          }

                          #sapo-product-reviews .sapo-product-reviews-list .sapo-review-reply-list .btn-show-prev {
                            background-color: rgba(128, 187, 53, 0.1);
                            color: #80BB35
                          }

                          .bpr-success-popup .icon-checked svg path {
                            fill: #80BB35
                          }

                          .sapo-review-reply-form .bpr-form-actions .bpr-reply-button-submit {
                            border-color: #80BB35;
                            background: #80BB35;
                          }
                        </style>
                        <style></style>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
          <div class="section_prd_feature swiper-container swiper_related swiper-container-initialized swiper-container-horizontal swiper-container-free-mode" id="sidebarproduct">
            <div class="title_module section">
              <h2 class="bf_flower">
                <a href="javascript:;" title="Sản phẩm tương tự">Sản phẩm tương tự</a>
              </h2>
            </div>
            <div class=" section products product_related swiper-wrapper" id="swiper-wrapper-a5c71086f90624a1a" aria-live="polite">
                @foreach($product_relates as $product_relate)
              <div class="item swiper-slide swiper-slide-visible swiper-slide-active" role="group" aria-label="1 / 3" style="width: 270px; margin-right: 30px;">
                <div class="item_product_main">
                  <form action="/cart/add" method="post" class="variants product-action MultiFile-intercepted" data-id="product-actions-31460691" enctype="multipart/form-data">
                    <div class="product-thumbnail">
                      <a class="image_thumb scale_hover" href="{{ route('w.pro.index', ['category'=>$product_relate->category->slug, 'product'=>$product_relate->slug]) }}" title="{{$product_relate->name}}">
                        <img class="lazyload" src="{{$product_relate->image}}" data-src="{{$product_relate->image}}" alt="{{$product_relate->name}}">
                      </a>
                      <div class="action1">
                        <input type="hidden" name="variantId" value="91139530">
                        <a href="{{route('w.cart.create', $product_relate->id)}}" class="hidden-xs btn-buy btn-cart btn btn-views left-to add_to_cart active " title="Thêm vào giỏ hàng">
                          <img src="//bizweb.dktcdn.net/100/428/530/themes/824064/assets/i-shopping-cart.svg?1677906059540" alt="{{$product_relate->name}}">
                          <span>Thêm giỏ hàng</span>
                        </a>
                      </div>
                    </div>
                    <div class="product-info">
                      <h3 class="product-name">
                        <a href="{{ route('w.pro.index', ['category'=>$product_relate->category->slug, 'product'=>$product_relate->slug]) }}" title="{{$product_relate->name}}">{{$product_relate->name}}</a>
                      </h3>
                      <div class="price-box"> {{number_format($product_relate->price)}}₫ </div>
                    </div>
                  </form>
                </div>
              </div>
                @endforeach
            </div>
            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
@endsection
@push('css')
@endpush
@push('script')
    <script>
  $(document).ready(function() {
    $('input.input-qty').each(function() {
      var $this = $(this),
        qty = $this.parent().find('.is-form'),
        min = Number($this.attr('min')),
        max = Number($this.attr('max'))
      if (min == 0) {
        var d = 0
      } else d = min
      $(qty).on('click', function() {
        if ($(this).hasClass('minus')) {
          if (d > min) d += -1
        } else if ($(this).hasClass('plus')) {
          var x = Number($this.val()) + 1
          if (x <= max) d += 1
        }
        $this.attr('value', d).val(d)
      })
      $('input.input-qty').change(function() {
        if ($(this).val() < 1) {
          $(this).val(1);
        }
      })
    })
  });
</script>
@endpush
