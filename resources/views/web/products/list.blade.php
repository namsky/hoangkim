@extends('web.layouts.master')

@section('page_thumb', asset('hoangviet/img/vechungtoi.jpg'))
@section('content')
<div class="breadcrumb_background">
  <div class="title_full">
    <div class="container a-center">
      <h1 class="title_page">Tất cả sản phẩm</h1>
    </div>
  </div>
  <section class="bread-crumb">
    <span class="crumb-border"></span>
    <div class="container">
      <div class="row">
        <div class="col-12 a-left">
          <ul class="breadcrumb">
            <li class="home">
              <a href="/">
                <span>Trang chủ</span>
              </a>
              <span class="mr_lr">&nbsp; <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-angle-right fa-w-8">
                  <path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" class=""></path>
                </svg>&nbsp; </span>
            </li>
            <li>
              <strong>
                <span>{{!empty($category) ? $category->category_name : 'Tất cả sản phẩm'}} </span>
              </strong>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
</div>
<div class="section wrap_background">
  <div class="container ">
    <div class="base-sidebar-collection">
      <div class="bg_collection section">
        <div class="row">
          <div class="main_container collection col-xl-9 col-lg-9 col-md-12 col-sm-12">
            <h2 class="collection-title"> {{!empty($category) ? $category->category_name : 'Tất cả sản phẩm'}} </h2>

            <script>
              window.addEventListener('DOMContentLoaded', (event) => {
                var swiperBannerCol = new Swiper('.swiper_col', {
                  slidesPerView: 1,
                  spaceBetween: 13,
                  slidesPerGroup: 1,
                  loop: false,
                  navigation: {
                    nextEl: '.swiper_col .next_col',
                    prevEl: '.swiper_col .prev_col',
                  },
                  breakpoints: {
                    300: {
                      slidesPerView: 1,
                      spaceBetween: 0
                    },
                    640: {
                      slidesPerView: 2,
                      spaceBetween: 30
                    },
                    768: {
                      slidesPerView: 2,
                      spaceBetween: 13
                    },
                    1024: {
                      slidesPerView: 1,
                      spaceBetween: 0
                    },
                    1200: {
                      slidesPerView: 1,
                      spaceBetween: 0
                    }
                  }
                });
              });
            </script>
            <div class="category-products products">
              <section class="products-view products-view-grid collection_reponsive list_hover_pro">
                <div class="row content-col">
                  @foreach ($products as $product)
                  <div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4 product-col">
                    <div class="item_product_main">
                      <form action="/cart/add" method="post" class="variants product-action" data-id="product-actions-31460698" enctype="multipart/form-data">
                        <div class="product-thumbnail">
                          <a class="image_thumb scale_hover" href="{{ route('w.pro.index', ['category'=>$product->category->slug, 'product'=>$product->slug]) }}" title="{{$product->name}}">
                            <img class="lazyload loaded" src="{{ $product->image }}" alt="{{$product->name}}" data-was-processed="true">
                          </a>
                          <div class="action1">
                            <a href="{{route('w.cart.create', $product->id)}}" class="hidden-xs btn-buy btn-cart btn btn-views left-to add_to_cart active " title="Thêm vào giỏ hàng">
                              <img src="//bizweb.dktcdn.net/100/428/530/themes/824064/assets/i-shopping-cart.svg" alt="{{$product->name}}">
                              <span>Thêm giỏ hàng</span>
                            </a>
                          </div>
                        </div>
                        <div class="product-info">
                          <h3 class="product-name">
                            <a href="{{ route('w.pro.index', ['category'=>$product->category->slug, 'product'=>$product->slug]) }}" title="{{$product->name}}">{{$product->name}}</a>
                          </h3>
                          <div class="price-box"> {{ number_format($product->price , 0, ',', '.') }} ₫</div>
                        </div>
                      </form>
                    </div>
                  </div>
                  @endforeach

                </div>
                  {{$products->links()}}
{{--                <div class="section pagenav">--}}
{{--                  <nav class="clearfix relative nav_pagi w_100">--}}
{{--                    <ul class="pagination clearfix">--}}
{{--                      <li class="page-item disabled">--}}
{{--                        <a class="page-link" href="#">--}}
{{--                          <i class="icon icon-left"></i>--}}
{{--                        </a>--}}
{{--                      </li>--}}
{{--                      <li class="active page-item disabled">--}}
{{--                        <a class="page-link" href="javascript:;">1</a>--}}
{{--                      </li>--}}
{{--                      <li class="page-item">--}}
{{--                        <a class="page-link" onclick="doSearch(2)" href="javascript:;">2</a>--}}
{{--                      </li>--}}
{{--                      <li class="page-item hidden-xs">--}}
{{--                        <a class="page-link" onclick="doSearch(2)" href="javascript:;">--}}
{{--                          <i class="icon icon-right" aria-hidden="true"></i>--}}
{{--                        </a>--}}
{{--                      </li>--}}
{{--                    </ul>--}}
{{--                  </nav>--}}
{{--                  <script>--}}
{{--                    var cuPage = 1--}}
{{--                  </script>--}}
{{--                </div>--}}
              </section>
            </div>
          </div>
          <aside class="sidebar left-content col-xl-3 col-lg-3 col-md-12 col-sm-12">
            <div class="filter-mb wrap_background_aside asidecollection">
              <div class="filter-mb-h">
                <div class="filter-mb-sc">
                  <aside class="aside-item sidebar-category collection-category margin-bottom-20">
                    <div class="aside-title">
                      <h2 class="title-head margin-top-0 cate">
                        <span>Danh mục sản phẩm</span>
                      </h2>
                    </div>
                    <div class="aside-content padding-bottom-20">
                      <nav class="nav-category navbar-toggleable-md">
                        <ul class="nav navbar-pills">
                            @foreach($categories as $categorie)
                          <li class="nav-item">
                            <a class="nav-link" href="{{route('w.products.category',$categorie->slug)}}">{{$categorie->category_name}}</a>
                          </li>
                            @endforeach
                        </ul>
                      </nav>
                    </div>
                  </aside>

                </div>

              </div>
            </div>
          </aside>
        </div>
      </div>
    </div>
  </div>
</div>
</section>


@endsection
