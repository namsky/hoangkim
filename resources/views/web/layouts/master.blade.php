<!DOCTYPE html>
<html lang="vi">
  <head>
    <meta charset="UTF-8" />
    <meta name="theme-color" content="#f02b2b" />
    <link rel="canonical" href="index.html" />
    <meta name='revisit-after' content='2 days' />
    <meta name="robots" content="noodp,index,follow" />
      <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta property="og:type" content="website">
    <link rel="icon" href="{{asset('customer/logo.png')}}" type="image/x-icon" />
    <link rel="preload" as='style' type="text/css" href="{{asset('ionpia/assets/header.scssf184.css')}}">
    <link rel="preload" as='style' type="text/css" href="{{asset('ionpia/assets/header.scssf184.css')}}">
    <link rel="preload" as='style' type="text/css" href="{{asset('ionpia/assets/quickviews.scssf184.css')}}">
    <link rel="preload" as='style' type="text/css" href="{{asset('ionpia/assets/popup.scssf184.css')}}">
    <link rel="preload" as='style' type="text/css" href="{{asset('ionpia/assets/responsive.scssf184.css')}}">
    <link rel="preload" as='style' type="text/css" href="{{asset('ionpia/assets/main.scssf184.css')}}">
    <link rel="preload" as='style' type="text/css" href="{{asset('ionpia/assets/index.scssf184.css')}}">
    <link rel="preload" as="style" href="{{asset('ionpia/assets/bootstrapf184.css')}}" type="text/css">
    <link rel="preload" as='style' type="text/css" href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&amp;display=swap">
    <link rel="preload" as='style' type="text/css" href="https://fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700;800;900&amp;display=swap">
    <link rel="preload" as='style' type="text/css" href="https://fonts.googleapis.com/css2?family=Niramit:wght@400;500;600;700&amp;display=swap">
    <!--------------------------------------------------------------------------------------------------->
    <link href="{{asset('ionpia/assets/header.scssf184.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('ionpia/assets/bootstrapf184.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('ionpia/assets/responsive.scssf184.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('ionpia/assets/popup.scssf184.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('ionpia/assets/main.scssf184.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('ionpia/assets/index.scssf184.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('ionpia/assets/contact_style.scssf184.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('ionpia/assets/quickviews.scssf184.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('ionpia/assets/product_style.scssf184.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('ionpia/assets/bpr.css')}}" rel="stylesheet" type="text/css" media="all" />
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>

  </head>
<body>
    @include('web.layouts.includes.header')

    @include('sweetalert::alert')

    <div class="clearfix"></div>

    @yield('content')

    @include('web.layouts.includes.footer')

    {{-- alert --}}
    @include('web.components.alert')
    @include('web.components.errors')

    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
    <script type="text/javascript">
        $(function() {
            $(".slide_gioithieu").owlCarousel({

                items: 2,
                responsive: {
                    1200: { item: 2 }, // breakpoint from 1200 up
                    982: { items: 2 },
                    768: { items: 2 },
                    480: { items: 1 },
                    0: { items: 1 }
                },
                loop: true,
                rewind: false,
                autoplay: true,
                autoplayTimeout: 8000,
                autoplayHoverPause: false,
                smartSpeed: 1500, //slide speed smooth

                dots: false,
                dotsEach: true,
                nav: true,
                navText: ['<i class="fa fa-angle-left icon_slider"></i>', '<i class="fa fa-angle-right icon_slider"></i>'],
                navSpeed: 250, //slide speed when click button

                autoWidth: false,
                margin: 15,
            });
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/readmore-js@2.1.0/readmore.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.0/css/swiper.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.0/js/swiper.min.js"></script>
    @stack('scripts')
</body>

</html>
