@extends('web.layouts.master')
@section('page_title', 'Thư ngỏ')
@section('page_thumb', asset('hoangviet/img/vechungtoi.jpg'))
@section('content')
  <section class="danhmuc_product cate_news_hot" style="margin: 20px 0;">
    <div class="container container_feature">
      <div class="row justify-content-center" style="display:flex; justify-content: center;align-items: center">
        <div class="col-lg-9">
          <div class="cate_box_new">
            <div class="list_new_mid">
              <div class="">
                <h1 class="title_detail" style="font-size: 23px;">Thư ngỏ</h1>
                <div class="clearfix-15"></div>
                <div class="content">
                    <p>
                        <i>Kính thưa quý vị khách hàng, đối tác !</i>
                        <br>
                        CÔNG TY CỔ PHẦN TẬP ĐOÀN KHOA HỌC CÔNG NGHỆ HOÀNG VIỆT, Công ty ra đời trong bối cảnh được Đảng, Chính Phủ ủng hộ tuyệt đối về việc xây dựng và phát triển Doanh nghiệp đổi mới sáng tạo, ứng dụng khoa học, công nghệ vào việc tổ chức nghiên cứu, sản xuất, kinh doanh. Công ty chúng tôi với định hướng, chiến lược phát triển sản phẩm và xây dựng thương hiệu; với sứ mệnh cam kết cung cấp những sản phẩm chất lượng cao, hiệu quả và an toàn trong suốt hành trình “<b><i>chăm sóc sức khỏe cộng đồng</i></b>”.
                        <br>
                        Lấy sức khỏe cộng đồng là mục tiêu nhất quán và lâu dài để hướng tới. Luôn luôn nắm bắt - đổi mới - ứng dụng công nghệ hiện đại để tạo những sản phẩm chất lượng cao.
                        <br>
                        Lấy chất lượng sản phẩm làm cam kết cao nhất và sự tín nhiệm của khách hàng làm mục đích phấn đấu lâu dài.
                        <br>
                        Lấy lợi ích và sự thịnh vượng của khách hàng làm mục tiêu dài hạn.
                        <br>
                        Lấy sự sáng tạo - cống hiến - trí thức làm phương châm hành động.
                        <br>
                        Chúng tôi luôn cố gắng “<b>Đẩy mạnh kết nối</b>” nhưng vẫn “<b>Chú trọng phát triển bền vững</b>” - “<b>Vì sức khỏe cộng đồng Việt</b>” để trở thành một TẬP ĐOÀN KHOA HỌC CÔNG NGHỆ CHUYÊN NGHIỆP -  HOÀNG VIỆT GROUP.
                        <br>
                        Với tầm nhìn chiến lược của lãnh đạo công ty và sự nỗ lực không mệt mỏi của đội ngũ cán bộ luôn đồng lòng gắng sức, Công ty chúng tôi đang trên con đường trở thành một trong những công ty hàng đầu trong lĩnh vực sản xuất kinh doanh những sản phẩm chăm sóc sức khoẻ cộng đồng CHẤT LƯỢNG - KHÁC BIỆT - HIỆU QUẢ.
                        <br>
                        Bên cạnh hội tụ đầy đủ các yếu tố, cộng với sự chung sức, chung lòng của những nhà khoa học hàng đầu Việt Nam như: GS. TS. NGND - NGUYỄN LÂN DŨNG, PGS. TS. NGND - NGUYỄN VÕ KỲ ANH, TS. NGND - LÊ ĐÌNH TIẾN, THS. NGÔ QUỐC LUẬT, THS. LÊN TÂN cùng với các GS, TS đầu ngành ở Việt Nam. Hoàng Việt Group - với một khát khao cháy bỏng trở thành Doanh nghiệp khoa học công nghệ chuyên nghiệp và trở thành thành công ty đại chúng trong thời gian sớm nhất, chúng tôi tự tin sẽ đem đến sự hài lòng nhất cho khách hàng khi đến với chúng tôi.
                        <br>
                        Chân thành cám ơn Quý khách hàng, Quý đối tác!
                        <br>
                        <p style="text-align: end">Hà Nội, tháng 4 năm 2023.</p>
                    </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </section>
@endsection
