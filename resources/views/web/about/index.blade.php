@extends('web.layouts.master')

@section('content')
<div class="breadcrumb_background">
	<div class="title_full">
		<div class="container a-center">
			<span class="title_page">Hoàng Kim - Hạnh Phúc Muôn Nơi</span>
		</div>
	</div>
	<section class="bread-crumb">
	<span class="crumb-border"></span>
	<div class="container">
		<div class="row">
			<div class="col-12 a-left">
				<ul class="breadcrumb">
					<li class="home">
						<a href="/"><span>Trang chủ</span></a>
						<span class="mr_lr">&nbsp;<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-angle-right fa-w-8"><path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z" class=""></path></svg>&nbsp;</span>
					</li>

					<li><strong><span>Hoàng Kim Mart</span></strong></li>

				</ul>
			</div>
		</div>
	</div>
</section>
</div>
<section class="page">
	<div class="container">
		<div class="wrap_background_aside padding-top-15 margin-bottom-40">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="page-title category-title">
					<h1 class="title-head"><a href="#">Hoàng Kim – Hạnh Phúc Muôn Nơi</a></h1>
				</div>
				<div class="content-page rte">
					<p style="margin-bottom:11px">
                        <span style="font-size:11pt">
                            <span style="line-height:107%">
                                <span style="font-family:Calibri,&quot;sans-serif&quot;"
                                ><span lang="VI" style="font-size:15.0pt">
                                        <span style="line-height:107%">Tên công ty : Công Ty TNHH Xúc Tiến Thương Mại Hoàng Kim</span></span></span></span></span></p>
<p style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,&quot;sans-serif&quot;"><span lang="VI" style="font-size:15.0pt"><span style="line-height:107%">Địa chỉ : Só 27 Yết Kiêu, Quận Hà Đông, Thành Phố Hà Nội</span></span></span></span></span></p>
<p style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,&quot;sans-serif&quot;"><span lang="VI" style="font-size:15.0pt"><span style="line-height:107%">Hotline : 0982.768.265</span></span></span></span></span></p>
<p style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,&quot;sans-serif&quot;"><span lang="VI" style="font-size:15.0pt"><span style="line-height:107%">Với tiêu chí Sức Khỏe Là Vàng, tại Hoàng Kim chúng tôi chỉ cung cấp các sản phẩm có nguồn gốc rõ ràng và đã được các chuyên gia của Hoàng Kim trực tiếp kiểm tra và đánh giá. Ngoài ra Hoàng Kim còn kết hợp với các nông trai để đưa đến tay người tiêu dùng các sản phẩm chất lượng và an toàn tuyệt đối.</span></span></span></span></span></p>
<p style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,&quot;sans-serif&quot;"><span lang="VI" style="font-size:15.0pt"><span style="line-height:107%">Thông điệp của chúng tôi:</span></span></span></span></span></p>
<p style="margin-bottom:11px"><span style="font-size:11pt"><span style="line-height:107%"><span style="font-family:Calibri,&quot;sans-serif&quot;"><span lang="VI" style="font-size:15.0pt"><span style="line-height:107%">HOÀNG KIM – Hạnh Phúc Muôn Nơi</span></span></span></span></span></p>
				</div>
			</div>
		</div>
		</div>
	</div>
</section>
@endsection
