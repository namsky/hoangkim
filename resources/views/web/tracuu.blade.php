<section class="tracuu hidden-sm hidden-xs">
    <div class="container">
      <div class="sum-noidung-tracuu">
        <h2>Tra Cứu Thuốc, TPCN, Bệnh lý...</h2>
        <form action="https://thuanphongpharma.com/tim-kiem" method="get">
          <div class="noidung-tracuu">
            <input onkeyup="result_search_product(this)" type="text" placeholder="Nhập từ khoá... " name="s">
            <button type="submit">
              <i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </div>
        </form>
        <h3>Tra Cứu Hàng Đầu</h3>
        <div class="motso_tu_khoa_noibat">
          <a href="tim-kiem?s=viên bổ phế">viên bổ phế</a>
          <a href="tim-kiem?s=dung dịch xịt mũi">dung dịch xịt mũi</a>
          <a href="tim-kiem?s=panadol">panadol</a>
          <a href="tim-kiem?s=thuốc điều trị ho">thuốc điều trị ho</a>
          <a href="tim-kiem?s=sữa rửa mặt">sữa rửa mặt</a>
        </div>
      </div>
    </div>
  </section>
  <section class="muathuoc_dedang">
    <div class="container">
      <h2>Mua Hàng Dễ Dàng Tại Thuận Phong</h2>
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-4">
          <div class="item-muathuocde item-muathuocde-1">
            <div class="img-item-muathuocde">
              <img src="{{ asset('hoangviet/img/muade-1.jpg') }}" alt="">
            </div>
            <div class="noidung-item-muathuocde">
              <h3>Chụp đơn hàng</h3>
              <p class="hidden-sm hidden-xs">đơn giản & nhanh chóng</p>
            </div>
            <div class="icon-right-item hidden-md hidden-lg">
              <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
          <div class="item-muathuocde item-muathuocde-1">
            <div class="img-item-muathuocde ">
              <img src="{{ asset('hoangviet/img/muade-2.jpg') }}" alt="">
            </div>
            <div class="noidung-item-muathuocde">
              <h3>Gửi thông tin <span class="hidden-sm hidden-xs">đơn hàng</span>
              </h3>
              <p class="hidden-sm hidden-xs">để được tư vấn đặt hàng</p>
            </div>
            <div class="icon-right-item hidden-md hidden-lg">
              <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
          <div class="item-muathuocde">
            <div class="img-item-muathuocde">
              <img src="{{ asset('hoangviet/img/muade-3.jpg') }}" alt="">
            </div>
            <div class="noidung-item-muathuocde">
              <h3>Nhận báo giá <span class="hidden-sm hidden-xs"> từ công ty</span>
              </h3>
              <p class="hidden-sm hidden-xs">kèm theo tư vấn miễn phí</p>
            </div>
          </div>
        </div>
      </div>
      <h5>
        <span>
          <a href="https://zalo.me/0964333146">Đặt Hàng Ngay</a>
        </span>
        <p>Hoặc mua qua hotline <b>
            <a href="https://zalo.me/0964333146">0964333146</a>
          </b>
        </p>
      </h5>
    </div>
  </section>
  <style>
    .sum-noidung-tracuu {
      padding: 24px 80px;
      border-radius: 16px;
      background-color: #fff;
      /* margin-top: -88px; */
      position: relative;
      z-index: 2;
      box-shadow: 0 1px 4px rgb(10 10 10 / 15%);
    }
  
    .sum-noidung-tracuu h2 {
      margin-bottom: 16px;
      font-size: 32px;
      line-height: 40px;
      color: #1e293b;
    }
  
    .sum-noidung-tracuu h3 {
      font-size: 20px;
      line-height: 28px;
      margin-bottom: 8px;
      color: #1e293b;
      font-weight: 400;
    }
  
    section.tracuu {
      /* transform: translateY(-87px); */
      margin-top: -50px;
    }
  
    .sum-noidung-tracuu form {
      border-radius: 100px;
      min-height: 56px;
      /* padding: 0 16px 0 24px; */
      z-index: 3;
      position: relative;
      border: 1px solid #d8e0e8;
    }
  
    .sum-noidung-tracuu input {
      font-size: 18px;
      line-height: 24px;
      color: #718198;
      height: 54px;
      width: 100%;
      border-radius: 50px 0 0 50px;
      padding-left: 15px;
      border: none;
      outline: none;
    }
  
    .sum-noidung-tracuu button {
      border-radius: 0 100px 100px 0;
      padding: 0 28px;
      min-width: 126px;
      color: #fff;
      background: #007732;
      outline: none;
      border: 1px solid #d8e0e8;
      outline: none;
    }
  
    .sum-noidung-tracuu button i {
      font-size: 24px;
    }
  
    .noidung-tracuu {
      width: 100%;
      display: flex;
      width: 100%;
      font-family: inherit;
      background-color: #fff;
      background-clip: padding-box;
      /* border: 1px solid #d8e0e8; */
      border-radius: 12px;
      transition: color .3s ease-in-out, background-color .3s ease-in-out, border-color .3s ease-in-out, box-shadow .3s ease-in-out;
      border-radius: 50px;
    }
  
    .motso_tu_khoa_noibat {
      display: flex;
      gap: 8px 12px;
      flex-wrap: wrap;
    }
  
    .motso_tu_khoa_noibat a {
      color: #334155;
      background-color: #edf2f8;
      font-size: 16px;
      line-height: 32px;
      display: inline-flex;
      align-items: center;
      justify-content: center;
      position: relative;
      cursor: pointer;
      padding-right: 12px;
      padding-left: 12px;
      border-radius: 20px;
    }
  
    @media (max-width:992px) {
      .motso_tu_khoa_noibat {
        overflow-x: scroll;
      }
    }
  
    @media (max-width: 768px) {
      .motso_tu_khoa_noibat {
        gap: 0 8px;
        flex-wrap: nowrap;
        white-space: nowrap;
        margin-right: -10px;
      }
  
      .sum-noidung-tracuu {
        padding: 10px 10px !important;
      }
  
      .sum-noidung-tracuu h2 {
        font-size: 16px !important;
      }
  
      .sum-noidung-tracuu input {
        height: 40px !important;
      }
  
      .sum-noidung-tracuu button {
        height: 40px !important;
        min-width: 100px !important;
      }
  
      .sum-noidung-tracuu form {
        min-height: 40px !important;
      }
    }
  </style>