<?php

use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\HomewController;
use App\Http\Controllers\Web\AccountwController;
use App\Http\Controllers\Web\LoginwController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\ProductController;
// use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\LogMoneyController;
use App\Http\Controllers\Customer\LoginsController;
use App\Http\Controllers\Customer\HomesController;
use App\Http\Controllers\Customer\OrdersController;
use App\Http\Controllers\Customer\AccountController;
use App\Http\Controllers\Customer\BankAccountController;
use App\Http\Controllers\Customer\CustomersController;
use App\Http\Controllers\Customer\ProductsController;
use App\Http\Controllers\Customer\PackageController;
use App\Http\Controllers\Customer\PayController;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;

// create account admin auto

//Route::get('/create-admin', function () {
//  $data['name'] = 'admin';
//  $data['email'] = 'admin@gmail.com';
//  $data['phone'] = '0987654321';
//  $data['password'] = Hash::make('admin123');
//  Admin::create($data);
//  session()->flash('success', 'Thành công');
//  return redirect()->route('ad.admin.index');
//});

// Admin router (quản trị viên)
Route::get('admin/login', 'Admin\LoginController@index')->name('ad.login.index');
Route::post('admin/login', 'Admin\LoginController@login')->name('ad.login');

Route::group(['prefix' => '/admin', 'as' => 'ad.', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index')->name('index');
    Route::post('logout', 'LoginController@logout')->name('logout');
    Route::resource('product', 'ProductController')->except('show');
    Route::post('/product/status/{id}', 'ProductController@status')->name('product.status');
    Route::get('/product/hot', 'ProductController@hot')->name('pro.hot');
    Route::get('search-product', 'ProductController@searchProduct')->name('product.searchProduct');
    Route::resource('admin', 'AdminController')->except('show');
    Route::resource('customer', 'CustomerController');
    Route::get('search-customer', 'CustomerController@searchCustomer')->name('customer.searchCustomer');

    //List Khách hàng vãng lai
    Route::get('khach-hang-vang-lai','CustomerController@customer')->name('customer.customer');
    // blog
    Route::resource('blog', 'BlogController')->except('show');
    Route::resource('blog_category', 'BlogCategoryController')->except('show');

    // Cộng trừ tiền
    Route::post('/customer/money', 'CustomerController@money')->name('customer.money');
    // Route::resource('user', 'UserController');
    // Route::get('search-user', 'UserController@searchUser')->name('user.searchUser');
    Route::resource('order', 'OrderController');
    Route::get('search-order', 'OrderController@searchOrder')->name('order.searchOrder');
    Route::resource('affiliate', 'AffiliateController');
    Route::get('/log', 'LogMoneyController@index')->name('log');

    //config
    Route::resource('/config', 'ConfigController');

    //banner
    Route::resource('/banner', 'BannerController');
    Route::patch('/banner/type/{banner}', 'BannerController@type')->name('banner.type');

    Route::get('/log-bank', 'LogMoneyController@log_bank')->name('log.bank');
    Route::get('/log-wallet', 'LogMoneyController@log_wallet')->name('log.wallet');
    Route::get('/log-transaction', 'LogMoneyController@log_transaction')->name('log.transaction');
    Route::get('/log-point', 'LogMoneyController@log_point')->name('log.point');
    Route::get('/log-order', 'LogMoneyController@log_order')->name('log.order');

    // Chức năng mua điểm
    Route::get('/point', 'UserController@buy_point')->name('point.index');
    Route::post('/point/update', 'UserController@update_point')->name('point.update');

    // Quản lý nạp / rút
    Route::get('recharge','PaymentController@recharge')->name('payment.recharge');
    Route::patch('post_recharge/{id}','PaymentController@post_recharge')->name('payment.post_recharge');


    Route::get('withdraw','PaymentController@withdraw')->name('payment.withdraw');
    Route::patch('withdraw/{id}','PaymentController@post_withdraw')->name('payment.post_withdraw');
    //End

    Route::get('search-affiliate', 'AffiliateController@searchAffiliate')->name('affiliate.searchAffiliate');
    // Xuất file Excel
    Route::get('export', 'OrderController@exportOrders')->name('exportOrders');
    Route::get('export', 'OrderController@export')->name('export');

    // Quản lý danh mục sản phẩm
    Route::resource('category', 'CategoryController');

    // Tính và cộng thưởng hoa hồng theo tháng
    Route::get('/bonus',  [OrdersController::class,'month_bonus'])->name('bonus');

    // Cấu hình % hoa hồng, thu nhập, thưởng
    Route::get('rose-config', 'ConfigController@rose_config')->name('rose.config');
    Route::post('rose-update', 'ConfigController@rose_update')->name('rose.update');
    // Cấu hình thưởng nhóm theo ngày
    Route::get('group-bonus', 'ConfigController@group_bonus_config')->name('group_bonus.config');
    Route::post('group-bonus-update', 'ConfigController@group_bonus_update')->name('group_bonus.update');
    // Cấu hình thưởng thu nhập/thu nhập
    Route::get('incomes', 'ConfigController@incomes_config')->name('incomes.config');
    Route::post('incomes-update', 'ConfigController@incomes_update')->name('incomes.update');

    // Hiển thị danh sách đội nhóm
    Route::get('groups', 'AffiliateController@show_groups')->name('group.index');
    Route::get('group_detail/{group_name}', 'AffiliateController@group_detail')->name('group.group_detail');

    // Săn Voucher
    Route::get('san-voucher', 'VoucherController@index')->name('voucher.index');
    Route::post('confirm-san-voucher/{id}', 'VoucherController@confirm')->name('voucher.confirm');
    Route::post('destroy-san-voucher/{id}', 'VoucherController@destroy')->name('voucher.destroy');


});

// Customer router (Đại lý)
Route::get('customers/login', [LoginsController::class,'index'])->name('us.login.index');
Route::post('customers/login', [LoginsController::class,'login'])->name('us.login');

Route::get('customers/group', [HomesController::class,'group_money']);

Route::group(['prefix' => '/customers', 'as' => 'us.', 'namespace' => 'Customer', 'middleware' => 'customerLogin'],function () {
    Route::get('san-pham-dau','ProductsController@checkmember')->name('checkmember');
    Route::post('san-pham','ProductsController@add_to_cart')->name('product.add_to_cart');
    Route::get('san-pham-checkout','ProductsController@checkout')->name('product.checkout');
    Route::get('san-pham-reset','ProductsController@reset')->name('product.reset');
    Route::post('san-pham-thanh-toan','ProductsController@store_dau_vao')->name('product.store_dau_vao');


    Route::get('mua-san-pham-dau/{id}','ProductsController@store')->name('buycombo.store');
    Route::get('thong-tin-san-pham-dau-tu','ProductsController@info')->name('buycombo.info');
    Route::get('thanh-toan-san-pham-dau-tu','ProductController@payment')->name('buycombo.payment');
//    Route::get('mua-san-pham-dau','ProductsController@store')->name('buycombo.store');
    Route::group(['middleware' => 'checkMember'], function (){
        Route::get('/', [HomesController::class,'index'])->name('home');
        Route::resource('account', 'AccountController')->only('index','update');

        Route::get('group_detail/{group_name}', 'IonGroupController@show')->name('group.show');

        // Chuyển ví
        Route::post('bank_account', 'AccountController@bank_account')->name('bank.bank_account');
        Route::get('bank_wallet', 'AccountController@bank_wallet_view')->name('bank.bank_wallet_view');
        Route::post('bank_wallet/{user}', 'AccountController@bank_wallet')->name('bank.bank_wallet');
        Route::get('chuyen-diem', 'AccountController@bank_out_view')->name('bank.bank_out_view');
        Route::post('chuyen-diem-post', 'AccountController@bank_transfer')->name('bank.bank_out');
        Route::get('bank/search', 'AccountController@search_receiver')->name('bank.search_receiver');

        // Nạp tiền
        Route::get('payment', 'PaymentController@index')->name('payment.index');
        Route::get('payment-history', 'PaymentController@history')->name('payment.history');
        Route::post('payment-offline', 'PaymentController@payment_offline')->name('payment.offline');
        //End
        //Rút tiền
        //Route::get('payment', 'PaymentController@index')->name('payment.index');
        Route::get('rut-diem', 'PaymentController@withdraw')->name('payment.withdraw');
        Route::post('payment-withdraw', 'PaymentController@post_withdraw')->name('payment.post_withdraw');
        Route::get('lich-su-rut-diem', 'PaymentController@history_withdraw')->name('payment.history_withdraw');
        // End

        // Lịch sử giao dịch
        Route::get('/history', 'AccountController@history')->name('history');

        Route::get('order', 'OrdersController@index')->name('order.index');
        Route::resource('affiliate', 'AffiliatesController')->only('index');
        Route::resource('customer', 'CustomersController')->only('index');
        Route::resource('product', 'ProductsController')->only('index');
        Route::resource('package', 'PackageController')->only('index');
        // Route::get('pay-product/{product}', 'PayController@index')->name('pay.product')->middleware(['checkOrder']);
        Route::resource('bank', 'BankAccountController')->only('index','store','update');
        Route::get('logout', 'LoginsController@logout')->name('logout');
        Route::post('search', 'BankAccountController@search')->name('search');

        Route::get('network/affiliate', 'AffiliatesController@index')->name('aff.index');

        // Cửa hàng
        Route::resource('product','ProductController')->except('show');
        Route::get('san-pham-dau-tu','ProductController@combo')->name('pro.combo');
        Route::get('san-pham-dau-tu/mua/{id}','ProductController@combo_store')->name('product.combo_store');

        // Săn Voucher
        Route::get('san-voucher','VoucherController@index')->name('voucher.index');
        Route::post('san-voucher','VoucherController@buy')->name('voucher.buy');
        Route::post('upload_bill','VoucherController@upload_bill')->name('voucher.upload_bill');



    });



});

Route::get('/payment/cancel', 'Customer\PayController@cancel')->name('cart.payment.cancel');
Route::get('/payment/alepay_success', 'Customer\PayController@alepay_success')->name('cart.payment.alepay_success');

Route::get('ref/{username}', 'Customer\AffiliatesController@ref')->name('ref');

Route::post('pay', 'Customer\PayController@store')->name('us.pay.store');
Route::get('pay-product/{product}', 'Customer\PayController@index')->name('us.pay.product');
Route::get('pay-package/{product}', 'Customer\PayController@index')->name('us.pay.package');

// Thanh toán giỏ hàng
Route::get('customer-pay', 'Customer\PayController@customer_pay')->name('w.customer_pay');
Route::post('customer-pay/store', 'Customer\PayController@store_order')->name('w.customer_pay.store');

Route::get('payment/success','Customer\PaymentController@success')->name('payment.success');
Route::get('payment/return','Customer\PaymentController@responeVnpay')->name('payment.return');

// Đăng ký thông qua giới thiệu
Route::get('/customers/{link}', 'Customer\LoginsController@register')->middleware(['checkLinkRegister']);
//Route::post('/customers/register', [LoginsController::class,'store'])->name('us.register.store');
//Đăng kí ngoài trang web
Route::get('/register', [LoginsController::class,'web_register'])->name('w.register');
Route::post('/customers/store', [LoginsController::class,'store'])->name('w.register.store');


// Thưởng tháng theo combo mua sản phẩm
Route::get('/bonus', 'Customer\OrdersController@month_bonus')->name('bonus');

// Thưởng nhóm theo ngày
Route::get('/day_bonus', 'Customer\OrdersController@day_bonus')->name('day_bonus');


//  User router (khách hàng)

 Route::group(['as' => 'w.', 'namespace' => 'Web'], function () {
    Route::get('/', 'HomewController@index')->name('index');
    /* blog */
    Route::get('/danh-sach-bai-viet', 'BlogController@blogs')->name('blogs');
    Route::get('/danh-sach-bai-viet/{category_slug}/{blog_slug}', 'BlogController@single_blog')->name('single_blog');

    /* san pham */
    Route::get('san-pham','ProductController@listProducts')->name('products');
    /* tim kiem */
    Route::get('san-pham/tim-kiem', 'ProductController@search')->name('product.search');
    Route::get('san-pham/{category}/{product}', 'ProductController@index')->name('pro.index');
    Route::get('san-pham/{slug}','ProductController@listProductsCategory')->name('products.category');


    Route::get('shop/category/{id}', 'ProductController@product_archive')->name('product.archive');
    Route::get('check-login','HomewController@check_login')->name('check_login');
    Route::get('/ve-chung-toi', 'HomewController@aboutUs')->name('about');
    Route::get('/thu-ngo', 'HomewController@letter')->name('letter');
    Route::get('/lien-he', 'ContactController@index')->name('contact');
    Route::get('/tuyen-dung', 'RecruitmentController@index')->name('recruitment');
    //Cart
    Route::get('/gio-hang', 'CartController@index')->name('cart.index');
    Route::get('/them-san-pham/{id}', 'CartController@create')->name('cart.create');
    Route::post('/mua-gio-hang', 'CartController@store')->name('cart.store');
    Route::post('/cap-nhat-gio-hang', 'CartController@update')->name('cart.update');
    Route::get('/xoa-san-pham/{rowId}', 'CartController@destroy')->name('cart.destroy');




 });

Route::group(['prefix' => 'laravel-filemanager'], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

// Cron Job
// Chia Voucher hàng ngày
Route::get('/chia-voucher-daily', 'CronJobController@chia_voucher_daily');


?>
