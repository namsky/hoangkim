$(document).ready(function() {
    $('#toggle').click(function() {
        $('nav').slideToggle();
    });
    $('#dropdown').click(function() {
      $('.nav-dropdown').slideToggle();
  });
});
 var  slider1 = new Splide( '#splide1', {
     type      : 'loop',
     direction : 'ttb',
     height    : '450px',
     perPage: 3,
     focus     : 0,
     autoStart:  true,
     autoScroll: {
         speed: 0.1,
  },
    })
slider1.mount(window.splide.Extensions);
// .mount( window.splide.Extensions );

var  slider2 = new Splide( '#splide2', {
  //   type      : 'loop',
  //   drag      : 'free',
  //   height    : '350px',
  //   width     : '100%',
  //   perPage   : 5,
  //   focus     : 0,
  //   omitEnd   : true,
  //   autoStart :  true,
  //   autoScroll: {
  //   speed     : 0.1,
  // },
  type       : 'loop',
  height     : '350px',
  focus     : 0,
  width     : '100%',
  paginationDirection: 'ltr',
  perPage    : 1,
  omitEnd: true,
    })
    slider2.mount(window.splide.Extensions);

// var  slider4 = new Splide( '#splide4', {
//     type      : 'loop',
//     direction : 'ttb',
//     height    : '450px',
//     perPage: 3,
//     focus     : 0,
//     autoStart:  true,
//     autoScroll: {
//         speed: 0.1,
//     },
// })
// slider4.mount(window.splide.Extensions);

var  slider3 = new Splide( '#splide3', {
    type      : 'loop',
    direction : 'ttb',
    height    : '450px',
    perPage: 3,
    focus     : 0,
    autoStart:  true,
    autoScroll: {
    speed: 0.1,
  },
    })
    slider3.mount(window.splide.Extensions);
