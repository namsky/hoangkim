$(document).ready(function(){
    // Scroll btn
    $(window).scroll(function(){
        if($(window).width() > 768){
            if($(this).scrollTop() > 100){
                $('#navbar').addClass('sticky');
            } else{
                $('#navbar').removeClass('sticky');
            }
        }
    });

    /* scroll to top */
    // Scroll Top Hide Show
    $(window).on('scroll', function(){
        if ($(this).scrollTop() > 250) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    //Scroll To Top Animate
    $('.scroll-to-top').on('click', function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });

    //   CLICK NAV ITEM TO SCROLL
    $("#link-introduce").click(function (){
        $('html, body').animate({
            scrollTop: $("#sec_introduce").offset().top
        }, 1000);
    });

    $("#link-products").click(function (){
        $('html, body').animate({
            scrollTop: $("#sec_product").offset().top
        }, 1000);
    });



    $("#link-feedback").click(function (){
        $('html, body').animate({
            scrollTop: $("#sec_feedback").offset().top
        }, 1000);
    });

    $("#link-footer").click(function (){
        $('html, body').animate({
            scrollTop: $('footer').offset().top
        }, 1000);
    });



    // form dang ky
    $("#order-product").submit(function(e) {
        // value form
        e.preventDefault();
        var name = $("#name").val();
        var phone = $("#phone").val();
        var address = $("#address").val();
        var product = $("#product").val();
        var amount = $("#amount").val();
        // var sale  = $("#sale").val();
        if( name == '' || phone == ''  || address=='' || product=='' || amount==''){
            Swal.fire({
                title: 'Vui lòng nhập đủ thông tin !',
                icon: 'error',
                confirmButtonText: 'Đóng'
            })
        }else{
            // message telegram
            var message = `Khách mua hàng:     Tên: ${name}        - Số điện thoại: ${phone}       - Địa chỉ: ${address}      - Sản phẩm: ${product}       - Số lượng: ${amount}`;
            $.ajax({
                type: "GET",
                url: '' + message,
                data: '', // serializes the form's elements.
                success: function(data)
                {
                    if(data.ok==true){
                        Swal.fire(
                            'Gửi Thành Công!',
                            'Cảm ơn bạn đã đăng ký!',
                            'success'
                        )
                    } else{
                        Swal.fire({
                            title: 'Gửi thất bại!',
                            text: 'Vui lòng thử lại sau!',
                            icon: 'error',
                            confirmButtonText: 'Cool'
                        })
                    }
                }
            })
        }
    });

    /* tính giá */

    // Format Number
    function addCommas(str) {
        var amount = new String(str);
        amount = amount.split("").reverse();

        var output = "";
        for (var i = 0; i <= amount.length - 1; i++) {
            output = amount[i] + output;
            if ((i + 1) % 3 == 0 && (amount.length - 1) !== i)
                output = '.' + output;
        }
        return output;
    }

    /* input number */
    $('.quantity').each(function() {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
            var price = $('#regular_price').val();
            var totalPrice = 0;
            totalPrice = newVal*price;
            $('#total_money').val(totalPrice);
            $('#price').val(addCommas(totalPrice * 1));
        });

        btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
            var price = $('#regular_price').val();
            var totalPrice = 0;
            totalPrice = newVal*price;
            $('#total_money').val(totalPrice);
            $('#price').val(addCommas(totalPrice * 1));
        });

    });
    $(".order_pro").click(function (){
        var pro_id = $(this).attr('data-target');
        var pro_name = $(this).attr('data-name');
        var pro_price = $(this).attr('data-price');
        $.ajax({
            type: "GET",
            url: '/check-login',
            data: {},
            success: function(res)
            {
                var data = JSON.parse(res);
                if(data.error_code == 0) {
                    $("#amount-format").val(addCommas(pro_price));
                    $("#regular_price").val(pro_price);
                    $("#price").val(addCommas(pro_price));
                    $("#pro-id").val(pro_id);
                    $(".productName").html(pro_name);
                    $("#myModal").modal();
                } else {
                    Swal.fire(
                        'Chưa đăng nhập!',
                        'Vui lòng đăng nhập/đăng ký để mua hàng',
                        'info'
                    )
                }
            }
        });
    });
});

