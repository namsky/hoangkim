<?php

return [

    'wm' => [
        'endpoint' => 'https://api.a45.me/api/public/Gateway.php',
        'wm_vendorid' => 'j53twapi',
        'wm_signature' => 'a955f10cadc35332cdf713e5b703489d',
        'api' => [
            'register' => [
                'cmd' => 'MemberRegister',
                'method' => 'GET'
            ],
            'login' => [
                'cmd' => 'SigninGame',
                'method' => 'GET'
            ],
            'change_balance' => [
                'cmd' => 'ChangeBalance',
                'method' => 'GET'
            ],
            'get_balance' => [
                'cmd' => 'GetBalance',
                'method' => 'GET'
            ]

        ]
    ],
    'ae' => [
        'endpoint' => 'https://tttint.onlinegames22.com/',
        'agentId' => 'j54test',
        'cert' => 'nm629crJl6th1GsI4Hp',
        "currency" => "CNY",
        "language" => "vn",
        'api' => [
            'register' => [
                'uri' => 'wallet/createMember',
                'method' => 'POST',
                "betLimit" => '{"SEXYBCRT":{"LIVE":{"limitId":[280301,280302]}}}',
            ],
            'login' => [
                'uri' => 'wallet/login',
                'method' => 'POST',
                "betLimit" => '{"SEXYBCRT":{"LIVE":{"limitId":[280301,280302]}}}',
            ],
        ]
    ],
    'evol' => [
        'endpoint' => 'http://staging.evolution.asia-live.com',
        'key' => 'snbced1fi2hvvmbx',
        'token' => '12a1cec6af27627993f6e4910809d4d9',
        'api' => [
            'register' => [
                'method' => 'POST'
            ],
        ]
    ],
    'cq9' => [
        'endpoint' => 'https://api.cqgame.games',
        'agentId' => 'j54test',
        'password' => 'j54test',
        'curency' => 'VNDK',
        'key' => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyaWQiOiI2MmZlNWQ4MTMzNjlmMjg0YjMyYWQ5NTgiLCJhY2NvdW50IjoiajU0dGVzdCIsIm93bmVyIjoiNjJmZTU5YjYzMzY5ZjI4NGIzMmFkOTIyIiwicGFyZW50IjoiNjJmZTU5YjYzMzY5ZjI4NGIzMmFkOTIyIiwiY3VycmVuY3kiOiJWTkQoSykiLCJqdGkiOiI1MjgzODAxMjMiLCJpYXQiOjE2NjA4MzcyNDksImlzcyI6IkN5cHJlc3MiLCJzdWIiOiJTU1Rva2VuIn0.3pEwqYr5A3tbwZQ1VMFYo1ZnSur7MC3c4tgstfB2xD8',
        'api' => [
            'register' => [
                'method' => 'POST',
                'url' => '/gameboy/player',
                'contentType' => 'application/x-www-form-urlencoded'
            ],
            'login' => [
                'method' => 'POST',
                'url' => '/gameboy/player/login',
                'contentType' => 'application/x-www-form-urlencoded'
            ],
            'getLinkGame' => [
                'method' => 'POST',
                'url' => '/gameboy/player/gamelink',
                'contentType' => 'application/x-www-form-urlencoded'
            ],
        ]
    ],
    'jdb' => [
        'endpoint' => 'http://api.jdb711.com/apiRequest.do?dc=LX&x=',
        'dc' => 'LX',
        'iv' => '06f4f48cbd7e381d',
        'key' => '564a6e6c11381779',
        'account' => 'j54test',
        'api' => [
            'register' => [
                'action' => 12,
                'method' => 'GET'
            ],
            'loginGame' => [
                'action' => 11,
                'method' => 'GET'
            ]
        ]
    ],
    'jili' => [
        'endpoint' => 'https://uat-wb-api.jlfafafa2.com/api1/',
        'agentId' => 'AB_j54vnd_VND',
        'agentKey' => '8928731dc2e058a8ad92abc36b0a32835b3b0309',
        // 'timezone' => 'UTC -4',
        'api' => [
            'register' => [
                'method' => 'POST',
                'url' =>'CreateMember'
            ],
        ]
    ],
    'allbet' => [
        'endpoint' => 'https://mw2.apidemo.net:8443',
        'agentId' => 'j54cna',
        'agentKey' => '3SINXc8MjlIu+YwMHT7kFmLGCDfmkxn6t1Pj/5AsU0EvymOSabEyEADA+C64Wi60qJC4y2HLbU9k8GyONb45Xw==',
        'operatorId' => '9098577',
        'suffix' => '2rk',
        'api' => [
            'register' => [
                'method' => 'POST',
                'url' =>'/CheckOrCreate'
            ],
            'login' => [
                'method' => 'POST',
                'url' =>'/Login'
            ],
        ]
    ],
];

