<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankLogMoney extends Model
{
    public function user(){
        return $this->belongsTo(User::class,'send_user_id');
    }

    use HasFactory;
    protected $table = 'bank_log_money';
    protected $fillable = [
        'send_user_id',
        'receiver_id',
        'before',
        'money',
        'after',
        'reason',
        'type',
        'status',
    ];
}
