<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'rate',
        'percent'
    ];
    public function grant(){
        return $this->belongsTo(Grant::class,'id');
    }
}
