<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MoneyFlag extends Model
{
    use HasFactory;
    protected $table = 'money_flag';

    protected $fillable = ['user_id', 'money', 'type', 'reason'];
}
