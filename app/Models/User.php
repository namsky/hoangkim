<?php

namespace App\Models;

use App\Models\LogMoney;
use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Grant;
use App\Models\Order;
use App\Models\Share;
use App\Models\GroupBonus;
use App\Models\Income;
use Illuminate\Support\Facades\Auth;
use App\Models\BankAccount;


class User extends Authenticatable
{
    use Notifiable;
    use Filterable;

    protected $guard = 'user';

    protected $gender = [
        1 => 'Nam',
        2 => 'Nữ'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'address',
        'avatar',
        'gender',
        'phone',
        'type',
        'url',
        'level',
        'rate',
        'f1_id',
        'f2_id',
        'f3_id',
        'wallet1',
        'point',
        'total_child',
        'group_status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    // Đại lý cấp trên
    public function f1_id() {
        $f1 = User::find(customer()->user()->f1_id);
        return $f1;
    }

    // Tính max thu nhập
    public function maxtt($user_id) {
        $sale =  Order::where('user_id',$user_id)->where('status',1)->sum('total_money');

        return round(($sale*250)/100);
    }

    // Tính số tiền hoa hồng đã nhận
    public function rose_received($user_id) {
        return LogMoney::where('user_id',$user_id)->where('status',1)->sum('money');
    }

    public function role()
    {
        return $this->hasOne(Role::class,'user_id','id');
    }

    public function bankAccount()
    {
        return $this->hasOne(BankAccount::class, 'user_id', 'id');
    }

    public function grant()
    {
        return $this->hasOne(Grant::class, 'order', 'rate');
    }

    public function share()
    {
        return $this->hasOne(Share::class, 'rate', 'rate');
    }

    public function group_bonus()
    {
        return $this->hasOne(GroupBonus::class, 'rate', 'rate');
    }

    public function income()
    {
        return $this->hasOne(Income::class, 'rate', 'rate');
    }

    public function affiliate()
    {
        return $this->hasMany(Affiliate::class, 'user_id', 'id');
    }
    public function order()
    {
        return $this->hasMany(Order::class);
    }

    public function log_money()
    {
        return $this->hasMany(LogMoney::class, 'user_id', 'id');
    }

    public function child_group(){
        return $this->hasMany(IonGroup::class, 'leader_id', 'id');
    }
    public function group(){
        return $this->hasOne(IonGroup::class, 'member_id', 'id');
    }

    public function customerBank()
    {
        return $this->hasMany(BankAccount::class,'user_id');
    }

    public function father()
    {
        return $this->belongsTo(User::class, 'f1_id');
    }




    // Hàm tổng quát tìm đại lý cấp trên nhận thưởng tầng
    public function find_f1($f1_id, $newbie){

        if(!$f1_id) {
            return array(
              'error_code' => 0,
              'message' => 'Không có cấp trên!',
            );
        }
        $f1 = User::find($f1_id);

        $condition = false;
        $bonus = Grant::where('type',3)->first()->percent;
        $f1_child = User::where('f1_id', $f1->id)->where('type', 1)->get()->count();
        if($f1_child >= 3 && $f1_child < 5 && $f1->level >= $newbie->level-5) {
            $condition = true;
        }elseif($f1_child>=5 && $f1_child < 10 && $f1->level >= $newbie->level-12){
            $condition = true;
        }elseif($f1_child>=10 && $f1->level>=$newbie->level-20){
            $condition = true;
        };
        if($condition){
            $log[0]['user_id'] = $f1->id;
            $log[0]['before'] = $f1->wallet2;
            $log[0]['money'] = $bonus;
            $f1->wallet2 = $f1->wallet2 + $bonus;
            // $f1->update();
            $log[0]['after'] = $f1->wallet2;
            $log[0]['type'] = 3;
            $log[0]['status'] = 1;
            $log[0]['reason'] = "Thưởng tầng giới thiệu thành công ".$newbie->username.' vào hệ thống';

            // Logmoney::create($log[0]);
            // Logmoney::create($log[1]);
        }
        if($f1->level>1){
            $f= new User();
            $f->find_f1($f1->f1_id, $newbie);
        };

    }

    public function rose($order, $userOrder) {
//        dd($product);
        if($userOrder->f1_id == NULL) {
            return array(
                'error_code' => 0,
                'message' => 'Không có cấp trên!',
            );
        }
//        dd(1);
        $user_f1 = User::find($userOrder->f1_id);
//        dd($user_f1);
        if($user_f1) {
            // Hoa hồng người giới thiệu trực tiếp
            $money = round($order->total_money*(6/100));
            $maxtt = $user_f1->maxtt($user_f1->id);
            $rose_received = $user_f1->rose_received($user_f1->id);
            if($rose_received < $maxtt) {
                $user_f1->wallet1 = $user_f1->wallet1 + $money;
                $user_f1->save();

                // Lưu log
                $log['user_id'] = $user_f1->id;
                $log['before'] = $user_f1->wallet1 - $money;
                $log['after'] = $user_f1->wallet1;
                $log['money'] = $money;
                $log['type'] = 1;
                $log['status'] = 1;
                $log['reason'] = 'Thưởng hoa hồng trực tiếp khi '.$userOrder->username.' mua hàng';
                LogMoney::create($log);
            } else {
                $money_flag = [
                    'user_id' => $user_f1->id,
                    'money' => $money,
                    'type' => 1,
                    'reason' => 'Số tiền nhận vượt quá Max TN. Max TN hiện tại:'.$maxtt,
                ];
                MoneyFlag::create($money_flag);
            }
            // Hoa hồng quản lý
            if($user_f1->f1_id) {
                $user_f2 = User::find($user_f1->f1_id);
                if($user_f2) {
                    $money = round($order->total_money*(2/100));
                    $maxtt = $user_f2->maxtt($user_f2->id);
                    $rose_received = $user_f2->rose_received($user_f2->id);
                    if($rose_received < $maxtt) {
                        $user_f2->wallet1 = $user_f2->wallet1 + $money;
                        $user_f2->save();

                        // Lưu log
                        $log['user_id'] = $user_f2->id;
                        $log['before'] = $user_f2->wallet1 - $money;
                        $log['after'] = $user_f2->wallet1;
                        $log['money'] = $money;
                        $log['type'] = 2;
                        $log['status'] = 1;
                        $log['reason'] = 'Thưởng hoa hồng quản lý khi '.$userOrder->username.' mua hàng';
                        LogMoney::create($log);
                    } else {
                        $money_flag = [
                            'user_id' => $user_f2->id,
                            'money' => $money,
                            'type' => 1,
                            'reason' => 'Số tiền nhận vượt quá Max TN. Max TN hiện tại:'.$maxtt,
                        ];
                        MoneyFlag::create($money_flag);
                    }

                    // Hoa hồng quản lý
                    if($user_f2->f1_id) {
                        $user_f3 = User::find($user_f2->f1_id);
                        if($user_f3) {
                            $maxtt = $user_f3->maxtt($user_f3->id);
                            $rose_received = $user_f3->rose_received($user_f3->id);
                            $money = round($order->total_money*(2/100));
                            if($rose_received < $maxtt) {
                                $user_f3->wallet1 = $user_f3->wallet1 + $money;
                                $user_f3->save();

                                // Lưu log
                                $log['user_id'] = $user_f3->id;
                                $log['before'] = $user_f3->wallet1 - $money;
                                $log['after'] = $user_f3->wallet1;
                                $log['money'] = $money;
                                $log['type'] = 2;
                                $log['status'] = 1;
                                $log['reason'] = 'Thưởng hoa hồng quản lý khi '.$userOrder->username.' mua hàng';
                                LogMoney::create($log);
                            } else {
                                $money_flag = [
                                    'user_id' => $user_f3->id,
                                    'money' => $money,
                                    'type' => 1,
                                    'reason' => 'Số tiền nhận vượt quá Max TN. Max TN hiện tại:'.$maxtt,
                                ];
                                MoneyFlag::create($money_flag);
                            }

                            // Hoa hồng quản lý
                            if($user_f3->f1_id) {
                                $user_f4 = User::find($user_f3->f1_id);
                                if($user_f4) {
                                    $maxtt = $user_f4->maxtt($user_f4->id);
                                    $rose_received = $user_f4->rose_received($user_f4->id);
                                    $money = round($order->total_money*(2/100));
                                    if($rose_received < $maxtt) {

                                        $user_f4->wallet1 = $user_f4->wallet1 + $money;
                                        $user_f4->save();

                                        // Lưu log
                                        $log['user_id'] = $user_f4->id;
                                        $log['before'] = $user_f4->wallet1 - $money;
                                        $log['after'] = $user_f4->wallet1;
                                        $log['money'] = $money;
                                        $log['type'] = 2;
                                        $log['status'] = 1;
                                        $log['reason'] = 'Thưởng hoa hồng quản lý khi '.$userOrder->username.' mua hàng';
                                        LogMoney::create($log);
                                    } else {
                                        $money_flag = [
                                            'user_id' => $user_f4->id,
                                            'money' => $money,
                                            'type' => 1,
                                            'reason' => 'Số tiền nhận vượt quá Max TN. Max TN hiện tại:'.$maxtt,
                                        ];
                                        MoneyFlag::create($money_flag);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }


    public function rose_customer($order, $userOrder) {
        if(!$userOrder->user_id) {
            return array(
                'error_code' => 0,
                'message' => 'Không có cấp trên!',
            );
        }
        $user_f1 = User::find($userOrder->user_id);
        $money = round($order->total_money*$user_f1->income->percent/100);
        for ($i = 0; $i <= 1000; $i++) {
            if($user_f1) {
                if($money > 0) {
                    $user_f1->wallet1 = $user_f1->wallet1 + $money;
                    $user_f1->update();
                    $log['user_id'] = $user_f1->id;
                    $log['before'] = $user_f1->wallet1 - $money;
                    $log['after'] = $user_f1->wallet1;
                    $log['money'] = $money;
                    $log['type'] = 6;
                    $log['status'] = 1;
                    $log['reason'] = 'Thưởng thu nhập/thu nhập khi '.$userOrder->name.' mua hàng';
                    LogMoney::create($log);

                    if($user_f1->f1_id != NULL) {
                        $user_f1 = User::find($user_f1->f1_id);
                        $money = round($money*$user_f1->income->percent/100);
                    } else break;
                } else break;
            } else break;

        }
    }


    public function find_father($id, $data, $i = 1){
        $child = User::where('f1_id',$id)->get();
        if($child){
            foreach ($child as  $customer) {
                $id_customer = $customer->id;
                $data[$i][0] = new \stdClass;
                $data[$i][0] -> f = '<div class="image-box"><img src="/web/images/user_vip.png" alt="\"></div><div class="username">'.$customer->name.'</div>';
                $data[$i][0] -> v = $customer->id;
                $data[$i][1] = $customer->f1_id;
                $data[$i][2] = 'đại lý cấp '.$customer->level;
                $i++;
                $this->find_father($id_customer, $data, $i);
            }
        }
        return $data;
    }


    // Hàm lấy tiền người dùng hiện tại
    public function group_money($user){
        $money = Order::where('user_id', $user->id)->where('status', 1)->sum('total_money');
        $total_money = $this->find_child($user->id, $money);
        return $total_money;
    }

    // Hàm tính doanh số nhóm
    public function group_sales($members){
        $money=0;
        if($members->count()>0){
            foreach($members as $member){
                $member_money = Order::where('user_id', $member->id)->where('status', 1)->get()->sum('total_money');
                $money += $member_money;
            }
        }
        return $money;
    }



    // Hàm để tìm các con bên dưới và cộng tổng doanh số
    function find_child($f1_id, $money){
        $childs = User::where('f1_id', $f1_id)->get();
        if($childs->count()>0){
            foreach($childs as $child){
                $child_money = Order::where('user_id', $child->id)->where('status', 1)->sum('total_money');
                $money += $child_money;
                if($child->total_child > 0 ){
                    $money = $this->find_child($child->id, $money);
                }
            }
        }
        return $money;
    }

    function total_group_sale($leader){
        $childs = User::where('f1_id', $leader->id)->where('group_status', 1)->get();
        $money = 0;
        if($childs->count()>0){
            foreach($childs as $child){
                $child_money = Order::where('user_id', $child->id)->where('status', 1)->sum('total_money');
                $money += $child_money;
            }
        }
        return $money;
    }


    public function minus_money($userOrder, $total, $reason) {
        $userOrder->wallet1 = $userOrder->wallet1-$total;
        $status = $userOrder->save();
        // Lưu Log
        $log = [
            'user_id' => $userOrder->id,
            'money' => $total,
            'before' => $userOrder->wallet1 + $total,
            'after' => $userOrder->wallet1,
            'status' => 0,
            'type' => 0,
            'reason' => $reason,
        ];
        LogMoney::create($log);
        //End
        return $status;
    }

    public function add_money($user, $amount, $wallet1, $reason) {
        $user->$wallet1 = $user->$wallet1+$amount;
        $status = $user->save();
        // Lưu Log
        $log = [
            'user_id' => $user->id,
            'money' => $amount,
            'before' => $user->$wallet1 - $amount,
            'after' => $user->$wallet1,
            'status' => 1,
            'type' => 7,
            'reason' => $reason,
        ];
        LogMoney::create($log);
        return $status;
    }


    public function check_rate($customer) {

        $check = Order::where('user_id', $customer->id)->where('status', 1)->sum('total_money');
        $money = $this->find_child($customer->id, $check);
        $check += $money;
        $rates = Grant::all();
        foreach ($rates as $rate) {
            if($check>$rate->total_sales) {
                $customer->rate = $rate->order;
                $customer->update();
            }
        }

        return $customer->rate;
    }

    public function find_all($user, $all_childs) {
        $childs = User::where('f1_id', $user->id)->get();
        $i = 0;
        foreach($childs as $child) {
            $all_childs[$i] = $child;
            $all_childs = $this->find_all($child, $all_childs);
            $i++;
        }
        return $all_childs;
    }

}
