<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\OrderDetail;
use App\Models\Category;
class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','slug', 'price', 'price_sale','price_system', 'stock', 'content', 'description', 'image', 'type', 'category_id', 'status', 'hot', 'status', 'user_id','expiry','origin','qrcode'
    ];

    public function orderDetail(){
        return $this->hasOne(OrderDetail::class, 'product_id', 'id');
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

    // Đếm số sp đã bán
    public function product_buy($id) {
        $order_details = OrderDetail::where('product_id',$id)->get();
        $count_buy = 0;
        if($order_details) foreach ($order_details as $order_detail) {
            $order = Order::find($order_detail->order_id);
            if($order && $order->status == 1) {
                $count_buy += $order_detail->amount;
            }
        }
        return $count_buy;
    }
}
