<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogMoney extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }


    protected $table = 'log_money';
    protected $fillable = [
        'user_id',
        'before',
        'after',
        'reason',
        'type',
        'money',
        'status',
        'voucher_id'
    ];


}
