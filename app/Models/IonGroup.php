<?php

namespace App\Models;
use App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IonGroup extends Model
{
    use HasFactory;
    protected $fillable = [
        'group_name',
        'leader_id',
        'member_id',
    ];

    public function leader()
    {
        return $this->belongsTo(User::class, 'leader_id');
    }

    public function members()
    {
        return $this->hasMany(User::class, 'id', 'member_id');
    }

    public function group_money($leader) {
        $groups = IonGroup::where('leader_id', $leader->id)->get();
        $group_money = 0;
        foreach($groups as $key => $group) {
            foreach($group->members as $member){
                $member_money = $member->order()->where('status',1)->sum('total_money');
                $group_money += $member_money;
            }
        }
        return $group_money;
    }

    public function single_group_money($group_name){
        $group = IonGroup::where('group_name', $group_name)->get();
        $money = 0;
        if($group){
            foreach($group as $member){
                $money += $member->members->order()->where('type', 1)->sum('total_money');
            }
        }
        return $money;
    }

}
