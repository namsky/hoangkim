<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\Admin\CreateBlogRequest;
use App\Http\Requests\Admin\UpdateBlogRequest;


class Blog extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id', 'name', 'content', 'description', 'slug', 'image'
    ];

    public function category()
    {
        return $this->belongsTo(BlogCategory::class);
    }

}
