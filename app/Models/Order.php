<?php

namespace App\Models;

use App\Models\Grant;
use App\Models\LogMoney;
use App\Models\User;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
    'user_id', 'phone', 'address', 'status', 'total_money','payment_type', 'pro_type', 'product_id', 'amount','name', 'customer_id', 'pay_money','point', 'transactionCode'
    ];
    public function orderDetail(){
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }
    public function affiliate(){
        return $this->hasMany(Affiliate::class,'order_id','id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }

    public function order_detail() {
        return $this->belongsTo(OrderDetail::class, 'order_id');
    }




    public function rose($order, $userOrder){
        $rose = Grant::all();
        $rose1 = $rose->where('type',1)->where('order',1)->first()->percent;
        $rose2 = $rose->where('type',1)->where('order',2)->first()->percent;
        $rose3 = $rose->where('type',1)->where('order',3)->first()->percent;

        //Tính và cập nhật hoa hồng cho cộng tác viên
        // dd($userOrder);
        if($userOrder->f1_id!=""){
            $f1 = User::where('id', $userOrder->f1_id)->first();
            $log[1]['user_id'] = $f1->id;
            $log[1]['before'] = $f1->wallet1;
            $log[1]['money'] = $f1_rose = $order->total_money*$rose1/100;
            $f1->wallet1 = $f1->wallet1+$f1_rose;
            $log[1]['after'] = $f1->wallet1;
            $log[1]['type'] = 6;
            $log[1]['status'] = 1;
            $log[1]['reason'] = 'Cộng tiền hoa hồng khi '.$userOrder->name.' mua hàng';
            $f1->update();
            LogMoney::create($log[1]);
            if($userOrder->f2_id!=""){
                $f2 = User::where('id', $userOrder->f2_id)->first();
                $log[2]['user_id'] = $f2->id;
                $log[2]['before'] = $f2->wallet1;
                $log[2]['money'] = $f2_rose = $order->total_money*$rose2/100;
                $f2->wallet1 =$f2->wallet1 + $f2_rose;
                $log[2]['after'] = $f2->wallet1;
                $log[2]['type'] = 6;
                $log[2]['status'] = 1;
                $log[2]['reason'] = 'Cộng tiền hoa hồng khi '.$userOrder->name.' mua hàng';
                $f2->update();
                LogMoney::create($log[2]);
                if($userOrder->f3_id!=""){
                    $f3 = User::where('id', $userOrder->f3_id)->first();
                    $log[3]['user_id'] = $f3->id;
                    $log[3]['before'] = $f3->wallet1;
                    $log[3]['money'] = $f3_rose = $order->total_money*$rose3/100;
                    $f3->wallet1 = $f3->wallet1 + $f3_rose;
                    $log[3]['after'] = $f3->wallet1;
                    $log[3]['type'] = 6;
                    $log[3]['status'] = 1;
                    $log[3]['reason'] = 'Cộng tiền hoa hồng khi '.$userOrder->name.' mua hàng';
                    $f3->update();
                    LogMoney::create($log[3]);
                }
            }
        }

    }



}
