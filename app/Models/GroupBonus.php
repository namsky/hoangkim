<?php

namespace App\Models;
use App\Models\Grant;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupBonus extends Model
{
    use HasFactory;
    protected $fillable = [
        'rate',
        'percent'
    ];
    public function grant(){
        return $this->belongsTo(Grant::class,'id');
    }
}
