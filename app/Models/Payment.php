<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount', 'method', 'user_id', 'transaction_id', 'order_code', 'error_code', 'status', 'oder_id', 'type'
    ];

    public function user() {
        return $this->belongsTo(User::class,'user_id');
    }

    public function customer_bank() {
        return $this->belongsTo(BankAccount::class, 'user_id','user_id');
    }
}
