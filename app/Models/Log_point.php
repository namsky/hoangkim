<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log_point extends Model
{
    use HasFactory;
    protected $table = 'log_points';
    protected $fillable = [
        'user_id',
        'before',
        'after',
        'reason',
        'type',
        'point',
        'status',
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
