<?php

namespace App\Components\Partners;

use GuzzleHttp\Client;
use Illuminate\Support\Str;
use App\Components\Core\Response;

class Alepay extends Partner
{
    protected function getBaseUri()
    {
        return $this->getConfigData('domain') . '/';

    }

    public function getSignature($data) {
        $checksum = "H9lQXWxFLIThTGn3L8DW6nDaGSjHey";
        $query = $this->genUri($data);
        if (isset($checksum)) {
            $vnpSecureHash =   hash_hmac('sha256', $query, $checksum);//
            $inputData['signature'] = $vnpSecureHash;
        }
        return $vnpSecureHash;
    }

    public function pay($inputData)
    {
//        $uri = $this->genUri($data);
//        $vnp_Url = "https://alepay-v3-sandbox.nganluong.vn/api/v3/checkout/request-payment";
//        $checksum = "ToRSJpRVEikDH3w1QSYE9gVQYGo8C1";
        $checksum = "mKFOW7DZjJTiB7G8vBJbRTGcSjuLSL";
        $query = $this->genUri($inputData);
//        dd($inputData);
        if (isset($checksum)) {
            $vnpSecureHash =   hash_hmac('sha256', $query, $checksum);//
            $inputData['signature'] = $vnpSecureHash;
        }
//        dd($inputData);

        $options['headers'] = $this->getHeaderSend();
        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json' ]
        ]);

//        $response = $client->post('https://alepay-v3-sandbox.nganluong.vn/api/v3/checkout/request-payment',
//            ['body' => json_encode(
//                $inputData
//            )]
//        );
        $response = $client->post('https://alepay-v3.nganluong.vn/api/v3/checkout/request-payment',
            ['body' => json_encode(
                $inputData
            )]
        );
        return  Response::create($response->getBody()->getContents(), $response->getStatusCode(), $response->getHeaders());

    }

    private function getHeaderSend()
    {
        return [
            'Content-Type'  => 'application/json'
        ];
    }


    public function makeRequest($path, $method = 'GET', $options = [])
    {
//        dd($path, $method, $options);
        $options = array_merge($this->getDefaultOptions(), $options);
        // $options['headers'] = $this->getHeaders($headers);
        $result = $this->client->request($method, $path, $options);
//        dd($result);
        $response = Response::create($result->getBody()->getContents(), $result->getStatusCode(), $result->getHeaders());
//        $this->log(array_merge([
//            'method' => $method,
//            'path' => $this->getBaseUri() . $path,
//        ], $options), [
//            'status' => $response->getStatusCode(),
//            'content' => $response->getContent()
//        ]);

        return $response;
    }

    public function genUri($data)
    {
        $uri = "";
        $index = 0;

        foreach($data as $key => $item) {
//            dd($item);
            if($index == 0) {
                if($key == 'allowDomestic') {
                    $uri .= $key . '=true';
                } else {
                    $uri .= $key . '='.$item;
                }
                $index++;

            } else {
                $uri .= '&' . $key . '='.$item;
            }
//            dd($key);
        };

        return $uri;
    }
}
