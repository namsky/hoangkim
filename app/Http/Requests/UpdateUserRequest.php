<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>  'required',
            'phone' => 'required|min:10',
            'address' => 'required',
        ]; 
    }

    public function messages()
    {
        return [
            'name.required' => 'Họ tên là trường bắt buộc',
            'phone.required' => 'Số điện thoại là trường bắt buộc',
            'phone.min' => 'Số điện thoại tối thiểu 10 số',
            'address.required' => 'Địa chỉ là trường bắt buộc',
        ];
    }
}
