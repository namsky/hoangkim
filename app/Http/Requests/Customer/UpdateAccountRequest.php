<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|min:10',
            'address' => 'required',
        ]; 
    }

    public function messages()
    {
        return [
            'phone.required' => 'Số điện thoại không được để trống!',
            'address.required' => 'Địa chỉ không được để trống!',
            'phone.min' => 'Số điện thoại phải nhập ít nhất 10 số!',
        ];
    }
}
