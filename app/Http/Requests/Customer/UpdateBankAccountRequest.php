<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBankAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'bank' => 'required', 
            'number' => 'required|min:9|max:14', 
            'branch' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên tài khoản không được để trống',
            'bank.required' => 'Tên ngân hàng không được để trống',
            'number.min' => 'Số tài khoản phải ít nhất 9 số',
            'number.max' => 'Số tài khoản không được quá 14 số',
            'number.required' => 'Số tài khoản không được để trống',
            'branch.required' => 'Chi nhánh không được để trống',
        ];
    }
}
