<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'unique:users',
            'name' =>  'required',
            'username' =>  'required|unique:users',
            'password' => 'required|min:6|max:15',
            'phone' =>  'required|min:10|unique:users',
            'address' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Email đã bị trùng',
            'username.unique' => 'Username đã bị trùng',
            'name.required' => 'Họ và Tên là trường bắt buộc',
            'password.required' => 'Mật khẩu là trường bắt buộc',
            'password.min' => 'Mật khẩu tối thiểu 6 ký tự',
            'password.max' => 'Mật khẩu tối đa 15 ký tự',
            'phone.required' => 'Số điện thoại là trường bắt buộc',
            'phone.min' => 'Số điện thoại tối thiểu 10 số',
            'phone.unique' => 'Số điện thoại này đã được sử dụng',
            'address.required' => 'Địa chỉ là trường bắt buộc',
        ];
    }
}
