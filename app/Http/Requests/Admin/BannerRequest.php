<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:5120',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên là trường bắt buộc nhập',
            'image.required' => 'Ảnh đại diện là trường bắt buộc nhập',
            'image.mimes' => 'Ảnh đại diện không đúng định dạng jpeg,png,jpg,gif,svg',
            'image.max' => 'Ảnh đại diện kích thước không được vượt quá 5120 Mb'
        ];
    }
}
