<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'price' => 'required',
            'content' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Tên sản phẩm là trường bắt buộc',
            'name.string' => 'Tên sản phẩm phải là chữ',
            'name.max' => 'Tên sản phẩm không quá 255 ký tự',
            'price.required' => 'Giá là trường bắt buộc',
            'content.required' => 'Nội dung là trường bắt buộc',
        ];
    }
}
