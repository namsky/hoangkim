<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserPayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|min:10',
            'address' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => 'Số điện thoại là trường bắt buộc',
            'phone.min' => 'Số điện thoại ít nhất phải 10 số',
            'address.required' => 'Địa chỉ nhận hàng là trường bắt buộc',
        ];
    }
}
