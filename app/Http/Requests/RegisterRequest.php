<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|required|unique:users',
            'username' =>  'required|unique:users',
            'name' =>  'required',
            'phone' => 'required',
            'password' => 'required|min:6|max:15',
            're-password' => 'required|same:password|min:6|max:15',
        ];

    }

    public function messages()
    {
        return [
            'email.email' => 'Email không đúng định dạng',
            'email.required' => 'Email là trường bắt buộc',
            'email.unique' => 'Email đã bị trùng',
            'username.unique' => 'Tên người dùng đã bị trùng',
            'name.required' => 'Tên là trường bắt buộc',
            'username.required' => 'Tên đăng nhập là trường bắt buộc',
            'phone.required' => 'SĐT là trường bắt buộc',
            'password.required' => 'Mật khẩu là trường bắt buộc',
            'password.min' => 'Mật khẩu tối thiểu 6 ký tự',
            'password.max' => 'Mật khẩu tối đa 15 ký tự',
            're-password.same' => 'Mật khẩu không khớp',
            're-password.required' => 'Mật khẩu không khớp',
            're-password.min' => 'Mật khẩu không khớp',
            're-password.max' => 'Mật khẩu không khớp',
        ];
    }
}
