<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Http\Requests\Admin\CreateProductRequest;
use App\Http\Requests\Admin\UpdateProductRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;


class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::orderBy('id', 'DESC')->paginate(20);

        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.products.create', compact('categories'));
    }

    public function store(CreateProductRequest $request)
    {
        $data = $request->only('name','price', 'price_sales', 'content', 'description','type','category_id','type');
        $data['slug'] = Str::slug($request->name);
        if($request->image != null){
            $data['image'] = $this->uploadFile($request->image);
        }else{
            $data['image'] = asset('/images/defaul-user-image.jpeg');
        }

        Product::create($data);
        session()->flash('success', 'Thêm sản phẩm mới thành công');

        return redirect()->route('ad.product.index');
    }

    public function edit( Product $product)
    {
        $categories = Category::all();
        return view('admin.products.edit', compact('product','categories'));
    }

    public function update(UpdateProductRequest $request,Product $product)
    {
        $data = $request->only('name', 'description', 'content', 'price', 'price_sales','type','category_id', 'type');
        $data['slug'] = Str::slug($request->name);
        if ($request->image != null) {
            $request->validate([
                'image' => 'mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);
            $path = $this->uploadFile($request->image);
            $data['image'] = $path;
        }

        $product->update($data);
        session()->flash('success', 'Cập nhật sản phẩm thành công');
        return redirect()->route('ad.product.index');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        session()->flash('success', 'Xóa sản phẩm thành công!');
        return redirect()->route('ad.product.index');
    }

    public function searchProduct(Request $request){
        $products = Product::Where('name', 'like', '%' .$request->name. '%')->orderBy('id', 'DESC')->paginate(20);
        return view('admin.products.index', compact('products'));
    }

    public function status($id)
    {
        $product = Product::find($id);
        if ($product->status == 0) {
            $product->update(['status' => 1]);
            session()->flash('success', 'Hiện sản phẩm thành công');
            return redirect()->back();
        } else {
            $product->update(['status' => 0]);
            session()->flash('success', 'Ẩn sản phẩm thành công');
            return redirect()->back();
        }

    }

    public function hot(Request $request) {
        $product = Product::find($request->pro_id);
        if($product->hot == 0) {
            $product->hot = 1;
        } else {
            $product->hot = 0;
        }
        $product->save();
        return response('Success', 200)
            ->header('Content-Type', 'text/plain');
    }
}
