<?php

namespace App\Http\Controllers\Admin;

use App\Models\Log_point;
use App\Models\LogMoney;
use App\Models\Order;
use App\Models\BankLogMoney;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogMoneyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bonus = LogMoney::orderBy('id','DESC')->paginate(10);
        $bank_in = BankLogMoney::where('status',0)->orderBy('id','DESC')->paginate(10);
        $bank_out = BankLogMoney::where('status',1)->orderBy('id','DESC')->paginate(10);
        $buy = Order::where('status',1)->orderBy('id','DESC')->paginate(10);
        return view('admin.log', compact('bonus','bank_in','bank_out', 'buy'));
    }

    public function log_bank() {
        $title = 'Lịch sử chuyển khoản';
        $datas = BankLogMoney::where('status',1)->orderBy('id','DESC')->paginate(20);
        return view('admin.log', compact('datas','title'));
    }
    public function log_wallet() {
        $title = 'Lịch sử chuyển ví';
        $datas = BankLogMoney::where('status',0)->orderBy('id','DESC')->paginate(10);
        return view('admin.log', compact('datas','title'));
    }
    public function log_transaction() {
        $title = 'Lịch sử giao dịch';
        $datas = LogMoney::orderBy('id','DESC')->paginate(10);
        return view('admin.log', compact('datas','title'));
    }
    public function log_order() {
        $title = 'Lịch sử mua hàng';
        $datas = Order::where('status',1)->orderBy('id','DESC')->paginate(10);
        return view('admin.logs.order', compact('datas','title'));
    }

    public function log_point() {
        $title = 'Lịch sử tích điểm';
        $datas = Log_point::orderBy('id','DESC')->paginate(20);
        return view('admin.logs.point', compact('datas','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LogMoney  $logMoney
     * @return \Illuminate\Http\Response
     */
    public function show(LogMoney $logMoney)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LogMoney  $logMoney
     * @return \Illuminate\Http\Response
     */
    public function edit(LogMoney $logMoney)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LogMoney  $logMoney
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LogMoney $logMoney)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LogMoney  $logMoney
     * @return \Illuminate\Http\Response
     */
    public function destroy(LogMoney $logMoney)
    {
        //
    }
}
