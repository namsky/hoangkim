<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BannerRequest;
use App\Http\Requests\Admin\UpdateBannerRequest;
use App\Models\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::orderBy('id', 'desc')->paginate(10);
        return view('admin.banner.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request)
    {
        
        try {
            $data = $request->only('name', 'type');
            $data['image'] = $this->uploadFile($request->image);
            if ($request->has('url')) $data['url'] = $request->url;
            Banner::create($data);
            return redirect()->route('ad.banner.index')->with('success', 'Thêm banner thành công!');
        } catch (\Throwable $th) {
            dd($data);
            return redirect()->back()->with('error', 'Thêm banner thất bại!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        return view('admin.banner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBannerRequest $request, Banner $banner)
    {
        try {
            $data = $request->only('name', 'type');
            if($request->has('image')) $data['image'] = $this->uploadFile($request->image);
            if ($request->has('url')) $data['url'] = $request->url;
            $banner->update($data);
            return redirect()->route('ad.banner.index')->with('success', 'Sửa banner thành công!');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Sửa banner thất bại!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        $banner->delete();
        return redirect()->route('ad.banner.index')->with('success', 'Xóa banner thành công!');
    }

    public function type(Banner $banner)
    {
        if($banner->type == 0){
            $banner->update(['type'=>1]);
            return redirect()->back()->with('success', 'Hiện banner thành công!');
        }
        $banner->update(['type'=>0]);
        return redirect()->back()->with('success', 'Ẩn banner thành công!');
    }
}
