<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Log_point;
use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\User;
use App\Models\Grant;
use App\Models\Affiliate;
use App\Models\LogMoney;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OrderExport;

class OrderController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders= Order::orderBy('id','DESC')->paginate(10);
        return view('admin.orders.index', compact('orders'));
    }

    /**
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
         $orderDetails = OrderDetail::where('order_id', $order->id)->get();
        return view('admin.orders.edit',compact(['order','orderDetails']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {

        //cập nhật trạng thái đơn hàng
        if ($order->status == 1 && $request->status == 1){
            session()->flash('error', 'Đơn hàng đã được xác nhận!');
            return redirect()->back();
        }
        $status = Order::find($order->id)->update(['status' => $request->status]);
        if($status) {
            if($order->user_id != NULL) {
                $userOrder = User::find($order->user_id);
                if ($userOrder) {
                    // Cộng điểm tích lũy cho người mua
                    $userOrder->point = $userOrder->point + $order->total_money;
                    $userOrder->save();
                    // Lưu log cộng điểm
                    $log_point = [
                        'user_id' => $userOrder->id,
                        'before' => $userOrder->point - $order->total_money,
                        'point' => $order->total_money,
                        'after' => $userOrder->point,
                        'type' => 1,
                        'status' => 1,
                        'reason' => 'Cộng điểm tích lũy khi mua sản phẩm',
                    ];
                    Log_point::create($log_point);

                    // Tính hoa hồng cho các thành viên cấp trên
                    $rose = new User();
                    $rose->rose($order, $userOrder);

                    if($order->pro_type == 3) {
                        $userOrder->type = 1;
                        $userOrder->update();
                    }
                }
            }

        }
        session()->flash('success', 'Cập nhật trạng thái đơn hàng thành công');
        return redirect()->route('ad.order.edit',$order->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        // cập nhật lại thông tin affiliate liên quan đến đơn hàng
        if($order->affiliate->count()){
            $order->affiliate->each(function ($item){
                $item->update(['order_id',0]);
            });
        }


        // xóa thông tin chi tiết của đơn hàng
        if(OrderDetail::where('order_id', $order->id)->delete()){
            $order->delete();
            session()->flash('success', 'Xóa đơn hàng thành công');
            return redirect()->route('ad.order.index');
        }else{
            session()->flash('error', 'Xóa đơn hàng thất bại');
            return redirect()->route('ad.order.index');
        }
    }

    // info order search
    public function searchOrder(Request $request) {
        $orders = Order::Where('id', 'like', '%' .$request->code. '%')->orderBy('id', 'DESC')->paginate(20);
        return view('admin.orders.index', compact('orders'));
    }

    public function exportOrders()
    {
        $orders = Order::all();
        return Excel::download(function($excel) use ($orders) {
            $excel->sheet('Orders', function($sheet) use ($orders) {
                $sheet->fromArray($orders);
            });
        }, 'orders.xlsx');
    }

    public function export()
    {
        return Excel::download(new OrderExport, 'orders.xlsx');
    }
}
