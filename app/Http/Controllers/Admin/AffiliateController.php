<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Affiliate;
use App\Models\User;
use App\Models\IonGroup;
class AffiliateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $affiliates = Affiliate::with('order', 'user')->get();   
        return view('admin.affiliates.index',compact('affiliates'));
    }

    public function destroy(Affiliate $affiliate)
    {
        $affiliate->delete();
        session()->flash('success', 'Xóa đơn Affiliate thành công');
        return redirect()->route('ad.affiliate.index');
    }

    public function searchAffiliate(Request $request){
        $affiliates = Affiliate::with([
            'user' => function ($query) use ($request) {
                return $query->where('name', 'like', "%$request->name%");
            },
        ])->get();
        return view('admin.affiliates.index',compact('affiliates'));
    }

    public function show_groups(){
        $groups = IonGroup::all();
        $groupData = IonGroup::select('group_name', 'leader_id', 'member_id')
        ->orderBy('group_name')
        ->get()
        ->groupBy('group_name');
        foreach($groupData as $data){
            $data[0]['money']  = 0;
            foreach($data as $key => $record){
                foreach($record->members as $member) {
                    $data[0]['money'] += $member->order()->where('status', 1)->sum('total_money');
                }
            }
        }
        return view('admin.users.groups', compact('groups','groupData'));
    }

    public function show($groupId)
    {
        $group = Group::with('leader', 'members')->find($groupId);
        
        return view('groups.show', ['group' => $group]);
    }

    public function group_detail($group_name) {
        $group_members= IonGroup::where('group_name', $group_name)->get();
        return view('admin.users.group_detail', compact('group_members','group_name'));
    }   
}
