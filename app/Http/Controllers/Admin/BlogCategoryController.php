<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\CreateCategoryRequest;
use App\Http\Requests\Admin\UpdateCategoryRequest;
use Exception;
use Illuminate\Support\Str;


class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = BlogCategory::orderBy('position', 'ASC')->paginate();

        return view('admin.categories.index', compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        try {
            $data = $request->only('name', 'description', 'position');
            $data['slug'] = Str::slug($request->name);
            if($request->image != null){ 
                $data['image'] = $this->uploadFile($request->image);
            }else{
                $data['image'] = asset('/images/no_image.png');
            }
            BlogCategory::create($data);

            session()->flash('success', 'Thành công');
            return redirect()->route('ad.blog_category.index');
        } catch(Exception $e) {
            session()->flash('error', 'Thất bại');
            return redirect()->back();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BlogCategory $blogCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogCategory $blog_category)
    {
        return view('admin.categories.edit', compact('blog_category'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, BlogCategory $blog_category)
    {
        $data = $request->only('name', 'description', 'position');

        if ($request->image != null) {
            $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);

            $path = $this->uploadFile($request->image);
            $data['image'] = $path;

        }
        $data['slug'] = Str::slug($request->name);
        $blog_category->update($data);

        session()->flash('success', 'Thành công');
        return redirect()->route('ad.blog_category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BlogCategory  $blogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogCategory $blog_category)
    {
        $blog_category->blogs()->delete();
        $blog_category->delete();
        session()->flash('success', 'Thành Công');
        return redirect()->route('ad.blog_category.index');
    }

}
