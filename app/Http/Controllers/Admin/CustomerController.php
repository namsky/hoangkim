<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use App\Models\User;
use App\Models\Role;
use App\Models\IonGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $customers = User::orderBy('id', 'DESC')->paginate(10);
        foreach ($customers as $key => $customer) {
            $groups = IonGroup::where('leader_id', $customer->id)->select('leader_id')
            ->selectRaw('COUNT(*) as members')
            ->groupBy(['leader_id', 'group_name'])
            ->get()->count();
            // dd($groups);
            $customer['groups'] = $groups;
        }
        return view('admin.customers.index', compact('customers'));
    }

    public function create()
    {
        return view('admin.customers.create');
    }

    public function store(CreateUserRequest $request)
    {
        // tạo cộng tác viên mới, tạo bởi admin master
        $data = $request->only('name', 'email', 'phone', 'gender', 'address','username');
        $data['type'] = 2;
        $data['url'] = time();
        $data['level'] = 1;
        $data['password'] = Hash::make($request->password);
        $customer = User::create($data);

        // tạo parent của cộng tác viên mới
        $role['user_id'] = $customer->id;
        $role['parent_id'] = 0;
        Role::create($role);

        session()->flash('success', 'Thêm tài khoản cộng tác viên thành công');
        return redirect()->route('ad.customer.index');
    }


    public function edit(User $customer)
    {
        return view('admin.customers.edit', compact('customer'));
    }

    public function update(UpdateUserRequest $request, User $customer)
    {
        $data = $request->all();
        $data['password'] = Hash::make($request->password);

        if ($request->password) {
            $request->validate([
                'password' => 'required|min:6|max:15'
            ]);
            $data['password'] = Hash::make($request->password);
        }

        $customer->update($data);
        session()->flash('success', 'Thành công');

        return redirect()->route('ad.customer.index');
    }

    public function searchCustomer(Request $request) {
        // if($request->data==''){
        //     session()->flash('error', 'Bạn chưa nhập username hoặc SĐT cần tìm');
        //     return redirect()->route('ad.customer.index');
        // }
        if($request->find_option == 'username'){
            $customers = User::when($request->data, function ($query) use ($request) {
                return $query->where('username', $request->data);
            })
            ->when($request->rate, function ($query) use ($request) {
                return $query->where('rate', $request->rate);
            })
            ->paginate(20);
        }
        if($request->find_option == 'phone'){
            $customers = User::when($request->data, function ($query) use ($request) {
                return $query->where('phone', $request->data);
            })
            ->when($request->rate, function ($query) use ($request) {
                return $query->where('rate', $request->rate);
            })
            ->paginate(20);
        }
        if($customers->count()==0){
            session()->flash('error', 'Không tồn tại người dùng thỏa mãn');
            return redirect()->route('ad.customer.index');
        }
        // $customers = User::Where('name', 'like', '%' .$request->name. '%')->Where('type',1)->orderBy('id', 'DESC')->paginate(20);
        return view('admin.customers.index', compact('customers'));
    }

    public function money(Request $request){
        $customer = User::where('id',$request->user_id)->first();
        switch ($request->type) {
            case 0:
                $customer->wallet1 += $request->money;
                $plus = $customer->update();
                if($plus){
                    session()->flash('success', 'Thành công');
                    return redirect()->back();
                } else {
                    session()->flash('error', 'Thất bại');
                    return redirect()->back();
                }
                break;
            case 1:
                $customer->wallet1 -= $request->money;
                $plus = $customer->update();
                if($plus){
                    session()->flash('success', 'Thành công');
                    return redirect()->back();
                }else {
                    session()->flash('error', 'Thất bại');
                    return redirect()->back();
                }
                break;
            case 2:
                $money = new User();
                $group_money = $money->group_money($customer);      // Doanh thu của thành viên
                $customer->wallet1 += $group_money*1/100;           // Cộng hoa hồng bằng 1% tổng doanh thu hàng tháng
                $plus = $customer->update();
                if($plus){
                    session()->flash('success', 'Thành công');
                    return redirect()->back();
                }else {
                    session()->flash('error', 'Thất bại');
                    return redirect()->back();
                }
                break;
            default:
                // do something if none of the above cases match
        }
        if($request->type==0){
            // Cộng tiền
            $customer->wallet1 += $request->money;
            $plus = $customer->update();
            if($plus){
                session()->flash('success', 'Thành công');
                return redirect()->back();
            } else {
                session()->flash('error', 'Thất bại');
                return redirect()->back();
            }
        } else {
            $customer->wallet1 -= $request->money;
            $plus = $customer->update();
            if($plus){
                session()->flash('success', 'Thành công');
                return redirect()->back();
            }else {
                session()->flash('error', 'Thất bại');
                return redirect()->back();
            }
        }

    }

    public  function customer() {
        $customers = Customer::orderBy('id','DESC')->paginate(20);
        return view('admin.users.kh-vang-lai', compact('customers'));
    }
}
