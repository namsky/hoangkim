<?php

namespace App\Http\Controllers\Admin;

use App\Components\Partners\Tele;
//use App\Components\Partners\Vnpay;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Payment;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\HtmlString;

class PaymentController extends Controller
{
    protected $vnpay;
    protected $tele;

    public function __construct(Vnpay $vnpay, Tele $tele)
    {
        $this->vnpay = $vnpay;
        $this->tele = $tele;
    }
    public function recharge() {
        $payments = Payment::where('type',0)->orderBy('created_at','DESC')->paginate(10);
        return view('admin.payments.recharge', compact('payments'));
    }

    public function withdraw() {
        $payments = Payment::where('type',1)->paginate(10);
        return view('admin.payments.withdraw', compact('payments'));
    }

    public function post_withdraw(Request $request, $id) {
        $payment = Payment::find($id);
        if($request->type == 0) {
            $code = $payment->customer_bank->bank;
            if($payment->status == 2) return redirect()->back()->with('error', 'Giao dịch đã hủy');
            if($payment->status == 1) return redirect()->back()->with('error', 'Giao dịch đã duyệt');
            $bankcode = '';
            switch ($code) {
                case 'Viettin Bank':
                    $bankcode = 'VTB';
                    break;
                case 'Vietcom Bank':
                    $bankcode = 'VCB';
                    break;
                case 'Agri Bank':
                    $bankcode = 'AGB';
                    break;
                case 'ACB':
                    $bankcode = 'ACB';
                    break;
                case 'Techcom Bank':
                    $bankcode = 'TCB';
                    break;
                case 'BIDV':
                    $bankcode = 'BIDV';
                    break;
                case 'MSB':
                    $bankcode = 'MSB';
                    break;
                case 'MB Bank':
                    $bankcode = 'MB';
                    break;
                case 'DongA Bank':
                    $bankcode = 'DAB';
                    break;
                case 'AB Bank':
                    $bankcode = 'ABB';
                    break;
                case 'EXIM Bank':
                    $bankcode = 'EIB';
                    break;
                case 'TP Bank':
                    $bankcode = 'TPB';
                    break;
                case 'VP Bank':
                    $bankcode = 'VPB';
                    break;
                case 'VIB':
                    $bankcode = 'VIB';
                    break;
                default:
                    $bankcode = '';
                    break;
            }
            $oderid = rand();
            $sign = md5("vnpay203725|".$oderid."|".$payment->amount."|bank|4c1aa4416d58f543fb82cd4aa4d5d7b0");
            $data = [
                'merchant_no' => 'vnpay203725',
                'order_no'  => $oderid,
                'amount' => $payment->amount,
                'channel'      => 'bank',
                'bank_code'  =>  $bankcode,
                'notify_url' => request()->getHttpHost().'/api/payment/result',
                'bank_number' => $payment->customer_bank->number,
                'beneficiary_name'  => $payment->customer_bank->name,
                'sign'   => $sign
            ];

            $uri = new HtmlString($this->vnpay->payout($data));
            $response = Http::withoutVerifying()->get($uri);
//        dd($response->json(),$oderid);
//        \Log::info('log info: ', ['info' => $response]);
            if($response['status'] == 0) {
                $payment->status = 1;
                $payment->oder_id = $oderid;
                $payment->save();
                return redirect()->back()->with('success', 'Thành công');
            } else return redirect()->back()->with('error', $response['message']);
        } else {
            if($payment->status == 2 | $payment->status == 1) {
                return redirect()->back()->with('error', 'Đơn đã hủy hoặc đã duyệt!');
            }
            $user = User::find($payment->user_id);
            $user->wallet2 = $user->wallet2 + $payment->amount;
            $user->save();
            $payment->status = 2;
            $payment->save();
            return redirect()->back()->with('success', 'Đã hủy và hoàn tiền cho khách hàng!');
        }
    }

    public function post_recharge(Request $request, $id) {
        $payment = Payment::find($id);
        if($payment->status != 2) {
            session()->flash('error', 'Đơn đã duyệt hoặc hủy!');
            return redirect()->back();
        }
        if($request->type == 0) {
            $user = User::find($payment->user_id);
            $user->wallet1 = $user->wallet1 + $payment->amount;
            $user->save();
            $payment->status = 1;
            $payment->save();
            session()->flash('success', 'Duyệt thành công!');
            return redirect()->back();
        } else {
            $payment->status = 3;
            $payment->save();
            session()->flash('success', 'Hủy thành công!');
            return redirect()->back();
        }
    }
}
