<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Voucher;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    public function index() {
        $vouchers = Voucher::orderBy('id','DESC')->paginate(20);
        return view('admin.voucher.index')->with(compact('vouchers'));
    }

    public function confirm($id) {
        $voucher = Voucher::find($id);
        if($voucher && $voucher->status == 0) {
            $voucher->status = 1;
            $voucher->save();
            session()->flash('success', 'Duyệt thành công');

            return redirect()->back();
        } else {
            session()->flash('error', 'Không tìm thấy !');

            return redirect()->back();
        }
    }
    public function destroy($id) {
        $voucher = Voucher::find($id);
        if($voucher && $voucher->status == 0) {
            $voucher->status = 2;
            $voucher->save();
            session()->flash('success', 'Duyệt thành công');

            return redirect()->back();
        } else {
            session()->flash('error', 'Không tìm thấy !');

            return redirect()->back();
        }
    }
}
