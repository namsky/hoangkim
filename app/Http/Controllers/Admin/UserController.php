<?php

namespace App\Http\Controllers\Admin;

// use App\Models\Client;
use App\Models\User;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;


// Cái Controller này không liên quan
class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::Where('type',0)->orderBy('id', 'DESC')->paginate(20);
        $money = new User();
//        $group_money = $money->group_money($customer);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(CreateUserRequest $request)
    {
        $data = $request->only('name','username', 'email', 'phone', 'gender', 'address');
        $data['type'] = 0;
        $data['password'] = Hash::make($request->password);
        $data['level'] = 1;
        User::create($data);
        return redirect()->route('ad.user.index')->with('success', 'Thêm tài khoản thành viên thành công');
    }

    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $data = $request->all();
        if ($request->password) {

            $request->validate([
                'password' => 'required|min:6|max:15'
            ]);

            $data['password'] = Hash::make($request->password);
        }

        $user->update($data);
        session()->flash('success', 'Cập Nhật tài khoản khách hàng thành công');
        return redirect()->route('ad.user.index');
    }

    public function destroy(User $user)
    {
        (Order::where('user_id',$user->id)->update(['user_id'=>0])) ? $user->delete() : '';
        session()->flash('success', 'Xóa Tài khoản khách hàng thành công');
        return redirect()->route('ad.user.index');
    }

    // info user search
    public function searchUser(Request $request) {
        $users = User::Where('name', 'like', '%' .$request->name. '%')->Where('type',0)->orderBy('id', 'DESC')->paginate(20);
        return view('admin.users.index', compact('users'));
    }

}
