<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateConfigRequest;
use App\Http\Requests\Admin\UpdateConfigRequest;
use App\Models\Config;
use App\Models\Grant;
use App\Models\Income;
use App\Models\GroupBonus;
use App\Models\Share;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    public function index()
    {
        $configs = Config::orderBy('id', 'DESC')->paginate();
        return view('admin.configs.index', compact('configs'));
    }

    public function create()
    {
        return view('admin.configs.create');
    }

    public function store(CreateConfigRequest $request)
    {
        $data = $request->only('key', 'value');
        Config::create($data);
        session()->flash('success', 'Thành công');

        return redirect()->route('ad.config.index');
    }

    public function edit(Config $config)
    {
        return view('admin.configs.edit', compact('config'));
    }

    public function update(UpdateConfigRequest $request, Config $config)
    {
        $config->update([
            'value' => $request->value
        ]);
        session()->flash('success', 'Thành công');

        return redirect()->route('ad.config.index');
    }

    public function destroy(Config $config)
    {
        $config->delete();
        session()->flash('success', 'Thành công');

        return redirect()->route('ad.config.index');
    }

    public function rose_config() {
        $roses = Grant::all();
        return view('admin.configs.rose_config', compact('roses'));
    }

    public function rose_update(Request $request) {
        // dd($request->all());
        $data = $request->only('percent','total_sales');
        $rose = Grant::where('id', $request->rose_id)->first();
        $update = $rose->update($data);
        if($update){
            session()->flash('success', 'Thành công');
            return redirect()->back();
        } else 
        session()->flash('error', 'Thất bại');
        return redirect()->back();
    }

    public function group_bonus_config() {
        $roses = GroupBonus::all();
        return view('admin.configs.group_bonus', compact('roses'));
    }

    public function group_bonus_update(Request $request) {
        // dd($request->all());
        $data = $request->only('percent');
        $rose = GroupBonus::where('id', $request->rose_id)->first();
        $update = $rose->update($data);
        if($update){
            session()->flash('success', 'Thành công');
            return redirect()->back();
        } else 
        session()->flash('error', 'Thất bại');
        return redirect()->back();
    }

    public function incomes_config() {
        $roses = Income::all();
        return view('admin.configs.incomes', compact('roses'));
    }

    public function incomes_update(Request $request) {
        // dd($request->all());
        $data = $request->only('percent');
        $rose = Income::where('id', $request->rose_id)->first();
        $update = $rose->update($data);
        if($update){
            session()->flash('success', 'Thành công');
            return redirect()->back();
        } else 
        session()->flash('error', 'Thất bại');
        return redirect()->back();
    }
}
