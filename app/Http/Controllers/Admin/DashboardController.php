<?php

namespace App\Http\Controllers\Admin;

//use App\Components\Partners\Vnpay;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Order;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\HtmlString;


class DashboardController extends Controller
{
//    protected $vnpay;
//    public function __construct(Vnpay $vnpay)
//    {
//        $this->vnpay = $vnpay;
//    }
    public function index()
    {
        $orders = Order::all();
        // $sumTotalMoney = Order::sum('total_money');
        $sumCustomer = User::where('type',1)->count();
        $allUser = User::all();
//        $sumwallet = $allUser->sum('wallet1') + $allUser->sum('wallet2') + $allUser->sum('wallet3');
//
//        // Get số tiền trong ví VNPay
//        $sign = md5("vnpay203725|4c1aa4416d58f543fb82cd4aa4d5d7b0");
//        $data = [
//            'merchant_no' => 'vnpay203725',
//            'sign'   => $sign
//        ];
//        $uri = new HtmlString($this->vnpay->balance($data));
//        $response = Http::withoutVerifying()->get($uri);
//        if($response->json()['status'] == 200) {
//            $wallet['momo_wallet'] = number_format($response->json()['data']['momoBalance']);
//            $wallet['online_wallet'] = number_format($response->json()['data']['onlineBalance']);
//            $wallet['payout_wallet'] = number_format($response->json()['data']['payoutBalance']);
//        } else {
//            $wallet['momo_wallet'] = 0;
//            $wallet['online_wallet'] = 0;
//            $wallet['payout_wallet'] = 0;
//        }
        //End

        return view('admin.home',compact('sumCustomer','allUser','orders'));
    }
}
