<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Cart;
//use Gloudemans\Shoppingcart\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total = Cart::subtotal();
        $carts = Cart::content();
        $carts_qty = Cart::content()->count();
        return view('web.cart.view')->with(compact('carts','total','carts_qty'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $product = Product::find($id);
        if(!$product) {
            session()->flash('warning','Sản phẩm không tồn tại!');
            return redirect()->back();
        }
        Cart::add($product->id,$product->name,1,$product->price,0,['image'=>$product->image]);

        return redirect()->route('w.cart.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product =Product::find($request->pro_id);
        if(!$product) {
            session()->flash('warning','Sản phẩm không tồn tại!');
            return redirect()->back();
        }
        Cart::add($product->id,$product->name,$request->qty,$product->price,0,['image'=>$product->image]);
        return redirect()->route('w.cart.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->cart) {
            foreach ($request->cart as $key=>$cart) {
                Cart::update($key, $cart);
            }
        }
        session()->flash('success','Cập nhật thành công!');
        return redirect()->route('w.cart.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($rowId)
    {
        Cart::remove($rowId);
        session()->flash('success','Xóa thành công!');
        return redirect()->route('w.cart.index');
    }
}
