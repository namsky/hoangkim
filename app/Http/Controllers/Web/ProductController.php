<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Cart;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index($category_slug, $product_slug) {
        $product = Product::where('slug', $product_slug)->first();
        $product_relates = Product::where('id','!=',$product->id)->where('category_id',$product->category_id)->take(5)->where('type',1)->get();
        $product_sales = Product::where('id','!=',$product->id)->take(4)->orderBy('id','ASC')->where('type',1)->get();
        $carts_qty = Cart::content()->count();
        return view('web.products.index',compact('product','product_relates','product_sales','carts_qty'));
    }

    public function listProducts() {
        $carts_qty = Cart::content()->count();
        $categories = Category::all();
        $products = Product::where('type',1)->where('status',1)->orderBy('created_at','DESC')->paginate();
        return view('web.products.list',compact('products','categories', 'carts_qty'));
    }

    public function listProductsCategory(string $slug) {
        $category = Category::where('slug', $slug)->first();
        $products = Product::where('type',1)->where('status',1)->where('category_id', $category->id)->orderBy('created_at','DESC')->paginate();
        $categories = Category::all();
        $carts_qty = Cart::content()->count();
        return view('web.products.list',compact('products','category','categories','carts_qty'));
    }

    public function product_archive($id){
        $category = Category::find($id);
        $carts_qty = Cart::content()->count();
        $products = Product::where('category_id', $id)->where('status',1)->paginate();
        $categories = Category::all();
        return view('web.products.list',compact('products','categories','carts_qty','category'));
    }

    public function search(Request $request)
    {
        $product = $request->product ?? '';
        $categories = Category::all();
        $products = Product::where('name', 'like', '%' .$product .'%')->where('status',1)->where('type',1)->orderBy('created_at','DESC')->paginate();
        return view('web.products.list',compact('products','categories'));
    }
}
