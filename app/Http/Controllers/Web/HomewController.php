<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Product;
use App\Models\Blog;
use App\Models\Category;
use DB;
use Cart;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Hash;

class HomewController extends Controller
{

    public function index()
    {
//        dd(Hash::make('123456'));
        $blogs = Blog::query()->latest('created_at')->take(3)->get();
        $blog = Blog::latest('created_at')->first();
        $banners = Banner::where('type', 1)->orderBy('id','DESC')->where('type',1)->limit(1)->get();
        $product_sales = Product::where('type',1)->orderBy('hot','DESC')->where('type',1)->where('status',1)->take(6)->get();
        $product1 = Product::where('type',1)->where('status',1)->first();
        $two_products = Product::skip(1)->take(2)->where('type',1)->where('status',1)->where('status',1)->get();
        $all_products = Product::orderBy('id','DESC')->take(12)->where('type',1)->where('status',1)->get();
        $carts_qty = Cart::content()->count();
        return view('web.home.home',compact( 'blogs','blog', 'product_sales', 'banners','product1', 'two_products', 'all_products', 'carts_qty'));
    }

    public function check_login() {
        if(customer()->user()) {
            $res['error_code'] = 0;
        } else {
            $res['error_code'] = 1;
            $res['message'] = 'Bạn chưa đăng nhập';
        }
        return response($res, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function letter()
    {
        return view('web.about.letter');
    }

    public function aboutUs()
    {
        $carts_qty = Cart::content()->count();
        return view('web.about.index', compact('carts_qty'));
    }

}
