<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Order;
use App\Models\User;


class AccountwController extends Controller
{
    public function index(){
        $orders = Order::where('user_id',auth('user')->user()->id)->get();
        return view('web.account',compact('orders'));
    }

    public function update(UpdateUserRequest $request, $id){
        $data = $request->only('name', 'phone', 'address','gender');
        if(User::find($id)->update($data)){
            $request->session()->flash('success','Cập nhật tài khoản thành công');
        }else{
            $request->session()->flash('warning','Cập nhật tài khoản thất bại');
        }
        return redirect()->route('w.account.index');            
    }

   

}
