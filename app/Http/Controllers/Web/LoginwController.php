<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\LoginUserRequest;
class LoginwController extends Controller
{

    public function index()
    {
        return view('web.login');
    }
    
    public function login(LoginUserRequest $res)
    {
        $login = [
            'email' => $res->email,
            'password' => $res->password
        ];
        if ( Auth::guard('user')->attempt($login)) {
            return redirect()->route('w.home');
        } else {
            session()->flash('warning', 'Email hoặc Mật khẩu không chính xác');
            return redirect()->back();
        }
    }

    public function logout()
    {
        Auth::guard('user')->logout();
        return view('web.login');
    }

    public function getRegister()
    {
        return view('web.register');
    }
    
    public function register(CreateUserRequest $res)
    {
        $newUser = $res->only('name','phone','email','address','gender');
        $newUser['password'] = Hash::make($res->password);
        $user = User::create($newUser);
        Auth::guard('user')->login($user);
        session()->flash('success', 'Đăng ký tài khoản thành công');
        return redirect()->route('w.home');
    }
}
