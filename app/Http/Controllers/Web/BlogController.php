<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Category;
use Cart;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function blogs() {
        $blogs = Blog::all();
        $carts_qty = Cart::content()->count();
        $blog_outs = Blog::take(10)->orderBy('id','ASC')->get();
        return view('web.blog.blogs', compact('blogs','blog_outs', 'carts_qty'));
    }

    public function single_blog($category_slug, $blog_slug) {
        $carts_qty = Cart::content()->count();
        $blog = Blog::where('slug', $blog_slug)->first();
        $blog_relates = Blog::where('category_id',$blog->category_id)->where('id','!=',$blog->id)->orderBy('id','DESC')->take(6)->get();
        $blog_outs = Blog::where('category_id',$blog->category_id)->where('id','!=',$blog->id)->take(6)->orderBy('id','ASC')->get();
        return view('web.blog.single_blog', compact('blog', 'category_slug', 'blog_relates','blog_outs','carts_qty'));
    }
}
