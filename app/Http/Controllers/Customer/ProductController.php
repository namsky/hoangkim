<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateProductRequest;
use App\Http\Requests\Admin\UpdateProductRequest;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index() {
        $products = Product::where('user_id', customer()->user()->id)->orderBy('id','DESC')->paginate(20);
        return view('customer.products.index', compact('products'));
    }

    public function create() {
        $categories = Category::where('slug','!=','don-hang-chuyen-nhuong-kinh-doanh')->where('slug','!=','san-pham-dau-vao')->get();
        return view('customer.products.create', compact('categories'));
    }

    public function store(CreateProductRequest $request) {
        $data = $request->only('name','price', 'price_sale', 'content', 'description','type','category_id','price_system','stock','expiry', 'origin');
        $data['slug'] = Str::slug($request->name);
        $data['user_id'] = customer()->user()->id;
        $data['status'] = 0;
        $data['type'] = 1;
        if($request->image != null){
            $data['image'] = $this->uploadFile($request->image);
        }else{
            $data['image'] = asset('/images/defaul-user-image.jpeg');
        }
        if($request->qrcode != null){
            $data['qrcode'] = $this->uploadFile($request->qrcode);
        }else{
            $data['qrcode'] = asset('/images/defaul-user-image.jpeg');
        }

        Product::create($data);
        session()->flash('success', 'Thêm sản phẩm mới thành công');

        return redirect()->route('us.product.index');
    }

    public function edit( Product $product)
    {
        $categories = Category::all();
        return view('customer.products.edit', compact('product','categories'));
    }

    public function update(UpdateProductRequest $request,Product $product)
    {
        $data = $request->only('name', 'description', 'content', 'price', 'price_sale','type','category_id','price_system','stock','expiry', 'origin');
        $data['slug'] = Str::slug($request->name);
        if ($request->image != null) {
            $request->validate([
                'image' => 'mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);
            $path = $this->uploadFile($request->image);
            $data['image'] = $path;
        }
        if ($request->qrcode != null) {
            $request->validate([
                'qrcode' => 'mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);
            $path_code = $this->uploadFile($request->qrcode);
            $data['qrcode'] = $path_code;
        }

        $product->update($data);
        session()->flash('success', 'Cập nhật sản phẩm thành công');
        return redirect()->route('us.product.index');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        session()->flash('success', 'Xóa sản phẩm thành công!');
        return redirect()->route('us.product.index');
    }

    public function combo() {
        $products = Product::where('type',2)->get();
        return view('customer.products.combo', compact('products'));
    }

    public function combo_store($id) {
        if($id < 1 && $id > 6) {
            session()->flash('error', 'Không tìm thấy sản phẩm!');
            return redirect()->back();
        }
        $combos = Product::where('type',2)->orderBy('id','ASC')->get();
        $product = $combos[$id-1];

        $order['user_id'] = customer()->user()->id;
        $order['name'] = customer()->user()->name;
        $order['phone'] = customer()->user()->phone;
        $order['address'] = customer()->user()->address;
        $order['total_money'] = $product->price;
        $order['pay_money'] = $product->price;
        $order['amount'] = 1;
        $order['product_id'] = $product->id;
        $order['pro_type'] = $product->type;
        $store = Order::create($order);

        $order_detail['order_id'] = $store->id;
        $order_detail['product_id'] = $product->id;
        $order_detail['amount'] = 1;
        $order_detail['money'] = $product->price;
        $order_detail['pay_money'] = $product->price;
        OrderDetail::create($order_detail);

        session()->flash('success', 'Đặt mua sản phẩm thành công. Vui lòng thanh toán theo thông tin sau!');
//        return view('customer.products.payment')->with(compact('store'));
//        $this->payment($store);
        session()->put('store_combo', $store);
        return redirect()->route('us.buycombo.payment');

    }

    public function payment() {
        $store = session('store_combo');
        return view('customer.products.payment')->with(compact('store'));
    }
}
