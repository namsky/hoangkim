<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Affiliate;


class AffiliatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::where('user_id',customer()->user()->id)->get();
        $orders = array();
        if($customers) foreach ($customers as $customer) {
            $order = Order::where('customer_id',$customer->id)->orderBy('id', 'desc')->get();
            if($order) foreach ($order as $item) {
                array_push($orders,$item);
            }
        }
//        dd($orders);
        // dd($affiliates[0]->order->orderDetail[0]->product());
        return view('customer.affiliate',compact('orders'));
    }

    public function ref($username) {
        $user = User::where('username', $username)->first();
        if($user) {
            session()->put('ref', $user->id);
//            if (session()->has('ref')) {
//                $useraff = session('ref');
//            }
        }

        return redirect()->route('w.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
