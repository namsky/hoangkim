<?php

namespace App\Http\Controllers\Customer;

//use App\Components\Partners\Vnpay;
use App\Components\Partners\Alepay;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\HtmlString;
use Carbon\Carbon;
use Cart;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $vnpay;

    public function __construct(Alepay $alepay)
    {
        $this->vnpay = $alepay;
    }

    public function checkmember() {
        if(customer()->user()->level == 1) {
            $member = 'Thành viên';
            $products = Product::where('type',3)->get();
            $min_price = 600000;
        } elseif (customer()->user()->level == 2) {
            $member = 'Nhà cung cấp';
            $min_price = 2000000;
            $products =Product::where('type',4)->get();
        } elseif (customer()->user()->level == 3) {
            $member = 'Nhà sản xuất';
            $min_price = 2000000;
            $products = Product::where('type',5)->get();
        }
//        dd($product);
        return view('customer.CheckMember.buyCombo', compact('products','member','min_price'));
    }

    public function add_to_cart(Request $request) {
        if($request->all()['listProduct'] && $request->all()['listQuantity'][0]) {
            $datas = array();
            foreach ($request->all()['listProduct'] as $key=>$listProduct) {
                $datas[$key]['product_id'] = $listProduct;
                $datas[$key]['qty'] = $request->all()['listQuantity'][$key];
            }
            foreach ($datas as $data) {
                $product_id = $data['product_id'];
                $qty = intval($data['qty']);
                $product = Product::find($product_id);
                Cart::add($product_id, $product->name, $qty, $product->price,0, ['image' => $product->image]);

            }
            $json['code'] = 200;
            return response($json, 200)
                ->header('Content-Type', 'text/plain');
        } else {
            $json['code'] = 0;
            $json['msg'] = 'Có lỗi, vui lòng thử lại!';
            return response($json, 200)
                ->header('Content-Type', 'text/plain');
        }
    }
    public function checkout() {
        $carts['carts'] = Cart::content();
        $carts['total'] = Cart::subtotal();
//        dd($carts['carts']);
        return view('customer.CheckMember.checkout')->with(compact('carts'));
    }
    public function reset() {
        Cart::destroy();
        return redirect()->route('us.checkmember');
    }
    public function store_dau_vao(Request $request) {
        $carts = Cart::content();
        $total = (float)str_replace(',', '', Cart::subtotal());
        $user = users()->user();
        $order['user_id'] = $user->id;
        $order['name'] = $request->name;
        $order['phone'] = $request->phone;
        $order['address'] = $request->address;
        $order['status'] = 0;
        $order['total_money'] = $total;
        $order['pro_type'] = 3;
        $order['product_id'] = 0;
        $order['payment_method'] = $request->ShipType;
        $order['amount'] = $carts->count();
        $order['note'] = $request->txtNote;
        $status = Order::create($order);
        if($status) {
            foreach ($carts as $cart) {
                $order_detail['order_id'] = $status->id;
                $order_detail['product_id'] = $cart->id;
                $order_detail['amount'] = $cart->qty;
                $order_detail['money'] = $cart->price;
                $order_detail['pay_money'] = $cart->price;
                OrderDetail::create($order_detail);
            }
            Cart::destroy();
            session()->flash('success', 'Đặt hàng thành công! Vui lòng đợi quản trị viên phê duyệt!');
            return redirect()->route('us.checkmember');
        } else {
            session()->flash('info', 'Có lỗi sảy ra. Vui lòng thử lại sau!');
            return redirect()->back();
        }
    }





    public function index()
    {
        $products = Product::where('type', 1)->orderBy('id')->get();
        return view('customer.product',compact('products'));
    }

    public function info(Request $request) {
        $product = Product::find($request->id);
        $buy = $product->product_buy($product->id);
        $json['product'] = $product;
        $json['buy'] = $buy;
        return response($json, 200)
            ->header('Content-Type', 'text/plain');
    }


    public function store($pro_id)
    {
        $product = Product::find($pro_id);
        $month = Carbon::now()->month;
        if(Carbon::now()->month < 10) {
            $month = '0'.Carbon::now()->month;
        }
        $day = Carbon::now()->day;
        if(Carbon::now()->day < 10) {
            $day = '0'.Carbon::now()->day;
        }
        $minute = Carbon::now()->minute;
        if(Carbon::now()->minute < 10) {
            $minute = '0'.Carbon::now()->minute;
        }
        $second = Carbon::now()->second;
        if(Carbon::now()->second < 10) {
            $second = '0'.Carbon::now()->second;
        }

        $time = Carbon::now()->year.''.$month.''.$day.''.Carbon::now()->hour.''.$minute.''.$second;
//        dd($time);
        if($product) {
            $order['user_id'] = customer()->user()->id;
            $order['name'] = customer()->user()->name;
            $order['phone'] = customer()->user()->phone;
            $order['address'] = customer()->user()->address;
            $order['total_money'] = $product->price;
            $order['pay_money'] = $product->price;
            $order['amount'] = 1;
            $order['product_id'] = $product->id;
            $order['pro_type'] = $product->type;
            $store = Order::create($order);

            $order_detail['order_id'] = $store->id;
            $order_detail['product_id'] = $product->id;
            $order_detail['amount'] = 1;
            $order_detail['money'] = $product->price;
            $order_detail['pay_money'] = $product->price;
            OrderDetail::create($order_detail);

            $data = [
                'vnp_Amount' => $product->price*100,
                'vnp_Command' => 'pay',
                'vnp_CreateDate' => $time,
                'vnp_CurrCode' => 'VND',
                'vnp_IpAddr' => '127.0.0.1',
                'vnp_Locale' => 'vn',
                'vnp_OrderInfo' => customer()->user()->username,
                'vnp_BankCode' => 'VNPAY',
                'vnp_OrderType' => 'other',
                'vnp_ReturnUrl' => 'http://127.0.0.1:8000/payment/return',
                'vnp_TmnCode' => 'VQZNI22Y',
                'vnp_TxnRef' => $store->id,
                'vnp_Version' => '2.1.0',
            ];
            ksort($data);
            $response = new HtmlString($this->vnpay->pay($data));
            return redirect($response);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
