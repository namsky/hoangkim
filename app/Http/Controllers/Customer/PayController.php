<?php

namespace App\Http\Controllers\Customer;

use App\Components\Partners\Alepay;
use App\Components\Partners\Tele;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Log_point;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\LogMoney;
use App\Models\User;
use App\Models\Role;
use App\Models\Grant;
use App\Models\Affiliate;
use App\Http\Requests\UserPayRequest;
use Cart;


class PayController extends Controller
{
    protected $alepay;

    public function __construct(Alepay $alepay)
    {
        $this->alepay = $alepay;
//        $this->tele = $tele;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        return view('customer.pay',compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserPayRequest $req)
    {
        dd('ăèăebvlkăvluiẳv;ouh');
        $product = Product::find($req->pro_id);
        $user = new User();
        $paid = false;
        $bonus_level = false;
        $total_money = $req->amount * $product->price;

        // Kiểm tra hình thức thanh toán
        if($product->type == 1) {
            if($req->payment_type == 1) {
                if(auth('user')->user()->wallet1<$total_money){
                    session()->flash('warning', 'Số dư trong ví không đủ để đặt hàng');
                    return redirect()->back();
                }
                $paid = $user->minus_money(customer()->user(),$total_money,'wallet1','Trừ tiền mua sản phẩm '.$product->name.'');

            } elseif ($req->payment_type == 0) {
                $paid = false;
            } else {
                session()->flash('warning', 'Vui lòng chọn phương thức thanh toán');
                return redirect()->back();
            }
        } elseif ($product->type == 2) {
            if($req->payment_type == 1) {
                if(auth('user')->user()->wallet1<$total_money){
                    session()->flash('warning', 'Số dư trong ví không đủ để đặt hàng');
                    return redirect()->back();
                }
                $paid = $user->minus_money(customer()->user(),$total_money,'wallet1','Trừ tiền mua combo đầu vào');
                if($paid == true) {
                    $bonus_level = true;
                }
            } elseif ($req->payment_type == 0) {
                $paid == false;
            } else {
                session()->flash('warning', 'Vui lòng chọn phương thức thanh toán');
                return redirect()->back();
            }
        }
        // Tạo đơn hàng
        $order['user_id'] = customer()->user() ? customer()->user()->id : null;
        $order['phone'] = $req->phone;
        $order['address'] = $req->address;
        if($paid == true) {
            $order['status'] = 1;
        } else $order['status'] = 0;

        $order['total_money'] = $total_money;
        $order['pay_money'] = $total_money;
        $order['payment_type'] = $req->payment_type;
        $order['pro_type'] = $product->type;
        $order['product_id'] = $product->id;
        $order['amount'] = $req->amount;

        if($bonus_level == true && $paid == true) {
        }
        session()->flash('success', 'Đặt hàng thành công');
        return redirect()->back();

    }

    public function customer_pay() {
        $total = floatval(str_replace(",", "", Cart::subtotal()));
        $carts = Cart::content();

        if(customer()->user()){
            $money_minius = 0;
            // Số tiền tối đa được trừ
            $max_minius = ($total * 20 )/100;
            if(customer()->user()->point <= $max_minius) {
                $money_minius = customer()->user()->point;
            } elseif (customer()->user()->point > $max_minius) {
                $money_minius = $max_minius;
            }
            return view('customer.customer_pay', compact('carts','total', 'money_minius'));
        }
        return view('customer.customer_pay', compact('carts','total'));
    }

    public function store_order(UserPayRequest $request) {
        $userOrder = auth('user')->user();
        $total = (float)str_replace(',', '', Cart::subtotal());
        $carts = Cart::content();
        $user = new User();
        if($request->name=='') {
            session()->flash('error', 'Tên là trường bắt buộc');
            return redirect()->back();
        }
        $new_customer = array();
        if(!customer()->user()) {
            $creat_customer['name'] = $request->name;
            $creat_customer['phone'] = $request->phone;
            $creat_customer['address'] = $request->address;
            $creat_customer['email'] = $request->email;
            if (session()->has('ref')) {
                $creat_customer['user_id'] = session('ref');
            }
            $new_customer = Customer::create($creat_customer);
        } else {
            $new_customer = false;
        }
//        $total_cart = Cart::subtotal();
        $total_cart = filter_var(Cart::subtotal(), FILTER_SANITIZE_NUMBER_INT);

        $order['user_id'] = customer()->user() ? customer()->user()->id : null;
        if($new_customer) $order['customer_id'] = $new_customer->id;
        $order['name'] = $request->name;
        $order['phone'] = $request->phone;
        $order['address'] = $request->address;
        $order['status'] = 0;
        if (customer()->user()) {
            $money_minius = 0;
            // Số tiền tối đa được trừ
            $max_minius = ($total_cart * 20 )/100;
            if(customer()->user()->point <= $max_minius) {
                $money_minius = customer()->user()->point;
            } elseif (customer()->user()->point > $max_minius) {
                $money_minius = $max_minius;
            }
            $order['pay_money'] = $total_cart - $money_minius;
            $order['point'] = $money_minius;
        } else {
            $order['pay_money'] = $total_cart;
        }
        $order['total_money'] = $total_cart;
        $order['pro_type'] = 1;
        $order['product_id'] = 1;
        $order['amount'] = Cart::count();
        $store = Order::create($order);
        $total_item = 0;
        foreach ($carts as $cart) {
            $product = Product::find($cart->id);
            $order_detail['order_id'] = $store->id;
            $order_detail['product_id'] = $cart->id;
            $order_detail['amount'] = $cart->qty;
            $order_detail['money'] = $product->price*$cart->qty;
            if (customer()->user()) {
                $order_detail['pay_money'] = $product->price*$cart->qty;
            } else {
                $order_detail['pay_money'] = $product->price*$cart->qty;
            }
            OrderDetail::create($order_detail);
            $total_item += $cart->qty;
        }

        if ($store) {
            // Trừ điểm và lưu log
            if(customer()->user() && $order['point'] > 0 ) {
                $userOrder->point =  $userOrder->point - $order['point'];
                $userOrder->save();
                $log_point = [
                    'user_id' => $userOrder->id,
                    'before' => $userOrder->point +  $order['point'],
                    'point' => $order['point'],
                    'after' => $userOrder->point,
                    'type' => 1,
                    'status' => 2,
                    'reason' => 'Trừ điểm tích lũy khi mua sản phẩm',
                ];
                Log_point::create($log_point);
            }

            Cart::destroy();


            $orderCode = $this->generate_string();
            // Thanh toán qua cổng ngân lượng
            $data = [
//                    'tokenKey' => 'sBt5hlBdIZggx3TgnMJyUzyaYTYEy3',
                'tokenKey' => 'iDsmItZ0AzklpAWIKjaqHWcYV4PeEM',
                'orderCode' => $orderCode,
                'customMerchantId' => $this->generate_string(),
                'amount' => $store->total_money,
                'currency' => 'VND',
                'orderDescription' => 'Thanh toán đơn hàng '.$store->id.'',
                'totalItem' => $total_item,
                'checkoutType' => 4,
                'bankCode' => 'ZALOPAY',
                'paymentMethod' => 'VIETQR',
                'returnUrl' => 'https://choquocte.vn/payment/alepay_success',
                'cancelUrl' => 'https://choquocte.vn/payment/cancel',
                'buyerName' => $store->name,
                'buyerEmail' => 'namnv.egomedia@gmail.com',
                'buyerPhone' => $store->phone,
                'buyerAddress' => $store->address,
                'buyerCity' => 'HN',
                'buyerCountry' => 'Việt Nam',
                'allowDomestic' => true,
                'language' => 'vi',
            ];
            ksort($data);
            $response = $this->alepay->pay($data);
            if($response->getContent()['code'] == 000) {
                Cart::destroy();
                $store['transactionCode'] = $response->getContent()['transactionCode'];
                $store->save();
                return redirect($response->getContent()['checkoutUrl']);
            } else {
                session()->flash('info', 'Có lỗi. Xin vui lòng thử lại!');
                return redirect()->route('w.index');
            }


//            if(customer()->user() != null){
//                session()->flash('success', 'Đặt hàng thành công');
//                return redirect()->route('us.order.index');
//            }else {
//                session()->flash('success', 'Đặt hàng thành công');
//                return redirect()->route('w.index');
//            }

        } else {
            session()->flash('error', 'Đặt hàng thất bại');
            return redirect()->back();
        }
    }
    public function cancel() {
        session()->flash('info', 'Bạn đã từ chối thanh toán!');
        return redirect()->route('w.index');
    }

    public function alepay_success(Request $request) {
        $order = Order::where('transactionCode', $request->transactionCode)->first();
        if($order && $order->status == 0) {
            $order->status = 1;
//            $order->time_confirm = Carbon::now('Asia/Ho_Chi_Minh');
            $order->save();
                if($order->user_id) {
                    $user = User::find($order->user_id);
                    $user->point = $user->point + $order->total_money;
                    $user->save();
                    // Lưu log cộng điểm
                    $log_point = [
                        'user_id' => $user->id,
                        'before' => $user->point - $order->total_money,
                        'point' => $order->total_money,
                        'after' => $user->point,
                        'type' => 1,
                        'status' => 1,
                        'reason' => 'Cộng điểm tích lũy khi mua sản phẩm',
                    ];
                    Log_point::create($log_point);
                }
//            $this->tele->order_success($order->name,$order->id,number_format($order->total_money));


            session()->flash('success', 'Thanh toán thành công!');
            return redirect()->route('w.index');
        } else {
            session()->flash('error', 'Không tìm thấy đơn hàng hoặc đơn hàng đã được thành toán!');
            return redirect()->route('w.index');
        }
    }
    function generate_string($input='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', $strength = 8) {
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
        return $random_string;
    }
}
