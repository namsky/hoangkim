<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use App\Models\Grant;
use App\Http\Requests\Customer\CreateCustomerRequest;
use App\Exceptions\Handler;
use Throwable;
Use Alert;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
class LoginsController extends Controller
{
    public function index() {
        return view('customer.login');
    }
    public function register($url){
        $parent = User::where('url',$url)->first();
        return view('customer.register',compact('parent'));
    }

    public function web_register(){
        return view('customer.web_register');
    }

    // Thêm cộng tác viên qua link giới thiệu
//    public function store(CreateCustomerRequest $request){
//        $userParent = User::where('url', session('linkAffiliate'))->get()[0];
//        $username = User::where('username', $request->username)->first();
//        // if($username){
//        //     session()->flash('error', 'Tên đăng nhập đã bị trùng');
//        //     return redirect()->back();
//        // }
//        $data = $request->only('name', 'password', 'email', 'username', 'phone');
//        $data['type'] = 0;
//        $data['url'] = time();
//        $data['password'] = Hash::make($request->password);
//        $data['level'] = $userParent->level + 1;
//        $data['rate'] = 0;
//        $data['f1_id'] = $userParent->id;
//        // Kiểm tra xem có đời ông không
//        $f1 = User::where('id', $userParent->id)->first();
//        if($f1->f1_id != ""){
//            $data['f2_id'] = $f1->f1_id;
//            // Kiểm tra đời cụ
//            $f2 = User::where('id', $f1->f1_id)->first();
//            if($f2->f1_id != ""){
//                $data['f3_id'] = $f2->f1_id;
//            };
//        };
//
//        $data['level'] = $userParent->level + 1;
//        try{
//            $customer = User::create($data);
//            $parent = User::where('id','=', $customer->f1_id)->first();
//            $total_child = User::where('f1_id', $parent->id)->count();
//
//            // Cập nhật số con trực tiếp sau mỗi lần thêm tài khoản mới qua link giới thiệu
//            $parent->total_child = $total_child;
//            $parent->update();
//
//            Auth::guard('user')->login($customer);
//            session()->flash('success', 'Tạo thành công tài khoản cộng tác viên');
//            return redirect()->route('us.home');
//        }catch(Throwable $ex){
//            session()->flash('warning', 'Tạo tài khoản cộng tác viên thất bại');
//            return redirect()->back();
//        }
//    }

    public function login(Request $request) {
        $login = $request->only('username', 'password');
        if(Auth::guard('user')->attempt($login)) {
//            $user = new User();
//            $rate = $user->check_rate(Auth('user')->user());
            session()->flash('success', 'Đăng nhập thành công');
            return redirect()->route('us.home');
        }else{
            session()->flash('error', 'Tên đăng nhập hoặc Mật khẩu không chính xác');
            return redirect()->route('us.login');
        }
    }
    public function logout() {
        Auth::guard('user')->logout();
        return redirect()->route('us.login');
    }

    public function store(CreateCustomerRequest $request) {
        $data = $request->only('name', 'password', 'email', 'username','phone');
//        dd($request->all());
        if($request->f1_url!=''){
            $f1 = User::where('url', $request->f1_url)->first();
            if(!$f1){
                session()->flash('error', 'Mã giới thiệu không tồn tại');
                return redirect()->back();
            }
            $data['f1_id'] = $f1->id;
            $f1->total_child = $f1->total_child + 1;
            $f1->save();
        } else {
            $data['f1_id'] = null;
        }
        $data['level'] = $request->level;
        $data['type'] = 0;
        $data['url'] = time();
        $data['password'] = Hash::make($request->password);

            $user = User::create($data);
            Auth::guard('user')->login($user);
            // Tạo đơn hàng đầu tiên
            if($user->level == 1) {
                $product = Product::where('slug','san-pham-cho-thanh-vien')->first();
            } elseif ($user->level == 2) {
                $product = Product::where('slug','san-pham-cho-nha-cung-cap')->first();
            } elseif ($user->level == 3) {
                $product = Product::where('slug','san-pham-cho-nha-san-xuat')->first();
            }
            if($product) {
                $order['user_id'] = $user->id;
                $order['name'] = $user->name;
                $order['phone'] = $user->phone;
                $order['address'] = $user->address;
                $order['total_money'] = $product->price;
                $order['pay_money'] = $product->price;
                $order['amount'] = 1;
                $order['product_id'] = $product->id;
                $order['pro_type'] = $product->type;
                $store = Order::create($order);

                $order_detail['order_id'] = $store->id;
                $order_detail['product_id'] = $product->id;
                $order_detail['amount'] = 1;
                $order_detail['money'] = $product->price;
                $order_detail['pay_money'] = $product->price;
                OrderDetail::create($order_detail);
            }
            session()->flash('success', 'Đăng ký thành công');
            return redirect()->route('us.home');
    }
}
