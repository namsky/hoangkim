<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\BankAccount;
use App\Models\User;
use App\Http\Requests\Customer\CreateBankAccountRequest;
use App\Http\Requests\Customer\UpdateBankAccountRequest;

class BankAccountController extends Controller
{
    public function index(){
        $id = Auth::guard('user')->user()->id;
        $user = Auth::guard('user')->user();
        $bankAccount = BankAccount::where('user_id', $id)->get();
        return view('customer.payments.bank',compact('bankAccount','user'));
    }
    public function store(CreateBankAccountRequest $request)
    {
        $data = $request->only('name','bank','number','branch');
        $data['user_id'] = Auth::guard('user')->user()->id;
        BankAccount::create($data);
        session()->flash('success', 'Thêm tài khoản Ngân Hàng thành công');
        return redirect()->route('us.payment.withdraw');
    }
    public function update($id,UpdateBankAccountRequest $request)
    {
        $data = $request->only('name','bank','number','branch');
        $result  = BankAccount::where('id', $id) -> update($data);
        if($result){
            session()->flash('success', 'Cập nhật tài khoản Ngân Hàng thành công');
        }else{
            session()->flash('error', 'Cập nhật tài khoản Ngân Hàng thất bại');
        }
        return redirect()->route('us.bank.index');
    }


}
