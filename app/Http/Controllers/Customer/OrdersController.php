<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use App\Models\LogMoney;
use App\Models\Grant;


class OrdersController extends Controller
{
    public function index(){
        $orders = Order::where('user_id', auth('user')->user()->id)->orderBy('id', 'desc')->get();
        return view('customer.order',compact('orders'));
    }

    public function day_bonus(){
        $users = User::all();
        foreach($users as $user){
            // Tính doanh số nhóm
            $money = new User();
            $group_money = $money->group_money($user); 
            $bonus_money = ($group_money*$user->group_bonus->percent)/100;
            // Cộng tiền vào ví 
            $log['user_id'] = $user->id; 
            $log['before'] = $user->wallet1; 
            $log['money'] = $bonus_money; 
            $user->wallet1 = $user->wallet1 + $bonus_money;
            $log['after'] = $user->wallet1; 
            $log['status'] = 1;
            $log['type'] = 5;
            $log['reason'] = 'Thưởng doanh số nhóm theo ngày';
            $success = $user->update();          
        }
        if($success){
            LogMoney::create($log);
            session()->flash('success', 'Cộng tiền hoa hồng cho thành viên thành công');
            return redirect()->route('ad.customer.index');
        } else {
            session()->flash('error', 'Cộng tiền hoa hồng cho thành viên thất bại');
            return redirect()->route('ad.customer.index');
        }
    }




    // Tính thưởng tháng theo combo mua sản phẩm
    public function month_bonus(){
        $users = User::all();
        foreach($users as $user){
            // Tính doanh số nhóm
            $money = new User();
            $group_money = $money->group_money($user); 
            $bonus_money = $group_money*1/100;
            // Cộng tiền vào ví 
            $log['user_id'] = $user->id; 
            $log['before'] = $user->wallet1; 
            $log['money'] = $group_money; 
            $user->wallet1 = $user->wallet1 + $bonus_money;
            $log['after'] = $user->wallet1; 
            $log['type'] = 5;
            $log['reason'] = 'Thưởng hoa hồng theo doanh số nhóm hàng tháng';
            $success = $user->update();          
        }
        if($success){
            LogMoney::create($log);
            session()->flash('success', 'Cộng tiền hoa hồng cho thành viên thành công');
            return redirect()->route('ad.customer.index');
        } else {
            session()->flash('error', 'Cộng tiền hoa hồng cho thành viên thất bại');
            return redirect()->route('ad.customer.index');
        }
    }

}
