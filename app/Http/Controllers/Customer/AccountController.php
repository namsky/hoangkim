<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Order;
use App\Models\LogMoney;
use App\Models\BankLogMoney;
use App\Http\Requests\Customer\UpdateAccountRequest;


class AccountController extends Controller
{
    public function index()
    {
        $customer = Auth::guard('user')->user();
        return view('customer.account',compact('customer'));
    }

    public function bank_account(){
//        $all_users = User::all();
//        $id = Auth::guard('user')->user()->id;
//        $user = Auth::guard('user')->user();
//        $bankAccount = BankAccount::where('user_id', $id)->get();
        return view('customer.bank');
    }

    public function update( UpdateAccountRequest $request)
    {
        $customer = User::find(customer()->user()->id);
        $data = $request->only('phone','address','gender','name', 'email');
        $customer->update($data);
        session()->flash('success', 'Cập nhật tài khoản thành công');
        return redirect()->route('us.account.index');
    }

    public function bank_wallet(Request $request, User $user){
        $user = customer()->user();
        if($request->money<100000){
            session()->flash('error', 'Số tiền tối thiểu là 100.000đ');
            return redirect()->back();
        }elseif($request->money%10000!=0){
            session()->flash('error', 'Số tiền phải là bội số của 10.000đ');
            return redirect()->back();
        }elseif($user->wallet2 >= $request->money){
            if($request->wallet == 0) {
                $log['receiver_id'] = $log['send_user_id'] = $user->id;
                $log['before'] = $user->wallet2;
                $log['money'] = $request->money;
                $user->wallet2 =  $user->wallet2 - $request->money;
                $user->wallet1 =  $user->wallet1 + $request->money;
                $log['after'] = $user->wallet2;
                $log['type'] = 1;
                $log['status'] = 0;
                $log['reason'] = 'Chuyển tiền từ ví điểm thưởng sang ví mua hàng';
                $user->update();
                BankLogMoney::create($log);
            } elseif ($request->wallet == 1) {
                $log['receiver_id'] = $log['send_user_id'] = $user->id;
                $log['before'] = $user->wallet2;
                $log['money'] = $request->money;
                $user->wallet2 =  $user->wallet2 - $request->money;
                $user->wallet3 =  $user->wallet3 + $request->money;
                $log['after'] = $user->wallet2;
                $log['type'] = 1;
                $log['status'] = 0;
                $log['reason'] = 'Chuyển tiền từ ví điểm thưởng sang ví thưởng mua lẻ';
                $user->update();
                BankLogMoney::create($log);
            } else {
                session()->flash('error', 'Vui lòng chọn ví!');
                return redirect()->back();
            }

            session()->flash('success', 'Chuyển tiền thành công');
            return redirect()->back();
        }
        else {
            session()->flash('error', 'Số dư trong ví điểm thưởng không đủ để thực hiện giao dịch này');
            return redirect()->back();
        }
     }

    public function bank_out_view(){
        $user = Auth::guard('user')->user();
        return view('customer.bank_out', compact('user'));
     }

    public function bank_wallet_view(){
        $user = Auth::guard('user')->user();
        return view('customer.bank', compact('user'));
     }

    public function bank_transfer(Request $request){
        $user = Auth::guard('user')->user();
        $receiver = User::where('username', $request->receiver_id)->first();
        if(!$receiver) {
            session()->flash('error', 'Người nhận không tồn tại!');
            return redirect()->back();
        }
        if($user->username == $receiver->id){
            session()->flash('error', 'Bạn không thể tự chuyển cho chính mình!');
            return redirect()->back();
        }elseif($request->money<100000){
            session()->flash('error', 'Số điểm tối thiểu là 100.000đ');
            return redirect()->back();
        }elseif($request->money%10000!=0){
            session()->flash('error', 'Số điểm phải là bội số của 10.000đ');
            return redirect()->back();
        }elseif($request->money > $user->wallet1){
            session()->flash('error', 'Bạn không đủ điểm để thực hiện giao dịch này');
            return redirect()->back();
        }else {
            $user->wallet1 = $user->wallet1 - $request->money;
            $user_tranfer = $user->update();
            if($user_tranfer) {
                // Lưu log bank trừ tiền
                $log['send_user_id'] = $user->id;
                $log['receiver_id'] = $receiver->id;
                $log['before'] = $user->wallet1 + $request->money;
                $log['money'] = $request->money;
                $log['after'] = $user->wallet1;
                $log['status'] = 1;
                $log['type'] = 1;
                $log['reason'] = 'Chuyển điểm cho '.$receiver->name;
                BankLogMoney::create($log);
                // Lưu log Money
                $log_money['user_id'] = $user->id;
                $log_money['before'] = $user->wallet1 + $request->money;
                $log_money['money'] = $request->money;
                $log_money['after'] = $user->wallet1;
                $log_money['type'] = 3;
                $log_money['reason'] = 'Chuyển điểm cho '.$receiver->name;
                $log_money['status'] = 0;
                LogMoney::create($log_money);

                // Cộng tiền
                $receiver->wallet1 = $receiver->wallet1 + $request->money;
                $receiver->update();
                // Lưu log Money
                $log_money['user_id'] = $receiver->id;
                $log_money['before'] = $receiver->wallet1 - $request->money;
                $log_money['money'] = $request->money;
                $log_money['after'] = $receiver->wallet1;
                $log_money['type'] = 3;
                $log_money['reason'] = 'Nhận điểm cho '.$user->name;
                $log_money['status'] = 1;
                LogMoney::create($log_money);

                session()->flash('success', 'Chuyển điểm thành công');
            }  else {
                session()->flash('success', 'Chuyển điểm thất bại, vui lòng thử lại!');
            }
            return redirect()->back()->with(compact('user'));
        }
     }

    public function search_receiver(Request $request){
        if($request->get('receiver_id'))
        {
            $receiver_id = $request->get('receiver_id');
            $data = User::where('username',$receiver_id)->first();
            $data ? $data = $data->name : $data = 0;
            return Response($data);
        }
    }

    public function history(){
        $user = Auth::guard('user')->user();
        $bonus = LogMoney::where('user_id', $user->id)->where('status', 1)->where('type', 'not like', 4)->orderBy('id', 'desc')->paginate(10);
        $bank = BankLogMoney::where('send_user_id', $user->id)->where('status', 1)->where('type', 1)->orderBy('id', 'desc')->paginate(10);
        $receive = BankLogMoney::where('receiver_id', $user->id)->where('status', 1)->where('type', 0)->orderBy('id', 'desc')->paginate(10);
        $minus = LogMoney::where('user_id', $user->id)->where('status', 0)->orderBy('id', 'desc')->paginate(10);
        $wallet_bank = BankLogMoney::where('send_user_id', $user->id)->where('status', 0)->orderBy('id', 'desc')->paginate(10);
        $histories = LogMoney::where('user_id', $user->id)->orderBy('id', 'desc')->get();
        return view('customer.history', compact('bank','bonus','receive', 'wallet_bank','minus','histories'));

        User::where('username', 'not like', "%ray%")->get();
    }

}
