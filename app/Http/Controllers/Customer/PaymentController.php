<?php

namespace App\Http\Controllers\Customer;

use App\Components\Partners\Tele;
//use App\Components\Partners\Vnpay;
use App\Http\Controllers\Controller;
use App\Models\Log_point;
use App\Models\LogMoney;
use App\Models\Order;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\HtmlString;

class PaymentController extends Controller
{
    protected $vnpay;
    protected $tele;

    public function __construct(Vnpay $vnpay, Tele $tele)
    {
        $this->vnpay = $vnpay;
        $this->tele = $tele;
    }
    public function index() {
        return view('customer.payment');
    }

    public function responeVnpay(Request $request) {
//        dd($request->all());
        $data = [
            'vnp_TmnCode' => $request->vnp_TmnCode,
            'vnp_Amount' => $request->vnp_Amount,
            'vnp_BankCode' => $request->vnp_BankCode,
            'vnp_BankTranNo' => $request->vnp_BankTranNo,
            'vnp_CardType' => $request->vnp_CardType,
            'vnp_PayDate' => $request->vnp_PayDate,
            'vnp_OrderInfo' => $request->vnp_OrderInfo,
            'vnp_TransactionNo' => $request->vnp_TransactionNo,
            'vnp_ResponseCode' => $request->vnp_ResponseCode,
            'vnp_TransactionStatus' => $request->vnp_TransactionStatus,
            'vnp_TxnRef' => $request->vnp_TxnRef
        ];
        ksort($data);
        $response = $this->vnpay->checkIPNVnPay($data);
        if($response == $request->vnp_SecureHash) {
            $order = Order::find($request->vnp_TxnRef);
            if($request->vnp_ResponseCode == 00 && $order->status == 0) {

                $order->status = 1;
                $order->save();

                $userOrder = User::find($order->user_id);
                $userOrder->type = 1;
                $userOrder->save();
                $log_point = [
                    'user_id' => $userOrder->id,
                    'before' => $userOrder->point - $order->total_money,
                    'point' => $order->total_money,
                    'after' => $userOrder->point,
                    'type' => 1,
                    'status' => 1,
                    'reason' => 'Cộng điểm tích lũy khi mua sản phẩm',
                ];
                Log_point::create($log_point);

                // Tính hoa hồng cho các thành viên cấp trên
                $rose = new User();
                $rose->rose($order, $userOrder);

                session()->flash('success', 'Thanh toán thành công!');
                return redirect()->route('us.home');
            } else {
                session()->flash('success', 'Thanh toán lỗi, vui lòng thử lại hoặc liên hệ quản trị viên!');
                return redirect()->route('us.home');
            }
        } else {
            session()->flash('success', 'Thanh toán lỗi, vui lòng thử lại hoặc liên hệ quản trị viên!');
            return redirect()->route('us.home');
        }
    }

    public function success(Request $request) {
//        $this->send_telegram(customer()->user(),'success',20000);
        $amount = $request->amount;
        $method = $request->type;
        if($method == 'bank_qr') {
            $bankcode = $request->code;
        } else $bankcode = '';


        $oderid = rand();
        $data = [
            'user_id' => customer()->user()->id,
            'amount' => $amount,
            'method' => $method,
            'type' => 0,
            'oder_id' => $oderid,
        ];
        Payment::create($data);
        $sign = md5("vnpay203725|" . $oderid . "|" . $amount . "|" . $method . "|4c1aa4416d58f543fb82cd4aa4d5d7b0");
        $data = [
            'merchant_no' => 'vnpay203725',
            'order_no' => $oderid,
            'amount' => $amount,
            'channel' => $method,
            'bank_code' => $bankcode,
//            'notify_url' => 'https://account.muasub.com.vn/api/payment/result',
            'notify_url' =>  'https://nutrilifegroup.vn/api/payment/result',
            'c_ip' => '',
            'result_url' => '',
            'phone_number' => '',
            'extra_param' => '',
            'sign' => $sign,
        ];
        $response = new HtmlString($this->vnpay->pay($data));
        return \Redirect::to($response);
    }
    public function result(Request $request) {
        $result_code = $request->result_code;
        $order_no = $request->order_no;
        $ylt_order_no = $request->ylt_order_no;
        $amount = $request->amount;
        $channel = $request->channel;
        $sign = $request->sign;
        \Log::info('log info: ', ['info' => $request->all()]);
        $verify = md5("vnpay203725|" . $order_no . "|" . $ylt_order_no . "|" . $amount . "|" . $channel . "|4c1aa4416d58f543fb82cd4aa4d5d7b0");


        if ($result_code == 'success') { //check xem có thành công hay không
            if ($verify == $sign) { //check xem chữ ký có giống nhau hay không
                //hàm này cộng tiền cho user
                $payment = Payment::where('oder_id', $order_no)->first();
                $user = User::find($payment->user_id);
                if ($payment->status == 0) {
                    $payment2 = Payment::find($payment->id);
                    $payment2->status = 1;
                    $payment2->order_code = $result_code;
                    $payment2->save();
                    $user->wallet1 = $user->wallet1 + (floatval($amount));
                    $user->save();
                    $this->tele->send($user,'success',$amount);
                    return response('success', 200)
                        ->header('Content-Type', 'text/plain');
                } else {
                    dd('false');
                }
            } else {
                dd('false');
            }
        } else {
            dd('false');
        }
//        return redirect()->route('us.acc.payment');
    }

    public function withdraw() {
        if(!customer()->user()->customerBank->first()) {
            session()->flash('info', 'Vui lòng thêm tài khoản ngân hàng trước khi rút điểm!');
            return redirect()->route('us.bank.index');
        }
        return view('customer.payments.withdraw');
    }

    public function post_withdraw(Request $request) {
        $user = customer()->user();
        $amount = intval($request->amount);
        $amout_minius = $request->amount + ($request->amount*16.5)/100;
        if($request->amount < 100000) {
            session()->flash('error', 'Số điểm rút tối thiểu là 100.000 điểm');
            return redirect()->back();
        }
        if($user->wallet1 < $amout_minius) {
            session()->flash('error', 'Số điểm của bạn không đủ để rút!');
            return redirect()->back();
        }
        //Trừ tiền user
        $paid = $user->minus_money($user,$amout_minius,'Rút điểm');
        if($paid === true) {
            $payment = array(
                'user_id' => $user->id,
                'method' => 'withdraw',
                'amount' => $request->amount,
                'status' => 0,
                'type' => 1,
                'oder_id' => time(),
            );
            Payment::create($payment);
//            $this->tele->withdraw(customer()->user(),'success',$request->amount);
            session()->flash('success', 'Yêu cầu rút điểm thành công!');
            return redirect()->back();
        }
    }

    public function history_withdraw() {
        $payments = Payment::where('type',1)->where('user_id',customer()->user()->id)->paginate(20);
        return view('customer.payments.history_withdraw')->with(compact('payments'));
    }

    public function history() {
        $payments = Payment::where('status',1)->orderBy('id','DESC')->where('user_id',customer()->user()->id)->paginate(10);
        return view('customer.payment_history')->with(compact('payments'));
    }

    public function payment_offline(Request $request) {
        if($request->amount_off < 20000) {
            session()->flash('error', 'Số tiền nạp tối thiểu là 20.000');
            return redirect()->back();
        }
        $user = customer()->user();
        $payment = array(
            'user_id' => $user->id,
            'method' => 'muahang id '.$user->id,
            'amount' => $request->amount_off,
            'status' => 2,
            'type' => 0,
            'oder_id' => rand(),
        );
        Payment::create($payment);
        session()->flash('success', 'Thành công!');
        return redirect()->back();
    }
}
