<?php

namespace App\Http\Controllers\Customer;

use App\Models\LogMoney;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Order;


class HomesController extends Controller
{
    public function index(Request $request)
    {
        $customer = Auth('user')->user();
        if($customer->affiliate->count()){
            foreach ($customer->affiliate as $affiliate) {
                $order_id[] = $affiliate->order_id;
            }
            $totalOrder = Order::whereIn('id', $order_id)->get()->sum('total_money');
        }else{
            $totalOrder = 0;
        }
        session(['totalMoneyAffiliate' => $customer->affiliate ? $customer->affiliate->sum('money') : 0]);
        ($customer->role) ? $parent = $customer->role->parent : $parent = 0;
        $money = new User();
        $group_money = $money->group_money($customer);
        $childs = User::where('f1_id', $customer->id)->get();

        // Hoa hồng trực tiếp đã nhận
        $hhtt = LogMoney::where('user_id', $customer->id)->where('type',1)->where('status',1)->sum('money');

        // Hoa hồng quản lý đã nhận
        $hhql = LogMoney::where('user_id', $customer->id)->where('type',2)->where('status',1)->sum('money');

        return view('customer.home',compact('customer','parent','totalOrder', 'group_money', 'childs', 'hhtt', 'hhql'));
    }


}
