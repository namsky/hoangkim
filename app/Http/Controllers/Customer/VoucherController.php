<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Voucher;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\Cast\Object_;

class VoucherController extends Controller
{
    public function index() {
        $user = customer()->user();
        $logs = Voucher::where('user_id',$user->id)->orderBy('id','DESC')->paginate(20);
        return view('customer.voucher.index')->with(compact('logs'));
    }

    public function buy(Request $request) {
        $user = customer()->user();
        $voucher['user_id'] = $user->id;
        $voucher['money'] = $request->amount*1000;
        $voucher['status'] = 0;

        $status = Voucher::create($voucher);
//        dd($status);
        if($status) {
            session()->flash('buy_point', $status);
            return redirect()->back();
        } else {
            session()->flash('error', 'Có lỗi. Vui lòng thử lại');
            return redirect()->back();
        }
    }

    public function upload_bill(Request $request) {
        $voucher = Voucher::find($request->voucher_id);
        if($voucher) {
            if($request->image) {
                $voucher->bill = $this->uploadFile($request->image);
                $voucher->save();
                session()->flash('success', 'Thành công. Vui lòng đợi quản trị viên xét duyệt!');
                return redirect()->back();
            } else {
                session()->flash('info', 'Vui lòng gửi ảnh chuyển khoản của bạn để được xét duyệt nhanh hơn!');
                return redirect()->back();
            }
        } else {
            session()->flash('error', 'Có lỗi. Vui lòng thử lại');
            return redirect()->back();
        }
    }
}
