<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use Illuminate\Support\Facades\View;
use App\Models\User;

use function GuzzleHttp\json_encode;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $f1s = User::where('f1_id',customer()->user()->id)->orderBy('id','DESC')->get();
        $i = 0;
        $f2s = [];
        if($f1s) foreach ($f1s as $key=>$f1) {
            $user_f2s = User::where('f1_id',$f1->id)->get();
            if($user_f2s) foreach ($user_f2s as $user_f2) {
                $f2s[$i] = $user_f2;
                $i++;
            }
        }
//        dd($f2s);
//        $f2s = User::where('f2_id',customer()->user()->id)->orderBy('id','DESC')->paginate(20);
//        $f3s = User::where('f3_id',customer()->user()->id)->orderBy('id','DESC')->paginate(20);
        return view('customer.customer',compact('f1s'));
    }


    public function find_f1($id, $data, $i = 1){
        $child = User::where('f1_id',$id)->get();
        if($child){
            foreach ($child as  $customer) {
                $id_customer = $customer->id;
                settype($id_customer, "string");
                $data[$i][0] = new \stdClass;
                $data[$i][0] -> f = '<div class="image-box"><img src="/web/images/user_vip.png" alt="\"></div><div class="username">'.$customer->name.'</div>';
                $data[$i][0] -> v = $customer->id;
                $data[$i][1] = $customer->f1_id;
                $data[$i][2] = 'đại lý cấp '.$customer->level;
                $i++;
                $this->find_f1($id_customer, $data, $i);
            }
        }
        return $data;
    }

}
