<?php

namespace App\Http\Controllers;

use App\Models\LogMoney;
use App\Models\User;
use App\Models\Voucher;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CronJobController extends Controller
{
    public function chia_voucher_daily(Request $request) {
        if($request->token != 'FSZ1BMOHgaqtGey') {
            return response('Token không hợp lệ', 200)
                ->header('Content-Type', 'text/plain');
        } else {
            $vouchers = Voucher::where('status',1)->orderBy('id','DESC')->get();
            if($vouchers) foreach ($vouchers as $voucher) {
                $user = User::find($voucher->user_id);
                if($user) {
                    $dt = Carbon::now('Asia/Ho_Chi_Minh');
                    // Check nhận đủ 130% chưa
                    $check_one = LogMoney::where('user_id', $user->id)->where('type',4)->where('status',1)->where('voucher_id',$voucher->id )->sum('money');
                    if($check_one < ($voucher->money * 130 )/100) {

                        // Check max thu nhập
                        $check_maxtn = $user->maxtt($user->id);
                        // Tổng thu nhập
                        $total_rose = $user->rose_received($user->id);
                        if($total_rose < $check_maxtn) {
                            $money = round((($voucher->money * 130 )/100 ) / 1080);
                            $user->wallet1 = $user->wallet1 + $money;
                            $user->save();


                            // Lưu Log
                            $log['user_id'] = $user->id;
                            $log['before'] = $user->wallet1 - $money;
                            $log['after'] = $user->wallet1;
                            $log['money'] = $money;
                            $log['type'] = 4;
                            $log['status'] = 1;
                            $log['voucher_id'] = $voucher->id;
                            $log['reason'] = 'Thưởng săn Voucher ngày '.$dt->toDateTimeString().'';
                            LogMoney::create($log);
                        }

                    }
                }
            }
        }
    }
}
