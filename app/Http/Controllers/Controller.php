<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Jenssegers\Agent\Agent;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Config;
use Illuminate\Support\Facades\View;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        // Fetch the Site Settings object
//        $this->site_settings = Setting::all();
        // $products_left = Product::orderBy('id','DESC')->where('type','=',2)->paginate(10);
        // $products_right = Product::orderBy('id','DESC')->where('type','=',1)->paginate(10);
        $categories = Category::all();
        // $brands = Brand::orderBy('id','DESC')->get();
        // View::share('products_left', $products_left);
        // View::share('products_right', $products_right);
         View::share('categories', $categories);
    }

    public function uploadFile($file)
    {
        $filenameWithExt = $file->getClientOriginalName();
        //Get just filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get just ext
        $extension = $file->getClientOriginalExtension();
        // Filename to store
        $fileNameToStore = $filename.'_'.time().'.'.$extension;
        // Upload Image
        $path = $file->storeAs('public/photos/blog/images', $fileNameToStore);
        return str_replace('public', '/storage', $path);
    }

    // public function view($view)
    // {
    //     $agent = new Agent();
    //     return $agent->isMobile() ? view('web.mobile.'. $view) :  view('web.'. $view);
    // }

    public function getConfigByKey($key){
        return Config::where('key',$key)->first();
    }
}
