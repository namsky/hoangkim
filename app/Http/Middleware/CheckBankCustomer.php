<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckBankCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // $bank_user = CustomerBank::where('user_id','=',customer()->user()->id)->first();
        // if(empty($bank_user)) {
        //     return redirect()->route('w.bank.index');
        // }
        return $next($request);
    }
}
