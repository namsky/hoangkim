<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;
class CheckLinkRegister
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $uri = $request->path();
        $url = Str::of($uri)->basename();
        $checkUrl = User::where('url',$url)->get();
        if(!$checkUrl->count()) {
            return abort(404);
        }
        session(['linkAffiliate' => $url]);
        return $next($request);
    }
}
