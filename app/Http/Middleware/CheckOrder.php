<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // check cộng tác viên có được mua sản phẩm không 
        if(auth('user')->user()->type > 0){
            return $next($request);
        }else{
            if(auth('user')->user()->order->count() > 0){
                session()->flash('info', 'Đơn hàng "gói sản phẩm" của bạn đang được xử lý. Vui lòng đợi!');
                return redirect()->route('us.order.index');
            }else{
                session()->flash('info', 'Bạn chưa phải cộng tác viên chính thức nên cần phải mua gói sản phẩm trước.');
            }
            return redirect()->route('us.package.index');
        }
    }
}
