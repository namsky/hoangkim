<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Config;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $category_all = Category::orderBy('id','DESC')->where('slug','!=','don-hang-chuyen-nhuong-kinh-doanh')->where('slug','!=','san-pham-dau-vao')->get();
        if($category_all) View::share('category_all', $category_all);
        /* config */
//         View::share('notify_home', Config::where('key', 'notify')->first());
        // View::share('email', Config::where('key', 'email')->first());
        // View::share('hotline', Config::where('key', 'hotline')->first());
        // View::share('facebook', Config::where('key', 'facebook')->first());
        // View::share('zalo', Config::where('key', 'zalo')->first());
        // View::share('shopee', Config::where('key', 'shopee')->first());
        // View::share('youtube', Config::where('key', 'youtube')->first());
        // View::share('listCategory', Category::orderBy('id', 'desc')->get());
    }
}
