<?php

namespace App\Providers;

use App\Models\Config;
use App\Models\User;
use App\Observers\ConfigObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Config::observe(ConfigObserver::class);
//        User::observe(UserObserver::class);
        Paginator::useBootstrap();
    }
}
